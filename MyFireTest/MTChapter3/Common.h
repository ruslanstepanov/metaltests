//
//  Common.h
//  MTChapter3
//
//  Created by Ruslan Stepanov on 07/11/2019.
//  Copyright © 2019 Ruslan Stepanov. All rights reserved.
//

#ifndef Common_h
#define Common_h

#import <simd/simd.h>

typedef struct {
    //  uniform QUALITY float uTime;
    float uTime;
    //  uniform QUALITY float uSaturation;
    float uSaturation;
    //  uniform QUALITY float uWindDisplacement;
    float uWindDisplacement;
} FragmentUniforms;

typedef enum {
  BufferIndexVertices = 0,
  BufferIndexFragmentUniforms = 1
} BufferIndices;

typedef enum {
    MainTexture = 0,
    WindTexture = 1,
    GradTexture = 2
} Textures;

typedef enum {
  Position = 0,
  UV = 1
} Attributes;

#endif /* Common_h */
