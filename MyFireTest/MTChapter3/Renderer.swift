//
//  Renderer.swift
//  MTChapter3
//
//  Created by Ruslan Stepanov on 05/11/2019.
//  Copyright © 2019 Ruslan Stepanov. All rights reserved.
//

import MetalKit

var defaultVertexDescriptor: MDLVertexDescriptor = {
  let vertexDescriptor = MDLVertexDescriptor()
  vertexDescriptor.attributes[Int(Position.rawValue)] =
                MDLVertexAttribute(name: MDLVertexAttributePosition,
                format: .float3,
                offset: 0, bufferIndex: 0)
    
  vertexDescriptor.layouts[0] = MDLVertexBufferLayout(stride: 12)
    //  vertexDescriptor.attributes[Int(UV.rawValue)] =
    //                  MDLVertexAttribute(name: MDLVertexAttributeTextureCoordinate,
    //                  format: .float2,
    //                  offset: 12, bufferIndex: 0)
    //
    //  vertexDescriptor.layouts[0] = MDLVertexBufferLayout(stride: 20)
  
  return vertexDescriptor
}()

class Renderer: NSObject {
    static var device: MTLDevice!
    static var commandQueue: MTLCommandQueue!
    var mdlMesh: MDLMesh!
    var mesh: MTKMesh!
    var vertexBuffer: MTLBuffer!
    var pipelineState: MTLRenderPipelineState!
    // Т.к. нам не нужно хранить ссылку на MTLLibrary, то поле для него не создаем.
    
    var timer: Float = 0
    
    var quadVertices : [float3]!
    var quadVertBuffer: MTLBuffer!
    var fragmentUniforms = FragmentUniforms()
    var quadVertDescriptor : MTLVertexDescriptor!
    var mainTexture: MTLTexture!
    var windTexture: MTLTexture!
    var gradTexture: MTLTexture!
    var tickCount : Int = 0
    
    init(metalView: MTKView) {
        Renderer.device = MTLCreateSystemDefaultDevice()
        //else {
        //  fatalError("GPU not available")
        //}
        metalView.device = Renderer.device
        Renderer.commandQueue = Renderer.device.makeCommandQueue()!
        
        super.init()
        
        // вершины квадрата
        //        quadVertices = [ [0, 1, 1],
        //                        [0, 0, 1],
        //                        [1.0, 1.0, 1],
        //                        [1.0, 0, 1] ]
        //        quadVertices = [ [-0.5, 0.5, 1],
        //                        [-0.5, -0.5, 1],
        //                        [0.5, 0.5, 1],
        //                        [0.5, -0.5, 1] ]
        quadVertices = [ [-1, 1, 1],
                        [-1, -1, 1],
                        [1, 1, 1],
                        [1, -1, 1] ]
        
        // Создаем Metal-буфер, который будет содержать вершины.
        // Почему-то нужно создавать локальную переменную, иначе позиции нулевые становятся.
        var vertices: [float3] = self.quadVertices
        quadVertBuffer = Renderer.device.makeBuffer(bytes: &vertices,
                                                    length: MemoryLayout<float3>.stride * quadVertices.count,
                                                    options: [])
        
        createQuadVertDescriptor()
        
        fragmentUniforms.uTime = 0.0
        fragmentUniforms.uWindDisplacement = 1.0
        fragmentUniforms.uSaturation = 1.0
        
        //        // Создаем меш куба
        //        //        createCubeMesh()
        //        createTrainMesh()
        //
        //        // сохраняем данные о вершинах в MTLBuffer
        //        vertexBuffer = mesh.vertexBuffers[0].buffer
        
        // Инициализируем MTLLibrary с нужными нам шейдерами.
        // В отличие от OpenGL-шейдеров, эти компилируются когда мы компилируем проект,
        // это намного эффективнее чем компилить "на лету".
        let library = Renderer.device.makeDefaultLibrary()
        let vertexFunction = library?.makeFunction(name: "vertex_main")
        let fragmentFunction = library?.makeFunction(name: "fragment_main")
        
        // Создаем дескриптор пайплайна.
        let pipelineDescriptor = MTLRenderPipelineDescriptor()
        pipelineDescriptor.vertexFunction = vertexFunction
        pipelineDescriptor.fragmentFunction = fragmentFunction
        //        pipelineDescriptor.vertexDescriptor = MTKMetalVertexDescriptorFromModelIO(mdlMesh.vertexDescriptor)
        pipelineDescriptor.vertexDescriptor = MTKMetalVertexDescriptorFromModelIO(defaultVertexDescriptor) // quadVertDescriptor
        pipelineDescriptor.colorAttachments[0].pixelFormat = metalView.colorPixelFormat
        
        // Чтобы прозрачность не становилась черной
        pipelineDescriptor.colorAttachments[0].isBlendingEnabled = true
        pipelineDescriptor.colorAttachments[0].rgbBlendOperation = .add
        pipelineDescriptor.colorAttachments[0].alphaBlendOperation = .add
        pipelineDescriptor.colorAttachments[0].sourceRGBBlendFactor = .destinationAlpha
        pipelineDescriptor.colorAttachments[0].sourceAlphaBlendFactor = .destinationAlpha
        pipelineDescriptor.colorAttachments[0].destinationRGBBlendFactor = .oneMinusSourceAlpha
        pipelineDescriptor.colorAttachments[0].destinationAlphaBlendFactor = .oneMinusBlendAlpha
        do {
            pipelineState = try Renderer.device.makeRenderPipelineState(descriptor: pipelineDescriptor)
        } catch let error {
            fatalError(error.localizedDescription)
        }
        
        
        // кремовый фон
        //        metalView.clearColor = MTLClearColor(red: 1.0, green: 1.0, blue: 0.8, alpha: 1.0)
        
        metalView.delegate = self
        
        
        // Textures
        let loader = MTKTextureLoader(device: Renderer.device)
        //        let url = NSBundle.mainBundle().URLForResource("pic", withExtension: "jpg")!
        //        let texture = try! loader.newTextureWithContentsOfURL(url, options: nil)
        
        // static const char* sMainTxName = "f_prm_cel.pvr.ccz"; // f_prm_cel_thin
        // static const char* sGradTxName = "f_grad_cel.pvr.ccz"; // f_grad_cel_blue
        // static const char* sWindTxName = "f_disp_cel.pvr.ccz";
        //        let options: [MTKTextureLoader.Option : Any] = [.generateMipmaps : true, .SRGB : true]
        let options: [MTKTextureLoader.Option : Any] = [MTKTextureLoader.Option.origin : MTKTextureLoader.Origin.topLeft.rawValue, .SRGB : false]
        let optionsForWind: [MTKTextureLoader.Option : Any] = [MTKTextureLoader.Option.origin : MTKTextureLoader.Origin.topLeft.rawValue]
        do {
            mainTexture = try loader.newTexture(name: "f_prm_cel", scaleFactor: 1.0, bundle: nil, options: options)
            windTexture = try loader.newTexture(name: "f_disp_cel", scaleFactor: 1.0, bundle: nil, options: options)
            gradTexture = try loader.newTexture(name: "f_grad_cel", scaleFactor: 1.0, bundle: nil, options: options)
        } catch let error {
            fatalError(error.localizedDescription)
        }
    }
    
    func createQuadVertDescriptor() {
        // Создаем vertexDescriptor, который будет хранить параметры,
        // кот. нужно знать объекту.
        // После создания объекта, дескриптор будет больше не нужен.
        // Один и тот же дескриптор можно использовать для нескольких объектов.
        quadVertDescriptor = MTLVertexDescriptor()
        // obj-файл содержит координаты нормалей и текстурные координаты
        // так же как и данные о позициях вершин.
        // В данный момент нам не нужны нормали поверхностей или текстурные координаты,
        // Сейчас нужны только позиции.
        // Мы говорим дескриптору, что позиции нужно закружать как 3 флоата.
        // Всего в MTLVertexDescriptor содержится 31 атрибут.
        quadVertDescriptor.attributes[0].format = .float3
        // Смещение, которое говорит где начинаются нужные данные в буффере.
        quadVertDescriptor.attributes[0].offset = 0
        // Когда мы отправляем данные о вершинах на GPU через renderEncoder,
        // мы отправляем их в MTLBuffer и идентифицируем нужный буффер по индексу.
        // Всего есть 31 доступных буферов.
        // Metal отслеживает их в таблице аргументов буфера.
        // Используем буфер 0, чтобы функция вершинного шейдера
        // могла сопоставлять входящие данные вершины в буфере 0 с этим макетом вершины (vertex layout).
        quadVertDescriptor.attributes[0].bufferIndex = 0
        
        // UV
        quadVertDescriptor.attributes[1].format = .float2
        quadVertDescriptor.attributes[1].offset = 12
        quadVertDescriptor.attributes[1].bufferIndex = 0
        
        // Здесь мы задаем шаг для буфера 0.
        // Шаг это число байт между информацией о разных вершинах.
        // Если шаг N, то значит N байт это инфа об одной вершине, а след. N байт о следующей.
        // Например, если инфа содержит инф-ю о позиции, нормалях и текстурных координатах,
        // то шаг будет float3 + float3 + float2.
        // Т.к. мы загружаем только только позицию, то шаг float3.
        quadVertDescriptor.layouts[0].stride = MemoryLayout<SIMD3<Float>>.stride + MemoryLayout<SIMD2<Float>>.stride
        
        //        // ModelI/O требует немного другой дескриптор, поэтому необходима конвертация.
        //        let meshDescriptor = MTKModelIOVertexDescriptorFromMetal(quadVertDescriptor)
        //        // Присваиваем атрибуту имя “position”.
        //        // Это говорит Model I/O, что здесь данные о позициях.
        //        // Нормали и текстурные координаты также доступны,
        //        // но этим вершинным дескриптором мы говорим Model I/O что нам не интересны эти данные.
        //        (meshDescriptor.attributes[0] as! MDLVertexAttribute).name = MDLVertexAttributePosition
        //        (meshDescriptor.attributes[1] as! MDLVertexAttribute).name = MDLVertexAttributeTextureCoordinate
    }
    
    func createCubeMesh() {
        mdlMesh = Primitive.makeCube(device: Renderer.device, size: 1)
        do {
            mesh = try MTKMesh(mesh: mdlMesh, device: Renderer.device)
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func createTrainMesh() {
        // Будем импортировать модель из файла.
        // создаем урл к файлу
        let objName = "train"
        guard let assetURL = Bundle.main.url(forResource: objName, withExtension: "obj") else {
          fatalError()
        }
        
        // Создаем vertexDescriptor, который будет хранить параметры,
        // кот. нужно знать объекту.
        // После создания объекта, дескриптор будет больше не нужен.
        // Один и тот же дескриптор можно использовать для нескольких объектов.
        let vertexDescriptor = MTLVertexDescriptor()
        // obj-файл содержит координаты нормалей и текстурные координаты
        // так же как и данные о позициях вершин.
        // В данный момент нам не нужны нормали поверхностей или текстурные координаты,
        // Сейчас нужны только позиции.
        // Мы говорим дескриптору, что позиции нужно закружать как 3 флоата.
        // Всего в MTLVertexDescriptor содержится 31 атрибут.
        vertexDescriptor.attributes[0].format = .float3
        // Смещение, которое говорит где начинаются нужные данные в буффере.
        vertexDescriptor.attributes[0].offset = 0
        // Когда мы отправляем данные о вершинах на GPU через renderEncoder,
        // мы отправляем их в MTLBuffer и идентифицируем нужный буффер по индексу.
        // Всего есть 31 доступных буферов.
        // Metal отслеживает их в таблице аргументов буфера.
        // Используем буфер 0, чтобы функция вершинного шейдера
        // могла сопоставлять входящие данные вершины в буфере 0 с этим макетом вершины (vertex layout).
        vertexDescriptor.attributes[0].bufferIndex = 0

        // Здесь мы задаем шаг для буфера 0.
        // Шаг это число байт между информацией о разных вершинах.
        // Если шаг N, то значит N байт это инфа об одной вершине, а след. N байт о следующей.
        // Например, если инфа содержит инф-ю о позиции, нормалях и текстурных координатах,
        // то шаг будет float3 + float3 + float2.
        // Т.к. мы загружаем только только позицию, то шаг float3.
        vertexDescriptor.layouts[0].stride = MemoryLayout<SIMD3<Float>>.stride
        // ModelI/O требует немного другой дескриптор, поэтому необходима конвертация.
        let meshDescriptor = MTKModelIOVertexDescriptorFromMetal(vertexDescriptor)
        // Присваиваем атрибуту имя “position”.
        // Это говорит Model I/O, что здесь данные о позициях.
        // Нормали и текстурные координаты также доступны,
        // но этим вершинным дескриптором мы говорим Model I/O что нам не интересны эти данные.
        (meshDescriptor.attributes[0] as! MDLVertexAttribute).name = MDLVertexAttributePosition
        
        
        let allocator = MTKMeshBufferAllocator(device: Renderer.device)
        
        // По заданным урлу, дескриптору и аллокатору,
        // открываем файл модели.
        let asset = MDLAsset(url: assetURL,
                             vertexDescriptor: meshDescriptor,
                             bufferAllocator: allocator)
        // Затем считываем эти данные в ModelI/O-меш.
        mdlMesh = asset.object(at: 0) as! MDLMesh

        // конвертируем меш в MTKMesh
        mesh = try! MTKMesh(mesh: mdlMesh, device: Renderer.device)
    }
}

// расширяем класс Renderer, чтобы он соответстовал MTKViewDelegate
extension Renderer: MTKViewDelegate {
    // будет вызываться каждый раз, когда меняется размер окна.
    // Это позволит обновить координатную систему рендера.
    func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) {
    }
    
    // вызывается каждый кадр.
    func draw(in view: MTKView) {
        // print("draw")
        
        guard let descriptor = view.currentRenderPassDescriptor,
            let commandBuffer = Renderer.commandQueue.makeCommandBuffer(),
            let renderEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: descriptor)
        else {
            return
        }
        
        //        // Каждый кадр увеличиваем таймер.
        //        timer += 0.05
        //        // Чтобы получить позицию от -1 до 1 используем синус.
        //        var currentTime = sin(timer)
        //        // Если вы отправляете только небольшое количество данных (меньше 4kb) в GPU,
        //        // то setVertexBytes(_:length:index:) будет альтернативой установке MTLBuffer.
        //        // Здесь мы устанавливаем currentTime на индекс 1 в таблице аргументов буфера.
        //        renderEncoder.setVertexBytes(&currentTime,
        //                                      length: MemoryLayout<Float>.stride,
        //                                      index: 1)
        
       
        // void FireEffect::onTimer()
        let speed: Float = 0.03;
        fragmentUniforms.uTime += speed * 0.1
        if (fragmentUniforms.uTime > 10.0) {
            fragmentUniforms.uTime -= 10.0
        }
        
        // drawing code goes here
        renderEncoder.setRenderPipelineState(pipelineState)
        
        //        renderEncoder.setVertexBuffer(vertexBuffer, offset: 0, index: 0)
        //        for submesh in mesh.submeshes {
        //            renderEncoder.drawIndexedPrimitives(type: .triangle,
        //                                                 indexCount: submesh.indexCount,
        //                                                 indexType: submesh.indexType,
        //                                                 indexBuffer: submesh.indexBuffer.buffer,
        //                                                 indexBufferOffset: submesh.indexBuffer.offset)
        //        }
        
        renderEncoder.setVertexBuffer(quadVertBuffer, offset: 0, index: Int(BufferIndexVertices.rawValue))
        renderEncoder.setFragmentBytes(&fragmentUniforms,
                                       length: MemoryLayout<FragmentUniforms>.stride,
                                       index: Int(BufferIndexFragmentUniforms.rawValue))
        
        renderEncoder.setFragmentTexture(mainTexture, index: Int(MainTexture.rawValue))
        renderEncoder.setFragmentTexture(windTexture, index: Int(WindTexture.rawValue))
        renderEncoder.setFragmentTexture(gradTexture, index: Int(GradTexture.rawValue))

        renderEncoder.drawPrimitives(type: .triangleStrip, vertexStart: 0, vertexCount: quadVertices.count)
        
        renderEncoder.endEncoding()
        guard let drawable = view.currentDrawable else {
            return
        }
        commandBuffer.present(drawable)
        commandBuffer.commit()
    }
}
