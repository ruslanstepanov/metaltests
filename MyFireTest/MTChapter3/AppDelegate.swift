//
//  AppDelegate.swift
//  MTChapter3
//
//  Created by Ruslan Stepanov on 01/11/2019.
//  Copyright © 2019 Ruslan Stepanov. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

