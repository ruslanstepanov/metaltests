//
//  Primitive.swift
//  MTChapter3
//
//  Created by Ruslan Stepanov on 05/11/2019.
//  Copyright © 2019 Ruslan Stepanov. All rights reserved.
//

import MetalKit

class Primitive {
    // Создаем меш куба
    class func makeCube(device: MTLDevice, size: Float) -> MDLMesh {
        let allocator = MTKMeshBufferAllocator(device: device)
        let mesh = MDLMesh(boxWithExtent: [size, size, size],
                            segments: [1, 1, 1],
                            inwardNormals: false, geometryType: .triangles,
                            allocator: allocator)
        return mesh
    }
}
