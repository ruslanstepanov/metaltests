//
//  ViewController.swift
//  MTChapter3
//
//  Created by Ruslan Stepanov on 01/11/2019.
//  Copyright © 2019 Ruslan Stepanov. All rights reserved.
//

import Cocoa
import MetalKit

class ViewController: NSViewController {
    var renderer: Renderer?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        guard let metalView = view as? MTKView else {
          fatalError("metal view not set up in storyboard")
        }
        
        // Инициализируем Рендерер.
        // Каждый кадр, будет вызываться метод renderer.draw
        // В нем мы будем производить отрисовку.
        renderer = Renderer(metalView: metalView)
        
        // Как мы делали раньше,
        // теперь нужно проивести настройку окружения Metal.
        // Главное преимущество Metal перед OpenGL в том, что
        // некоторые объекты можно создать один раз заранее, а не каждый кадр.
        // Список таких объектов:
        // MTLDevice (ссылка на GPU),
        // MTLCommandQueue (отвечает за создание и организацию MTLCommandBuffers на каждом кадре),
        // MTLLibrary (здесь хранятся исходные коды для вертексного и фрагментного шейдеров);
        // MTLRenderPipelineState'ы (может быть несколько) (устанавливает инфу для отрисовки:
        //      какой шейдер использовать, настройки глубины и цвета, как читать данные о вершинах)
        // MTLBuffer'ы (может быть несколько) (хранит инфу, например о вершинах, в формате пригодном для отправки на GPU)
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}

