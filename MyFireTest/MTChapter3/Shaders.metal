//
//  Shaders.metal
//  MTChapter3
//
//  Created by Ruslan Stepanov on 05/11/2019.
//  Copyright © 2019 Ruslan Stepanov. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;
#import "Common.h"

// Структура для описания атрибутов вершины,
// которые совпадают с вертексным-дескриптором.
// В нашем случае, только позиция.
struct VertexIn {
    //  attribute QUALITY vec4 a_position;
    float3 position [[ attribute(Position) ]];
    // attribute QUALITY vec2 a_texCoord;
    //    float2 uv [[ attribute(UV) ]];
};

// Структура возвращаемая вершинным шейдерм
struct VertexOut {
  float4 position [[ position ]];
  // текстурные координаты - будем отправлять в фрагментный шейдер
  // varying QUALITY vec2 v_texCoord;
  float2 uv;
};

// Реализация vertex-шейдера, который принимает структуры VertexIn и возвращает позиции вершины как float4.
// Вершины индексируются в vertex-буфере.
// vertex-шейдер получает текущий индекс через атрибут [[ stage_in ]]
// и распаковывает VertexIn для вершины с текущим индексом.
// Также здесь мы получаем timer как float из буфера 1.
vertex VertexOut vertex_main_my(const VertexIn vertexIn [[ stage_in ]]) {
    VertexOut out;
    // gl_Position = CC_MVPMatrix * a_position;
    out.position = float4(vertexIn.position, 1);
    // v_texCoord = a_texCoord;
    //    out.uv = vertexIn.uv;
    return out;
}
// from chapter 4
vertex VertexOut vertex_main(constant float3 *vertices [[ buffer(0) ]],
                             uint id [[ vertex_id ]])
{
    // извлекаем нужную позицию из массива
    VertexOut vertex_out;
    // преобразуем позицию в float4 и применяем матрицу преобразования
    vertex_out.position = float4(vertices[id], 1);
    vertex_out.uv = float2(vertex_out.position[0]/2+0.5, 1-vertex_out.position[1]/2+0.5);
    return vertex_out;
}

/*
 uniform QUALITY sampler2D uMainTexture;
 uniform QUALITY sampler2D uWindTexture;
 uniform QUALITY sampler2D uGradTexture;

 // задаются из кода, каждый кадр
 uniform QUALITY float uTime;
 uniform QUALITY float uSaturation;
 uniform QUALITY float uWindDisplacement;
 */

fragment float4 fragment_main(VertexOut in [[stage_in]],
                              texture2d<float> uMainTexture [[ texture(MainTexture) ]],
                              texture2d<float> uWindTexture [[ texture(WindTexture) ]],
                              texture2d<float> uGradTexture [[ texture(GradTexture) ]],
                              constant FragmentUniforms &fragmentUniforms [[ buffer(BufferIndexFragmentUniforms)]]) {
    // varying QUALITY vec2 v_texCoord;
    float2 v_texCoord = in.uv;
    //  uniform QUALITY float uTime;
    float uTime = fragmentUniforms.uTime;
    //  uniform QUALITY float uSaturation;
    float uSaturation = fragmentUniforms.uSaturation;
    //  uniform QUALITY float uWindDisplacement;
    float uWindDisplacement = fragmentUniforms.uWindDisplacement;
    
    // берем цвет пикселя из текстуры
    // textureSampler - управляет тем как брать тексели,
    // например, параметр filter::linear говорит сгладить пиксели, чтобы не было шакалов
    // filter::nearest - оставит шакальные пиксели
    // address::repeat - замостить тайлами всю поверхносьь
    constexpr sampler textureSampler(min_filter::linear, mag_filter::linear, s_address::repeat, t_address::repeat);
    //    constexpr sampler textureSampler(filter::linear, address::repeat);
    
    // debug test
    //    return uMainTexture.sample(textureSampler, v_texCoord);
    //    return uWindTexture.sample(textureSampler, v_texCoord);
    //    return uGradTexture.sample(textureSampler, v_texCoord);
    
    // float blue = texture2D(uMainTexture, vec2(v_texCoord.x, v_texCoord.y)).b;
    float blue = uMainTexture.sample(textureSampler, v_texCoord).b;
    
    // vec2 windTex = texture2D(uWindTexture, vec2(v_texCoord.x, v_texCoord.y + uTime * 0.7)).rb;
    float2 windTex = uWindTexture.sample(textureSampler, float2(v_texCoord[0], v_texCoord[1] + uTime * 0.7)).rb;
    //    windTex = windTex * 2.0 - 1.0;
    windTex = windTex * 2.0 - 1.0;
    //    windTex *= blue * uWindDisplacement * 0.3;
    windTex *= blue * uWindDisplacement * 0.3;
    
    //    float red = texture2D(uMainTexture, vec2(v_texCoord.x, v_texCoord.y)).r;
    float red = uMainTexture.sample(textureSampler, v_texCoord).r;
    //    float green = texture2D(uMainTexture, vec2(v_texCoord.x + windTex.x, v_texCoord.y + uTime + windTex.y)).g;
    float green = uMainTexture.sample(textureSampler, float2(v_texCoord[0] + windTex[0], v_texCoord[1] + uTime + windTex[1])).g;
    
    //    float displacement = (red * green) * uSaturation;
    float displacement = (red * green) * uSaturation;
    
    //    vec2 fireTexCoord = vec2(0.01 + displacement, 0.5);
    float2 fireTexCoord = float2(0.01 + displacement, 0.5);
    //    vec4 fireColor = texture2D(uGradTexture, fireTexCoord);
    float4 fireColor = uGradTexture.sample(textureSampler, fireTexCoord);
    
    return fireColor;
}
