//
//  Gloabal.h
//  HelloWorld
//
//  Created by Aleksander Alexander on 26.07.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#ifndef LOST_GLOBAL_H
#define LOST_GLOBAL_H

#undef TYPE_BOOL

#include "cocos2d.h"
//#include "Cocos2d-x-1.h"
//#include "GameAsserts/GameAssert.h"
#include <string>
#include <cstdint>
#include <vector>

#include <set>
#include <cmath>
#include <valarray>
#include <unordered_map>

#include "IELog.hpp"

#define P_ANDROID (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#define P_IOS (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#define P_MAC (CC_TARGET_PLATFORM == CC_PLATFORM_MAC)

enum LoginState {
    //    LS_Startup,
    LS_InProcess,
    LS_Online,
    LS_Offline
};

enum class TextAlign : int;
namespace cc = cocos2d;

#define KEY_SOUND_OFF "sound_disable"
#define KEY_MUSIC_OFF "music_disable"

#define KEY_SOUND_VOLUME "sound_volume"
#define KEY_MUSIC_VOLUME "music_volume"

#pragma mark sounds
#define SND_ROOT "sound/"
#define SOUND_EXP ".mp3"

#define SND_ITEM_DROP SND_ROOT "drop" SOUND_EXP
#define SND_LEVEL_UP SND_ROOT "levelup" SOUND_EXP

#define SHADERS_ROOT "shaders/"
#define ATLASES_ROOT "atlases/"

#define GUI_EXP "webp"

#define GUI_ICON_9 "slot.png"
#define GUI_SLOT_9 "slot.png"
#define GUI_INFO_9 "window_inner_bg.png"
#define GUI_INFO_TOP_9 "bg_top.webp"
#define GUI_WINDOW_9 "window_bg.png"
#define GUI_BTN_3 "button.png"
#define GUI_HINT_BUBBLE "craft_window.png"
#define GUI_BTN_3_MIN "button_mini.png"
#define GUI_BTN_3_YES "button.png"
#define GUI_BTN_3_NO "button_red.png"
#define GUI_BTN_3_NO_MINI "button_mini_red.png"
#define GUI_TUTOR_HINT_9 "tutor_hint.webp"
#define GUI_MIN_NUM_3 "mininum.webp"
#define GUI_GRIN_FRAME_3 "green3x.webp"
#define GUI_SLOT_3 "slot3x.webp"
#define GUI_ICON_BOT_3 "bbar.webp" // bottom
#define GUI_ICON_COUNTER_3 "num.webp"
#define GUI_FB_FRIEND_PHOTO_FRAME "fb_friends_photo_frame.webp"
#define GUI_RATING_SLOT_3 "rating_slot.webp"

const float LANDSCAPE_Z_START = -1.0f;

//!#define USE_PREPARED

#define PI 3.14159265358979f
#define MATH_E 2.71828183f

#ifndef INT16_MAX
#define INT16_MAX 32767
#endif

#ifndef UINT32_MAX
#define UINT32_MAX 4294967295U
#endif

#define UNLIM -1

/// недопустимое значение == int(-1)
constexpr int NONE = -1;

//#define __ENGINE__WARNING__	if(1){int _ENGINE_WARNING_ = 0;}
#define __ENGINE__WARNING__

#define Random(min, max) ((float)((float)rand() / (float)RAND_MAX) * (max - min) + min)

#define FRandom(min, max) ((float)((float)rand() / (float)RAND_MAX) * ((max) - (min)) + (min))
#define IRandom(min, max) ((min) + (rand() % (int)((max) - (min) + 1)))

#define RadToDeg(a) (a / PI * 180.0f)

#define __TOKENPASTE(x) #x
#define __TOKENPASTE2(x) __TOKENPASTE(x)
#define __LINE__STR__ __TOKENPASTE2(__LINE__)

#define __FUNCTION__LOG__ cocos2d::log("%s", __PRETTY_FUNCTION__);

/// в протобуфе id словарь, одно полей std::uint64_t хронящий прототип - (BK_, RES_, ...)
typedef std::uint64_t PrototypeId;
/// в протобуфе id словарь, одно полей std::int64_t хронящий инстанс - (идентификатор конкретного человека, домика,
/// кучки ресурсов...)
typedef std::int64_t InstanceId;

typedef std::uint16_t ExtensionId;

typedef std::wstring WString;
typedef std::vector<std::wstring> WStringVec;

typedef std::vector<std::string> StringVec;
typedef std::set<std::string> StringSet;
typedef std::map<std::string, std::string> StringMap;
typedef std::vector<unsigned char> UCharVec;
typedef std::vector<char> ByteVec;

typedef std::vector<int> IntVec;
typedef std::vector<uint64_t> UIntVec64;
typedef std::set<int> IntSet;
typedef std::set<uint64_t> UIntSet64;

typedef std::vector<InstanceId> InstanceIdVec;
typedef std::vector<PrototypeId> PrototypeIdVec;
typedef std::set<InstanceId> InstanceIdSet;
typedef std::set<PrototypeId> PrototypeIdSet;

typedef std::vector<cocos2d::Point> PointVec;

typedef std::set<int> IndexSet;
typedef std::vector<int> IndexVec;
typedef std::valarray<double> DoubleArr;
typedef std::vector<double> DoubleVec;
typedef std::vector<float> FloatVec;

typedef std::vector<float> FloatVec;
typedef std::vector<FloatVec> FloatVecVec;
typedef std::vector<IntVec> IntVecVec;
typedef std::vector<bool> BitVec;
typedef std::vector<StringVec> StringVecVec;

typedef unsigned int uint;
typedef unsigned char uchar;

#define tM(x) ((x)*60.0)
#define tH(x) ((x)*tM(60.0))
#define tD(x) ((x)*tH(24.0))

template <class T, class... Args> std::unique_ptr<T> make_unique(Args&&... args)
{
    return std::unique_ptr<T>(new (std::nothrow) T(std::forward<Args>(args)...));
}

#endif
