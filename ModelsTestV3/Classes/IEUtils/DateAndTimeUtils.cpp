//
//  DateAndTimeUtils.cpp
//  Exeriment
//
//  Created by d.antonov on 25.08.16.
//  Copyright © 2016 Nexters. All rights reserved.
//
#include "DateAndTimeUtils.h"
//#include "GameSession.h"
#include <sys/time.h>
#include <iostream>
#include <chrono>
#include <ctime>

namespace ie {
// _________________________________________________________________________________________
double time()
{
    auto current_time = std::chrono::system_clock::now();
    auto duration_in_seconds = std::chrono::duration<double>(current_time.time_since_epoch());
    double num_seconds = duration_in_seconds.count();
    return num_seconds;
    //    if (SGameSession())
    //        return SGameSession()->getCurrentTimeDouble();
    return 0;
}
// _________________________________________________________________________________________
time_t timeSec()
{
    std::time_t time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    return time;
    //    if (SGameSession())
    //        return SGameSession()->getCurrentTime();
    return 0;
}
// _________________________________________________________________________________________
uint8_t centiSec()
{
    //    if (SGameSession())
    //        return uint8_t(SGameSession()->getSecFracPart() * 100.0);
    return 0;
}
// _________________________________________________________________________________________
double timeLocal()
{
    double cur = 0.0;
    timeval now_time;

    if (gettimeofday(&now_time, NULL) != 0) {
        //ie::log("ie::timeLocal - error in gettimeofday");
        return 0;
    }

    // Get new delta time
    cur = now_time.tv_sec + now_time.tv_usec / 1000000.0;

    return cur;
}
// _________________________________________________________________________________________
int getYear()
{
    // [1900 - present year]
    time_t serverTime = timeSec();//SGameSession()->getCurrentTime();
    const struct tm date = *gmtime(&serverTime);

    return date.tm_year + 1900;
}
// _________________________________________________________________________________________
int getMonth()
{
    // [1 - 12]
    time_t serverTime = timeSec();//SGameSession()->getCurrentTime();
    const struct tm date = *gmtime(&serverTime);

    return date.tm_mon + 1;
}
// _________________________________________________________________________________________
int getDay()
{
    // [1 - 31]
    time_t serverTime = timeSec();//SGameSession()->getCurrentTime();
    const struct tm date = *gmtime(&serverTime);

    return date.tm_mday;
}
// _________________________________________________________________________________________
int getHour()
{
    // [0 - 23]
    time_t serverTime = timeSec();//SGameSession()->getCurrentTime();
    const struct tm date = *gmtime(&serverTime);

    return date.tm_hour;
}
// _________________________________________________________________________________________
int getMinutes()
{
    // [0 - 59]
    time_t serverTime = timeSec();//SGameSession()->getCurrentTime();
    const struct tm date = *gmtime(&serverTime);

    return date.tm_min;
}
// _________________________________________________________________________________________
int getSeconds()
{
    // [0 - 59]
    time_t serverTime = timeSec();//SGameSession()->getCurrentTime();
    const struct tm date = *gmtime(&serverTime);

    return date.tm_sec;
}
// _________________________________________________________________________________________
time_t localToGMT(time_t local)
{
    struct tm* gtm = gmtime(&local);
    return mktime(gtm);
}
// _________________________________________________________________________________________
time_t gmtToLocal(time_t sec)
{
    struct tm localTime = *localtime(&sec);
    struct tm gmtTime = *gmtime(&sec);

    time_t t1 = mktime(&localTime);
    time_t t2 = mktime(&gmtTime);

    return 2 * t1 - t2;
}
// _________________________________________________________________________________________
} // namespace ie
