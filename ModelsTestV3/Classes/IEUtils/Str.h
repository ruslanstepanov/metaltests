//
//  Str.h
//  LostChapters
//
//  Created by Gleb Kolobkov on 01.07.15.
//  Copyright (c) 2015 Crazy Bit. All rights reserved.
//

#pragma once
#include <sstream>
#include <array>
#include <type_traits>
#include <stdexcept>

namespace ie {
namespace str {

    template <typename T> std::string toStr(T value)
    {
        std::stringstream ss;
        ss << value;
        return ss.str();
    }
} // namespace str
} // namespace ie
