//
//  StringFormatUtils.cpp
//  Exeriment
//
//  Created by d.antonov on 25.08.16.
//  Copyright © 2016 Nexters. All rights reserved.
//
#include "StringFormatUtils.h"
#include "StringUtils.h"
#include "ThreadUtils.h"

static const int sStringBufSize = 1024;

// _________________________________________________________________________________________
typedef std::string::value_type charT;
charT upper(charT arg)
{
    return std::use_facet<std::ctype<charT>>(std::locale()).toupper(arg);
}
// _________________________________________________________________________________________
const char* ie::formatBuff(const char* pszFormat, ...)
{
    if (!ie::isMainThread()) {
        std::string str("it can only execuit in main thread: ");
        str += pszFormat;
        //        IEAssert(false, str.c_str());
    }

    static char czBuf[sStringBufSize];

    va_list ap;
    va_start(ap, pszFormat);

    vsnprintf(czBuf, sStringBufSize, pszFormat, ap);
    va_end(ap);

    return &czBuf[0];
}
// _________________________________________________________________________________________
//const char* ie::formatBuff(StringTag tag, ...)
//{
//    const char* pszFormat = TEXT_C(tag);
//
//    if (!ie::isMainThread()) {
//        std::string str("it can only execuit in main thread: ");
//        str += pszFormat;
//        IEAssert(false, str.c_str());
//    }
//
//    static char czBuf[sStringBufSize];
//
//    va_list ap;
//    va_start(ap, tag);
//
//    vsnprintf(czBuf, sStringBufSize, pszFormat, ap);
//    va_end(ap);
//
//    return czBuf;
//}
// _________________________________________________________________________________________
std::string ie::strf(const char* pszFormat, ...)
{
    va_list ap;
    va_start(ap, pszFormat);

    char szBuf[sStringBufSize];

    //	szBuf = (char*)malloc( sStringBufSize * sizeof(char) );

    int writedSize = vsnprintf(szBuf, sStringBufSize, pszFormat, ap);
    va_end(ap);

    std::string ret = std::string(szBuf);

    if (writedSize < 0 || writedSize == sStringBufSize) {
        ie::log("ie::strf error");
    }
    if (writedSize > sStringBufSize) {
    //        LogicError("ie::strf unexpected behavior");
    }

    return ret;
}
// _________________________________________________________________________________________
char* ie::formatBigNumber(unsigned long number)
{
    static char bigNum[16];
    if (number > 100000L) {
        snprintf(bigNum, 16, "%ldk", number / 1000);
    }
    else {
        snprintf(bigNum, 16, "%ld", number);
    }

    return &bigNum[0];
}
// _________________________________________________________________________________________
std::string ie::encodeValue(std::string key, int value)
{
#define MAX_BUFFER_SIZE 512
    std::string retValue;
    int summ = 0;
    for (int i = 0; i < key.length(); i++) {
        summ += key[i];
    }
    int base = 10; // summ % 30 + 6;

    char buffer[MAX_BUFFER_SIZE];
    retValue = itoa(value, buffer, base);
    return retValue;
}
// _________________________________________________________________________________________
int ie::decodeValue(std::string key, std::string value)
{
    int retValue = 0;
    int summ = 0;

    for (int i = 0; i < key.length(); i++) {
        summ += key[i];
    }

    retValue = (int)strtol(value.c_str(), NULL, 10); //(summ % 30) + 6);

    return retValue;
}
// _________________________________________________________________________________________
/**
 * C++ version 0.4 char* style "itoa":
 * Written by Lukás Chmela
 * Released under GPLv3.
 */
char* ie::itoa(int value, char* result, int base)
{
    // check that the base if valid
    if (base < 2 || base > 36) {
        *result = '\0';
        return result;
    }

    char *ptr = result, *ptr1 = result, tmp_char;
    int tmp_value;

    do {
        tmp_value = value;
        value /= base;
        *ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz"[35
            + (tmp_value - value * base)];
    } while (value);

    // Apply negative sign
    if (tmp_value < 0)
        *ptr++ = '-';
    *ptr-- = '\0';
    while (ptr1 < ptr) {
        tmp_char = *ptr;
        *ptr-- = *ptr1;
        *ptr1++ = tmp_char;
    }
    return result;
}
// _________________________________________________________________________________________
std::string& ie::uppercase(std::string& str)
{
    std::transform(str.begin(), str.end(), str.begin(), upper);

    int size = (int)str.size();

    for (int i = 0; i < size; i++) {
        unsigned char c = str[i];
        if (c == 0xD1 || c == 0xD0 || c == 0xC3) {
            unsigned short newC = ie::eurUppercase(str[i], str[i + 1]);
            if (newC) {
                str[i] = HIBYTE(newC);
                str[i + 1] = LOWBYTE(newC);
            }
        }
    }

    return str;
}
// _________________________________________________________________________________________
std::string& ie::lowcaseLat(std::string& str)
{ // only for english
    int count = (int)str.size(); // FIXME: AUTO-FIX (fixed: 13)
    for (int i = 0; i < count; i++) {
        char& c = str[i];
        if (c <= 'Z' && c >= 'A')
            c = c - ('Z' - 'z');
    }

    return str;
}
// _________________________________________________________________________________________
WString& ie::lowcaseLat(WString& str)
{
    int length = (int)str.length(); // FIXME: AUTO-FIX (fixed: 14)
    for (int i = 0; i < length; i++) {
        wchar_t c = str[i];
        if (c >= L'A' && c <= L'Z') {
            str[i] = c - (L'Z' - L'z');
        }
    }
    return str;
}
// _________________________________________________________________________________________
// _________________________________________________________________________________________
/// Возвращает в зависимости от count, индекс нужного окончания (0, 1 или 2)
int getSuffixNum(time_t count)
{
    return 0;
    //    if (SLocale()->getLang() != Locale::Lang::RUS)
    //        return count == 1 ? 0 : 1;
    //
    //    time_t rest = (count >= 10 && count <= 20) ? count : count % 10;
    //    if (rest > 0) {
    //        if (rest < 2) {
    //            return 0;
    //        }
    //        else if (rest < 5) {
    //            return 1;
    //        }
    //    }
    //    return 2;
}
// _________________________________________________________________________________________
std::string ie::getSuffixVariation(const std::string& pattern, time_t count)
{
    std::string str(pattern);
    auto beginSuffix = str.find("[");
    auto endSuffix = str.find("]");
    if (beginSuffix != std::string::npos && endSuffix != std::string::npos && beginSuffix < endSuffix) {
        std::string suffix;
        int curSuffix = 0;
        auto numSuffix = getSuffixNum(count);
        for (auto i = beginSuffix + 1; i < endSuffix; ++i) {
            if (str[i] == ',')
                ++curSuffix;
            else if (numSuffix == curSuffix)
                suffix += str[i];
        }
        if (curSuffix == 2)
            str.replace(beginSuffix, endSuffix - beginSuffix + 1, suffix);
    }
    auto beginCount = str.find("%d");
    return beginCount == std::string::npos ? str : str.replace(beginCount, 2, strf("%d", count));
}
// _________________________________________________________________________________________
std::string ie::getDurationWithSuffix(time_t time)
{
    static constexpr time_t ONE_MINUTE = 60;
    static constexpr time_t ONE_HOUR = 60 * ONE_MINUTE;
    static constexpr time_t ONE_DAY = 24 * ONE_HOUR;

    if (time > ONE_DAY)
        return std::to_string(static_cast<int>(time / ONE_DAY));// ie::getSuffixVariation(TEXT(STR_D_DAY), static_cast<int>(time / ONE_DAY));
    else if (time > ONE_HOUR)
        return std::to_string(static_cast<int>(time / ONE_HOUR));// ie::getSuffixVariation(TEXT(STR_D_HOURS), static_cast<int>(time / ONE_HOUR));
    else
        return std::to_string(static_cast<int>(time / ONE_MINUTE));// ie::getSuffixVariation(TEXT(STR_D_MINUTES), static_cast<int>(time / ONE_MINUTE));
}
// _________________________________________________________________________________________
