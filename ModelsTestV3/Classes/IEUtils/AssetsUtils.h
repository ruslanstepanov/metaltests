//
//  AssetsUtils.h
//  Exeriment
//
//  Created by d.antonov on 26.08.16.
//  Copyright © 2016 Nexters. All rights reserved.
//
#pragma once

#include "Global.h"

#define PFX_ASSETS "assets_pi/"
#define PFX_ASSETS_ANDROID "assets/"
#define PFX_PRIMARY "primary/"
#define PFX_SECONDARY "secondary/"
#define PFX_WEB "web/"
#define PFX_ASSETS_PRIMARY PFX_ASSETS PFX_PRIMARY
#define PFX_ASSETS_SECONDARY PFX_ASSETS PFX_SECONDARY

namespace ie {
/// есть ли в ресурсах такой файл, относительный путь ищется с учетом
/// CCFileUtils::m_searchResolutionsOrderArray
bool isBundleFileExist(const std::string& fileName);
/// есть ли такой файл относительно пути (bundle|writepath)/assets/(primary|secondary)
bool isAssetsFileExist(const std::string& fileName);
std::string getAssetsAbsoluteFilePath(const std::string& fileName);
/// есть ли в WritablePath такой файл, (путь абсолютный)
bool isUserPathFileExist(const std::string& path); // Нигде не используется!
/// есть ли в WritablePath такой файл, (путь относительно WritablePath)
bool isUserFileExist(const std::string& fileName);

bool renameSettingsFiles(const std::string& inputFileName, const std::string& outputFileName);
} // namespace ie
