//
//  StringUtils.h
//  Exeriment
//
//  Created by d.antonov on 25.08.16.
//  Copyright © 2016 Nexters. All rights reserved.
//
#pragma once

#include "Global.h"

extern const std::string StringZero;
extern const StringVec StringVecZero;
extern const StringMap StringMapZero;

namespace ie {
WString& mbToWStr(const std::string& inStr, WString& outWStr);
std::string& wToMbStr(const std::wstring& inStr, std::string& outWStr);
/// принимает псевдосимвола из мульти байт строки и если они являются
/// символом unicode поддерживающим смену регистра, меняет регистр î -> Î
/// к односимвольным латинским опперация не применима
unsigned short eurUppercase(unsigned char c1, unsigned char c2);
/// Разбение строк
StringVec splitString(const std::string& str, char delimeter);
const StringVec& splitStringBuf(const std::string& str, char delimeter);
const StringVec& splitString(const std::string& str, const std::string& delimeter);
WStringVec* newSplitStringW(const WString& str, wchar_t delimeter);
/// склеить строки через разделитель
std::string joinStrings(const StringVec& strings, const char delimeter);
std::string joinStrings(const StringVec& strings, const std::string& delimeter);
StringVec joinStringVecs(const StringVec& strings1, const StringVec& strings2, bool dropDuplicates);

void replaceAll(std::string& where, const std::string& what, const std::string& onWhat);
std::string replaceAll(const std::string& where, const std::string& what, const std::string& onWhat);
void replaceAllW(WString& where, const WString& what, const WString& onWhat);
WString replaceAllW(const WString& where, const WString& what, const WString& onWhat);

bool stringContains(const std::string& str, const std::string& what);
bool stringStartsWith(const std::string& str, const std::string& what);
bool stringEndsWith(const std::string& str, const std::string& what);

/// удалить из строки начальные и конечные пробельные символы
std::string trimString(const std::string& str);

unsigned int crc32(unsigned int* prevCrc, char symbol);

std::string& dataToHexStr(const void* dat, uint size, std::string& ret);
std::string& dataToBase64(const void* dat, uint size, std::string& ret);
unsigned int shiftForUtf8Code(uchar code);
bool isUtf8WChar(unsigned int c);

char getFirstNotSpaceSymbol(const char* str);
std::string encryptDecryptStr(const std::string& str, const char mask = 's');
std::string encryptXorStr(const std::string& str, const std::string& password);

std::string_view nextWordView(const std::string_view& str, size_t& shift, const std::string& delims);
std::string_view nextWordView(std::string_view&& str, size_t& shift, const std::string& delims) = delete;

std::string_view prevWordView(const std::string_view& str, size_t& shift, const std::string& delims);
std::string_view prevWordView(std::string_view&& str, size_t& shift, const std::string& delims) = delete;

std::string_view trimStringView(const std::string_view& str);
std::string_view trimStringView(std::string_view&& str) = delete;

void log(std::string_view);
void log(const std::vector<std::string_view>&);

template <typename T> T fromString(const std::string& str, const T def);
} // namespace ie

// _________________________________________________________________________________________
template <typename T> T ie::fromString(const std::string& str, const T def)
{
    if (str.empty())
        return def;
    T ret;
    std::stringstream s;
    s << str;
    s >> ret;
    return ret;
}
// _________________________________________________________________________________________
