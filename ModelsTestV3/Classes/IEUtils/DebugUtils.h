//
//  DebugUtils.h
//  Exeriment
//
//  Created by d.antonov on 25.08.16.
//  Copyright © 2016 Nexters. All rights reserved.
//
#pragma once

#include "Global.h"

class AWindow;

namespace ie {
/// показывает сообщение об ошибке первые 4 раза
/// если включен режим тестирования
void debugMessage(const char* pszFormat, ...);
/// показывает сообщение об ошибке
/// если включен режим тестирования
void debugMessageImportant(const char* pszFormat, ...); // не используется

void startDebugTimer();
void startDebugTimer(std::string label);
double getDebugTimer(); // не используется
double showDebugTimer();

/// вкл/выкл отладочный буст
void debugBustOnOff();
/// вкл/выкл визуализацию линковки
void debugWindowLink();
/// вкл/выкл информацию о зданиях
void debugInfoOnOff();
bool debugInfo();
/// вкл/выкл информацию о зданиях
void debugTapZoneOnOff();
/// Обновить дебаговую индикацию кнопок
void updateDebugButtonIndication(AWindow* win);
} // namespace ie
