//
//  IELog.cpp
//  Experiment
//
//  Created by a.arsentiev on 27/02/17.
//
//

#include "IELog.hpp"

#include "Global.h"
//#include "SessionLog.h"
//#include "CrashlyticsSupport.hpp"
//#include "UniqueIDs.h"

static const int sStringBufSize = 1024;

namespace ie {

static void log_va(int flag, const char* format, va_list args);

const static int sLogOutGS = LogType::Out | LogType::CL;
const static int sLogOutSession = LogType::Out | LogType::Sesion | LogType::CL;
const static int sLogGameServer = LogType::GameServer;

// _________________________________________________________________________________________
void log(const std::string& str)
{
    logFStr(sLogOutGS, str);
}
// _________________________________________________________________________________________
void log(const char* pszFormat, ...)
{
    va_list args;
    va_start(args, pszFormat);
    log_va(sLogOutGS, pszFormat, args);
    va_end(args);
}
// _________________________________________________________________________________________
void logFStr(int lt, const std::string& str)
{
    //    if (lt & LogType::Out) {
    //        cocos2d::logStrImpl((str + "\n").c_str());
    //    }
    //    if (lt & LogType::CL) {
    //        CrashlyticsSupport::log(str);
    //    }
    //    if (lt & LogType::Sesion) {
    //        if (SessionLog::Instance()) {
    //            SessionLog::Instance()->addToLog(str);
    //        }
    //    }
    //    if (lt & LogType::GameServer) {
    //        if (SessionLog::Instance()) {
    //            SessionLog::Instance()->addToProtoLog(str);
    //        }
    //    }
}
// _________________________________________________________________________________________
void logFast(const std::string& str)
{
    logFStr(LogType::Out, str);
}
// _________________________________________________________________________________________
void logFast(const char* pszFormat, ...)
{
    va_list args;
    va_start(args, pszFormat);
    log_va(LogType::Out, pszFormat, args);
    va_end(args);
}
// _________________________________________________________________________________________
void logSession(const std::string& str)
{
    logFStr(sLogOutSession, str);
}
// _________________________________________________________________________________________
void logSession(const char* pszFormat, ...)
{
    va_list args;
    va_start(args, pszFormat);
    log_va(sLogOutSession, pszFormat, args);
    va_end(args);
}
// _________________________________________________________________________________________
void logGS(const std::string& str)
{
    logFStr(sLogGameServer, str);
}
// _________________________________________________________________________________________
void logGS(const char* pszFormat, ...)
{
    va_list args;
    va_start(args, pszFormat);
    log_va(sLogGameServer, pszFormat, args);
    va_end(args);
}
// _________________________________________________________________________________________
std::string id2str(PrototypeId pid)
{
    return "";//std::string(uID::decodeTag(pid)) + "[" + std::to_string(pid) + "]";
}
// _________________________________________________________________________________________
// _________________________________________________________________________________________
void log_va(int flag, const char* format, va_list args)
{
    int bufferSize = sStringBufSize;
    char* buf = nullptr;

    do {
        buf = new (std::nothrow) char[bufferSize];
        if (buf == nullptr)
            return; // not enough memory

        int ret = vsnprintf(buf, bufferSize - 3, format, args);
        if (ret < 0) {
            bufferSize *= 2;

            delete[] buf;
        }
        else if (ret >= bufferSize) {
            bufferSize = ret + 4;
            delete[] buf;
        }
        else
            break;

    } while (true);

    // strcat(buf, "\n");

    logFStr(flag, buf);

    delete[] buf;
}

// _________________________________________________________________________________________
} // namespace ie
