//
//  Vector2d_inl.h
//  Experiment
//
//  Created by a.arsentiev on 12/04/17.
//
//

#pragma once

// _________________________________________________________________________________________
template <class T_Cell> //
Vector2d<T_Cell>::Vector2d()
{
}
// _________________________________________________________________________________________
template <class T_Cell> //
Vector2d<T_Cell>::Vector2d(uint2d s, Cell d)
    : mSize(s)
    , mDefault(d)
{
    resize(mSize);
}
// _________________________________________________________________________________________
template <class T_Cell> //
typename Vector2d<T_Cell>::Cell& Vector2d<T_Cell>::at(uint2d c)
{
    if (not check(c)) {
        static Vector2d::Cell sGarbage;
        sGarbage = mDefault;
        return sGarbage;
    }

    return mYs[c.y][c.x];
}
// _________________________________________________________________________________________
template <class T_Cell> //
typename Vector2d<T_Cell>::Cell& Vector2d<T_Cell>::operator[](uint2d c)
{
    return mYs[c.y][c.x];
}
// _________________________________________________________________________________________
template <class T_Cell> //
const typename Vector2d<T_Cell>::Cell& Vector2d<T_Cell>::at(uint2d c) const
{
    if (not check(c)) {
        static Vector2d::Cell sGarbage;
        return sGarbage;
    }

    return mYs[c.y][c.x];
}
// _________________________________________________________________________________________
template <class T_Cell> //
const typename Vector2d<T_Cell>::Cell& Vector2d<T_Cell>::operator[](uint2d c) const
{
    return mYs[c.y][c.x];
}
// _________________________________________________________________________________________
template <class T_Cell> //
uint2d Vector2d<T_Cell>::size() const
{
    return mSize;
}
// _________________________________________________________________________________________
template <class T_Cell> //
void Vector2d<T_Cell>::clear()
{
    mYs.clear();
    mSize = uint2d(0, 0);
}
// _________________________________________________________________________________________
template <class T_Cell> //
void Vector2d<T_Cell>::resize(uint2d s)
{
    mSize = s;
    mYs.resize(s.y);
    for (auto& v1 : mYs)
        v1.resize(s.x, mDefault);
}
// _________________________________________________________________________________________
template <class T_Cell> //
bool Vector2d<T_Cell>::check(uint2d c) const
{
    if (c.x < mSize.x && c.y < mSize.y)
        return true;
    return false;
}
// _________________________________________________________________________________________
template <class T_Cell> //
void Vector2d<T_Cell>::resetAll(Vector2d<T_Cell>::Cell c)
{
    for (auto& v1 : mYs)
        std::fill(v1.begin(), v1.end(), c);
}
// _________________________________________________________________________________________
