//
//  Math.cpp
//  LostChapters
//
//  Created by Gleb Kolobkov on 20.08.15.
//  Copyright (c) 2015 Crazy Bit. All rights reserved.
//

#include "MathUtils.h"

#include <cmath>

namespace ie {
namespace math {
    // _________________________________________________________________________________________
    int roundMultiply(double num, double coeff)
    {
        int val = int(1e-6 * round(1.e6 * num * coeff));
        return val;
    }
    // _________________________________________________________________________________________
    double normalize(double num)
    {
        return 1e-6 * round(1e6 * num);
    }
    // _________________________________________________________________________________________
    double normalizeAngle(double angle)
    {
        if (angle < 0.0)
            angle = 360.0 - std::fmod(-angle, 360.0);
        else if (angle >= 360.0)
            angle = std::fmod(angle, 360);
        return angle;
    }
    // _________________________________________________________________________________________
    double bestDistanceForRotation(double angle1, double angle2)
    {
        angle1 = normalizeAngle(angle1);
        angle2 = normalizeAngle(angle2);

        if (angle1 == angle2)
            return 0.0;

        double diff1, diff2;

        bool reverse = angle2 < angle1;
        if (reverse)
            std::swap(angle1, angle2);

        diff1 = angle2 - angle1;
        diff2 = (angle1 + 360.0) - angle2;

        if (reverse)
            std::swap(diff1, diff2);

        if (diff2 < diff1)
            return -diff2;

        return diff1;
    }
} // namespace math
} // namespace ie
