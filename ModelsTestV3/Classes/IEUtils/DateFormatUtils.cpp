//
//  DateFormatUtils.h
//  Exeriment
//
//  Created by d.antonov on 26.08.16.
//  Copyright © 2016 Nexters. All rights reserved.
//
#include "DateFormatUtils.h"
#include "StringFormatUtils.h"

const static int32_t sM = 60;
const static int32_t sH = sM * 60;
const static int32_t sD = sH * 24;

// _________________________________________________________________________________________
std::string ie::formatTime(time_t timeSeconds, bool showFull /* = false */)
{
    std::string retStr = "";

    time_t days = timeSeconds / sD;
    if (days) {
        if (retStr.size())
            retStr += " ";
        retStr += std::to_string(days);// ie::getSuffixVariation(TEXT_C(STR_DAY), days);
    }

    time_t hours = (timeSeconds - days * sD) / 3600;
    if (hours) {
        if (retStr.size())
            retStr += " ";
        retStr += std::to_string(hours);//ie::getSuffixVariation(TEXT_C(STR_HOUR), hours);
    }

    time_t minutes = (timeSeconds % 3600) / 60;
    if (minutes || showFull) {
        if (retStr.size())
            retStr += " ";
        retStr += std::to_string(minutes);//ie::getSuffixVariation(TEXT_C(STR_MINUTE), minutes);
    }

    time_t seconds = timeSeconds % 3600 % 60;
    if ((!hours && seconds) || showFull) {
        if (retStr.size())
            retStr += " ";
        retStr += std::to_string(seconds);//ie::getSuffixVariation(TEXT_C(STR_SECOND), seconds);
    }

    if (retStr == "")
        retStr = "0";

    return retStr;
}
// _________________________________________________________________________________________
std::string ie::formatTimeShort(time_t timeSeconds, bool showFull /* = false */)
{
    std::string retStr = "";

    time_t days = timeSeconds / sD;
    if (days) {
        if (retStr.size())
            retStr += " ";
        retStr += std::to_string(days);// ie::getSuffixVariation(TEXT_C(STR_DAY_SHORT), days);
    }

    time_t hours = (timeSeconds - days * sD) / 3600;
    if (hours) {
        if (retStr.size())
            retStr += " ";
        retStr += std::to_string(hours);// ie::getSuffixVariation(TEXT_C(STR_HOUR_SHORT), hours);
    }

    time_t minutes = (timeSeconds % 3600) / 60;
    if (minutes || showFull) {
        if (retStr.size())
            retStr += " ";
        retStr += std::to_string(minutes);// ie::getSuffixVariation(TEXT_C(STR_MINUTE_SHORT), minutes);
    }

    time_t seconds = timeSeconds % 3600 % 60;
    if ((!hours && seconds) || showFull) {
        if (retStr.size())
            retStr += " ";
        retStr += std::to_string(seconds);// ie::getSuffixVariation(TEXT_C(STR_SECOND_SHORT), seconds);
    }

    if (retStr == "")
        retStr = "0";

    return retStr;
}
// _________________________________________________________________________________________
std::string ie::formatTimeHMS(time_t seconds, bool showFull /* = false */)
{
    const int HOURS = (1 << 0);
    const int MINS = (1 << 1);

    std::string res;
    int flags = 0;

    time_t hours = seconds / 3600;
    if (hours || showFull) {
        res += ie::strf("%02d", hours);
        flags |= HOURS;
    }

    time_t minutes = (seconds % 3600) / 60;
    if (minutes || showFull) {
        if (flags & HOURS)
            res += ":";
        res += ie::strf("%02d", minutes);
        flags |= MINS;
    }

    time_t secs = seconds % 3600 % 60;
    if (secs || showFull) {
        if (flags & MINS)
            res += ":";
        res += ie::strf("%02d", secs);
    }

    return res;
}

// _________________________________________________________________________________________
std::string ie::formatDateTime(time_t timeSeconds)
{
    time_t seconds = timeSeconds;
    struct tm* local = localtime(&seconds);
    return asctime(local);
}
// _________________________________________________________________________________________
std::string ie::formatDateTime(time_t date, const std::string& dateTempl)
{
    time_t date_time_t = date;
    struct tm* local = localtime(&date_time_t);
    char buffer[256];
    strftime(buffer, 256, dateTempl.c_str(), local);
    return buffer;
}
// _________________________________________________________________________________________
std::string ie::formatTime2Digital(time_t timeSeconds)
{
    const static int32_t sM = 60;
    const static int32_t sH = sM * 60;
    const static int32_t sD = sH * 24;

    int days = int(timeSeconds / sD);
    int hours = (timeSeconds % sD) / sH;
    int minutes = (timeSeconds % sH) / sM;
    int seconds = (timeSeconds % sM);

    if (days) {
        return ie::strf("%02d:%02d", days, hours);
    }
    if (hours) {
        return ie::strf("%02d:%02d", hours, minutes);
    }
    return ie::strf("%02d:%02d", minutes, seconds);
}
// _________________________________________________________________________________________
//_jnbxmuzpnlpip__random_string___ozmdoqdltgxj_
