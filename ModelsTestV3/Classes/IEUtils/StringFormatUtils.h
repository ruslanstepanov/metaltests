//
//  StringFormatUtils.h
//  Exeriment
//
//  Created by d.antonov on 25.08.16.
//  Copyright © 2016 Nexters. All rights reserved.
//
#pragma once
//#include "GameLocale.h"

#include "Global.h"

#define CSTRKEY(name) #name
#define HIBYTE(x) ((unsigned int)(x) >> 8)
#define LOWBYTE(x) ((unsigned char)(x))

namespace ie {
/// форматирует строку в статический буфер до move constructor давал выигрышь в скорости, теперь бессмысленен
const char* formatBuff(const char* pszFormat, ...);
/// форматирует строку в статический буфер до move constructor давал выигрышь в скорости, теперь бессмысленен
//const char* formatBuff(StringTag, ...);
/// форматирует строку
std::string strf(const char* pszFormat, ...);
/// форматирует болшие числа 156 - "156", 13567 - "13k"
char* formatBigNumber(unsigned long number);

std::string encodeValue(std::string key, int value);
int decodeValue(std::string key, std::string value);
char* itoa(int value, char* result, int base);
std::string& uppercase(std::string& str);
WString& lowcaseLat(WString& str);
std::string& lowcaseLat(std::string& str);

/// формирует из паттерна строку с нужным окончанием (pattern = "%d д[ень,ня,ней]" => 5 дней)
std::string getSuffixVariation(const std::string& pattern, time_t count);
/// формирует время с окончаниями
std::string getDurationWithSuffix(time_t time);

} // namespace ie
