//
//  ThreadUtils.h
//  Exeriment
//
//  Created by d.antonov on 25.08.16.
//  Copyright © 2016 Nexters. All rights reserved.
//
#pragma once

namespace ie {
static int sMainThreadID = -1;

void markMainThreadID();
bool isMainThread();
} // namespace ie
