//
//  Math.h
//  LostChapters
//
//  Created by Gleb Kolobkov on 20.08.15.
//  Copyright (c) 2015 Crazy Bit. All rights reserved.
//

#pragma once

#include "Global.h"

namespace ie {
namespace math {

    ///	умножение с округлением как на сервере
    int roundMultiply(double num, double coeff);

    double normalize(double num);

    /// converts angle value between 0-360
    double normalizeAngle(double angle);
    // возвращает ближайшее расстояние между углами
    double bestDistanceForRotation(double angle1, double angle2);

    template <size_t arg1, size_t... others> struct static_max;

    template <size_t arg> struct static_max<arg> {
        static const size_t value = arg;
    };

    template <size_t arg1, size_t arg2, size_t... others> struct static_max<arg1, arg2, others...> {
        static const size_t value
            = arg1 >= arg2 ? static_max<arg1, others...>::value : static_max<arg2, others...>::value;
    };
} // namespace math
} // namespace ie
