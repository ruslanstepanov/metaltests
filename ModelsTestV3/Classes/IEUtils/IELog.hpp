//
//  IELog.hpp
//  Experiment
//
//  Created by a.arsentiev on 27/02/17.
//
//

#pragma once

#include <string>

namespace ie {
enum LogType {
    Out = 1, ///< в std::out / adb logcat
    CL = 2, ///< в крешлитик
    Sesion = 4, ///< в лог сессии
    GameServer = 8 ///< в лог общения с GameServer
};

/// напечатать в std::out и отправить в крешлитик
void log(const std::string&);
/// напечатать в std::out и отправить в крешлитик
void log(const char* f, ...);

/// напечатать в std::out но не отправлять в крешлитик
void logFast(const std::string&);
/// напечатать в std::out но не отправлять в крешлитик
void logFast(const char* f, ...);

/// отправить в лог сессии и ie::log()
void logSession(const std::string&);
/// отправить в лог сессии и ie::log()
void logSession(const char* f, ...);

/// напечатать в лог общения с GameServer
void logGS(const std::string&);
/// напечатать в лог общения с GameServer
void logGS(const char* f, ...);

/// метод производящий раскидывание строчки лога по логерам
void logFStr(int lt, const std::string&);

/// прототип в строку на основе ткущих словарей
std::string id2str(uint64_t);
} // namespace ie
