//
//  StringUtils.cpp
//  Exeriment
//
//  Created by d.antonov on 25.08.16.
//  Copyright © 2016 Nexters. All rights reserved.
//
#include "StringUtils.h"
//#include "utf8.h"
#include "StringFormatUtils.h"
#include "ThreadUtils.h"

extern const std::string StringZero;
extern const StringVec StringVecZero;
extern const StringMap StringMapZero;

#define MAKEWORD(a, b) ((unsigned short)(((unsigned char)(a)) | ((unsigned short)((unsigned char)(b))) << 8))

// _________________________________________________________________________________________
template <typename T>
void replaceAllT(std::basic_string<T>& where, const std::basic_string<T>& what, const std::basic_string<T> onWhat)
{
    for (typename std::basic_string<T>::size_type pos = 0, step = onWhat.length();; pos += step) {
        // Locate the substring to replace
        pos = where.find(what, pos);
        if (pos == std::basic_string<T>::npos)
            break;

        // Replace by erasing and inserting
        where.replace(pos, what.length(), onWhat);
    }
}
// _________________________________________________________________________________________
WString& ie::mbToWStr(const std::string& inStr, WString& outWStr)
{
    //    try {
    //        utf8::utf8to16(inStr.begin(), inStr.end(), back_inserter(outWStr));
    //    }
    //    catch (std::exception e) {
    //        ie::log("ie::mbToWStr exception");
    //    }
    return outWStr;
}
// _________________________________________________________________________________________
std::string& ie::wToMbStr(const std::wstring& inStr, std::string& outWStr)
{
    //    try {
    //        utf8::utf16to8(inStr.begin(), inStr.end(), back_inserter(outWStr));
    //    }
    //    catch (std::exception e) {
    //        ie::log("ie::wToMbStr exception");
    //    }
    return outWStr;
}
// _________________________________________________________________________________________
unsigned short ie::eurUppercase(unsigned char c1, unsigned char c2)
{
    unsigned short word = MAKEWORD(c2, c1);

    switch (word) {
        case 0xD0B0:
            return 0xD090; // а
        case 0xD0B1:
            return 0xD091; // б
        case 0xD0B2:
            return 0xD092; // в
        case 0xD0B3:
            return 0xD093; // г
        case 0xD0B4:
            return 0xD094; // д
        case 0xD0B5:
            return 0xD095; // е
        case 0xD191:
            return 0xD081; // ё
        case 0xD0B6:
            return 0xD096; // ж
        case 0xD0B7:
            return 0xD097; // з
        case 0xD0B8:
            return 0xD098; // и
        case 0xD0B9:
            return 0xD099; // й
        case 0xD0BA:
            return 0xD09A; // к
        case 0xD0BB:
            return 0xD09B; // л
        case 0xD0BC:
            return 0xD09C; // м
        case 0xD0BD:
            return 0xD09D; // н
        case 0xD0BE:
            return 0xD09E; // о
        case 0xD0BF:
            return 0xD09F; // п
        case 0xD180:
            return 0xD0A0; // р
        case 0xD181:
            return 0xD0A1; // с
        case 0xD182:
            return 0xD0A2; // т
        case 0xD183:
            return 0xD0A3; // у
        case 0xD184:
            return 0xD0A4; // ф
        case 0xD185:
            return 0xD0A5; // х
        case 0xD186:
            return 0xD0A6; // ц
        case 0xD187:
            return 0xD0A7; // ч
        case 0xD188:
            return 0xD0A8; // ш
        case 0xD189:
            return 0xD0A9; // щ
        case 0xD18A:
            return 0xD0AA; // ъ
        case 0xD18B:
            return 0xD0AB; // ы
        case 0xD18C:
            return 0xD0AC; // ь
        case 0xD18D:
            return 0xD0AD; // э
        case 0xD18E:
            return 0xD0AE; // ю
        case 0xD18F:
            return 0xD0AF; // я

        case 0xC3A0:
            return 0xC380; // à -> À
        case 0xC3A1:
            return 0xC381; // á -> Á
        case 0xC3A2:
            return 0xC382; // â -> Â
        case 0xC3A3:
            return 0xC383; // ã -> Ã
        case 0xC3A4:
            return 0xC384; // ä -> Ä
        case 0xC3A5:
            return 0xC385; // å -> Å
        case 0xC3A6:
            return 0xC386; // æ -> Æ
        case 0xC3A7:
            return 0xC387; // ç -> Ç
        case 0xC3A8:
            return 0xC388; // è -> È
        case 0xC3A9:
            return 0xC389; // é -> É
        case 0xC3AA:
            return 0xC38A; // ê -> Ê
        case 0xC3AB:
            return 0xC38B; // ë -> Ë
        case 0xC3AC:
            return 0xC38C; // ì -> Ì
        case 0xC3AD:
            return 0xC38D; // í -> Í
        case 0xC3AE:
            return 0xC38E; // î -> Î
        case 0xC3AF:
            return 0xC38F; // ï -> Ï
        case 0xC3B0:
            return 0xC390; // ð -> Ð
        case 0xC3B1:
            return 0xC391; // ñ -> Ñ
        case 0xC3B2:
            return 0xC392; // ò -> Ò
        case 0xC3B3:
            return 0xC393; // ó -> Ó
        case 0xC3B4:
            return 0xC394; // ô -> Ô
        case 0xC3B5:
            return 0xC395; // õ -> Õ
        case 0xC3B6:
            return 0xC396; // ö -> Ö
        case 0xC3B7:
            return 0xC397; // ÷ -> ×
        case 0xC3B8:
            return 0xC398; // ø -> Ø
        case 0xC3B9:
            return 0xC399; // ù -> Ù
        case 0xC3BA:
            return 0xC39A; // ú -> Ú
        case 0xC3BB:
            return 0xC39B; // û -> Û
        case 0xC3BC:
            return 0xC39C; // ü -> Ü
        case 0xC3BD:
            return 0xC39D; // ý -> Ý
        case 0xC3BE:
            return 0xC39E; // þ -> Þ
        case 0xC3BF:
            return 0xC5B8; // ÿ -> Ÿ
    }
    return 0;
}
// _________________________________________________________________________________________
StringVec ie::joinStringVecs(const StringVec& strings1, const StringVec& strings2, bool dropDuplicates)
{
    StringVec result;
    if (dropDuplicates) {
        std::set<std::string> resultSet;
        resultSet.insert(strings1.begin(), strings1.end());
        resultSet.insert(strings2.begin(), strings2.end());
        result.insert(result.end(), resultSet.begin(), resultSet.end());
    }
    else {
        result.insert(result.end(), strings1.begin(), strings1.end());
        result.insert(result.end(), strings2.begin(), strings2.end());
    }

    return result;
}
// _________________________________________________________________________________________
StringVec ie::splitString(const std::string& str, char delimeter)
{
    StringVec retArr;
    if (str.empty())
        return retArr;

    size_t prevFound = 0, found = 0;
    found = str.find(delimeter, found);
    if (found == std::string::npos) {
        retArr.push_back(str);
    }
    else {
        std::string tmpStr;
        if (found != 0) {
            tmpStr = str.substr(prevFound, found - prevFound).c_str();
            retArr.push_back(tmpStr);
        }
        prevFound = found;
        while ((found = str.find(delimeter, found)) != std::string::npos) {
            size_t n = found - prevFound;
            if (n) {
                tmpStr = str.substr(prevFound, found - prevFound);
                retArr.push_back(tmpStr);
            }
            found++;
            prevFound = found;
        }
        size_t n = str.length() - prevFound;
        if (n) {
            std::string tmpStr = str.substr(prevFound, n);
            retArr.push_back(tmpStr);
        }
    }
    return retArr;
}
// _________________________________________________________________________________________
const StringVec& ie::splitStringBuf(const std::string& str, char delimeter)
{
    //    IEAssert(ie::isMainThread(), "it can only execuit in main thread");

    static StringVec retArr;
    retArr.clear();

    size_t prevFound = 0, found = 0;
    found = str.find(delimeter, found);
    if (found == std::string::npos) {
        retArr.push_back(str);
    }
    else {
        std::string tmpStr;
        if (found != 0) {
            tmpStr = str.substr(prevFound, found - prevFound).c_str();
            retArr.push_back(tmpStr);
        }
        prevFound = found;
        while ((found = str.find(delimeter, found)) != std::string::npos) {
            size_t n = found - prevFound; // FIXME: AUTO-FIX (not_fixed: 17)
            if (n) {
                tmpStr = str.substr(prevFound, found - prevFound);
                retArr.push_back(tmpStr);
            }
            found++;
            prevFound = found;
        }
        size_t n = str.length() - prevFound; // FIXME: AUTO-FIX (not_fixed: 23)
        if (n) {
            std::string tmpStr = str.substr(prevFound, n);
            retArr.push_back(tmpStr);
        }
    }
    return retArr;
}
// _________________________________________________________________________________________
const StringVec& ie::splitString(const std::string& str, const std::string& delimeter)
{
    static StringVec retArr;
    retArr.clear();

    size_t prevFound = 0, found = 0;
    found = str.find(delimeter, found);
    if (found == std::string::npos) {
        retArr.push_back(str);
    }
    else {
        std::string tmpStr;
        if (found != 0) {
            tmpStr = str.substr(prevFound, found - prevFound).c_str();
            retArr.push_back(tmpStr);
        }
        prevFound = found;
        while ((found = str.find(delimeter, found)) != std::string::npos) {
            size_t n = found - prevFound;
            if (n) {
                tmpStr = str.substr(prevFound, found - prevFound);
                retArr.push_back(tmpStr);
            }
            found++;
            prevFound = found;
        }
        size_t n = str.length() - prevFound;
        if (n) {
            std::string tmpStr = str.substr(prevFound, n);
            retArr.push_back(tmpStr);
        }
    }
    return retArr;
}
// _________________________________________________________________________________________
std::string ie::joinStrings(const StringVec& strings, const std::string& delimeter)
{
    size_t size = strings.size();

    if (size == 0) {
        return "";
    }
    if (size == 1) {
        return strings[0];
    }

    std::string result;
    for (size_t i = 0; i < size - 1; ++i) {
        result += strings[i] + delimeter;
    }
    result += strings[size - 1];

    return result;
}
// _________________________________________________________________________________________
std::string ie::joinStrings(const StringVec& strings, const char delimeter)
{
    return joinStrings(strings, std::string(&delimeter, 1));
}
// _________________________________________________________________________________________
void ie::replaceAll(std::string& where, const std::string& what, const std::string& onWhat)
{
    replaceAllT<char>(where, what, onWhat);
}
// _________________________________________________________________________________________
std::string ie::replaceAll(const std::string& where, const std::string& what, const std::string& onWhat)
{
    std::string ret(where);
    replaceAllT<char>(ret, what, onWhat);
    return ret;
}
// _________________________________________________________________________________________
void ie::replaceAllW(WString& where, const WString& what, const WString& onWhat)
{
    replaceAllT<wchar_t>(where, what, onWhat);
}
// _________________________________________________________________________________________
WString ie::replaceAllW(const WString& where, const WString& what, const WString& onWhat)
{
    WString ret(where);
    replaceAllT<wchar_t>(ret, what, onWhat);
    return ret;
}
// _________________________________________________________________________________________
bool ie::stringContains(const std::string& str, const std::string& what)
{
    return (str.find(what) != std::string::npos);
}
// _________________________________________________________________________________________
bool ie::stringStartsWith(const std::string& str, const std::string& what)
{
    if (what.length() <= str.length()) {
        return (0 == memcmp(str.c_str(), what.c_str(), what.length()));
    }

    return false;
}
// _________________________________________________________________________________________
bool ie::stringEndsWith(const std::string& str, const std::string& what)
{
    if (what.length() <= str.length()) {
        return (0 == memcmp(&str.c_str()[str.length() - what.length()], what.c_str(), what.length()));
    }

    return false;
}
// _________________________________________________________________________________________
std::string ie::trimString(const std::string& str)
{
    auto wsfront = std::find_if_not(str.begin(), str.end(), [](int c) { return std::isspace(c); });
    auto wsback = std::find_if_not(str.rbegin(), str.rend(), [](int c) { return std::isspace(c); }).base();
    return (wsback <= wsfront ? std::string() : std::string(wsfront, wsback));
}
// _________________________________________________________________________________________
WStringVec* ie::newSplitStringW(const WString& str, wchar_t delimeter)
{
    WStringVec* retArr = new WStringVec();

    size_t prevFound = 0, found = 0;
    found = str.find(delimeter, found);
    if (found == WString::npos) {
        retArr->push_back(str);
    }
    else {
        WString tmpStr;
        if (found != 0) {
            tmpStr = str.substr(prevFound, found - prevFound).c_str();
            retArr->push_back(tmpStr);
        }
        prevFound = found;
        while ((found = str.find(delimeter, found)) != WString::npos) {
            size_t n = found - prevFound;
            if (n) {
                tmpStr = str.substr(prevFound, found - prevFound);
                //                    ie::log("%s", tmpStr.c_str());
                retArr->push_back(tmpStr);
            }
            found++;
            prevFound = found;
        }
        size_t n = str.length() - prevFound;
        if (n) {
            WString tmpStr = str.substr(prevFound, n);
            retArr->push_back(tmpStr);
        }
    }
    return retArr;
}
// _________________________________________________________________________________________
unsigned int ie::crc32(unsigned int* prevCrc, char symbol)
{
    static const unsigned int kCrc32Table[256] = {
        0x00000000, 0x77073096, 0xee0e612c, 0x990951ba, 0x076dc419, 0x706af48f, 0xe963a535, 0x9e6495a3, 0x0edb8832,
        0x79dcb8a4, 0xe0d5e91e, 0x97d2d988, 0x09b64c2b, 0x7eb17cbd, 0xe7b82d07, 0x90bf1d91, 0x1db71064, 0x6ab020f2,
        0xf3b97148, 0x84be41de, 0x1adad47d, 0x6ddde4eb, 0xf4d4b551, 0x83d385c7, 0x136c9856, 0x646ba8c0, 0xfd62f97a,
        0x8a65c9ec, 0x14015c4f, 0x63066cd9, 0xfa0f3d63, 0x8d080df5, 0x3b6e20c8, 0x4c69105e, 0xd56041e4, 0xa2677172,
        0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b, 0x35b5a8fa, 0x42b2986c, 0xdbbbc9d6, 0xacbcf940, 0x32d86ce3,
        0x45df5c75, 0xdcd60dcf, 0xabd13d59, 0x26d930ac, 0x51de003a, 0xc8d75180, 0xbfd06116, 0x21b4f4b5, 0x56b3c423,
        0xcfba9599, 0xb8bda50f, 0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924, 0x2f6f7c87, 0x58684c11, 0xc1611dab,
        0xb6662d3d, 0x76dc4190, 0x01db7106, 0x98d220bc, 0xefd5102a, 0x71b18589, 0x06b6b51f, 0x9fbfe4a5, 0xe8b8d433,
        0x7807c9a2, 0x0f00f934, 0x9609a88e, 0xe10e9818, 0x7f6a0dbb, 0x086d3d2d, 0x91646c97, 0xe6635c01, 0x6b6b51f4,
        0x1c6c6162, 0x856530d8, 0xf262004e, 0x6c0695ed, 0x1b01a57b, 0x8208f4c1, 0xf50fc457, 0x65b0d9c6, 0x12b7e950,
        0x8bbeb8ea, 0xfcb9887c, 0x62dd1ddf, 0x15da2d49, 0x8cd37cf3, 0xfbd44c65, 0x4db26158, 0x3ab551ce, 0xa3bc0074,
        0xd4bb30e2, 0x4adfa541, 0x3dd895d7, 0xa4d1c46d, 0xd3d6f4fb, 0x4369e96a, 0x346ed9fc, 0xad678846, 0xda60b8d0,
        0x44042d73, 0x33031de5, 0xaa0a4c5f, 0xdd0d7cc9, 0x5005713c, 0x270241aa, 0xbe0b1010, 0xc90c2086, 0x5768b525,
        0x206f85b3, 0xb966d409, 0xce61e49f, 0x5edef90e, 0x29d9c998, 0xb0d09822, 0xc7d7a8b4, 0x59b33d17, 0x2eb40d81,
        0xb7bd5c3b, 0xc0ba6cad, 0xedb88320, 0x9abfb3b6, 0x03b6e20c, 0x74b1d29a, 0xead54739, 0x9dd277af, 0x04db2615,
        0x73dc1683, 0xe3630b12, 0x94643b84, 0x0d6d6a3e, 0x7a6a5aa8, 0xe40ecf0b, 0x9309ff9d, 0x0a00ae27, 0x7d079eb1,
        0xf00f9344, 0x8708a3d2, 0x1e01f268, 0x6906c2fe, 0xf762575d, 0x806567cb, 0x196c3671, 0x6e6b06e7, 0xfed41b76,
        0x89d32be0, 0x10da7a5a, 0x67dd4acc, 0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5, 0xd6d6a3e8, 0xa1d1937e,
        0x38d8c2c4, 0x4fdff252, 0xd1bb67f1, 0xa6bc5767, 0x3fb506dd, 0x48b2364b, 0xd80d2bda, 0xaf0a1b4c, 0x36034af6,
        0x41047a60, 0xdf60efc3, 0xa867df55, 0x316e8eef, 0x4669be79, 0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236,
        0xcc0c7795, 0xbb0b4703, 0x220216b9, 0x5505262f, 0xc5ba3bbe, 0xb2bd0b28, 0x2bb45a92, 0x5cb36a04, 0xc2d7ffa7,
        0xb5d0cf31, 0x2cd99e8b, 0x5bdeae1d, 0x9b64c2b0, 0xec63f226, 0x756aa39c, 0x026d930a, 0x9c0906a9, 0xeb0e363f,
        0x72076785, 0x05005713, 0x95bf4a82, 0xe2b87a14, 0x7bb12bae, 0x0cb61b38, 0x92d28e9b, 0xe5d5be0d, 0x7cdcefb7,
        0x0bdbdf21, 0x86d3d2d4, 0xf1d4e242, 0x68ddb3f8, 0x1fda836e, 0x81be16cd, 0xf6b9265b, 0x6fb077e1, 0x18b74777,
        0x88085ae6, 0xff0f6a70, 0x66063bca, 0x11010b5c, 0x8f659eff, 0xf862ae69, 0x616bffd3, 0x166ccf45, 0xa00ae278,
        0xd70dd2ee, 0x4e048354, 0x3903b3c2, 0xa7672661, 0xd06016f7, 0x4969474d, 0x3e6e77db, 0xaed16a4a, 0xd9d65adc,
        0x40df0b66, 0x37d83bf0, 0xa9bcae53, 0xdebb9ec5, 0x47b2cf7f, 0x30b5ffe9, 0xbdbdf21c, 0xcabac28a, 0x53b39330,
        0x24b4a3a6, 0xbad03605, 0xcdd70693, 0x54de5729, 0x23d967bf, 0xb3667a2e, 0xc4614ab8, 0x5d681b02, 0x2a6f2b94,
        0xb40bbe37, 0xc30c8ea1, 0x5a05df1b, 0x2d02ef8d,
    }; // kCrc32Table

    return (*prevCrc >> 8) ^ kCrc32Table[(*prevCrc ^ symbol) & 0xFF];
}
// _________________________________________________________________________________________
std::string& ie::dataToHexStr(const void* dat, uint size, std::string& ret)
{
    const unsigned char* dat_c = (const unsigned char*)dat;

    ret.resize(size * 2, '_');

    for (int i = 0; i < size; i++) {
        int val = dat_c[i];
        ie::itoa(val, &ret[i * 2], 16);
        if (ret[i * 2 + 1] == 0) {
            ret[i * 2 + 1] = ret[i * 2 + 0];
            ret[i * 2 + 0] = '0';
        }
    }

    return ret;
}
// _________________________________________________________________________________________
std::string& ie::dataToBase64(const void* dat, uint size, std::string& ret)
{
    ret.clear();

    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

    unsigned char* input = (unsigned char*)dat;

    int outLen = ((size + 2) / 3) * 4;
    ret.resize(outLen, '_');
    unsigned char* output = (unsigned char*)&ret[0]; // new unsigned char[  ];

    for (int i = 0; i < size; i += 3) {
        int value = 0;
        for (int j = i; j < (i + 3); j++) {
            value <<= 8;

            if (j < size) {
                value |= (0xFF & input[j]);
            }
        }

        int index = (i / 3) * 4;
        output[index + 0] = table[(value >> 18) & 0x3F];
        output[index + 1] = table[(value >> 12) & 0x3F];
        output[index + 2] = (i + 1) < size ? table[(value >> 6) & 0x3F] : '=';
        output[index + 3] = (i + 2) < size ? table[(value >> 0) & 0x3F] : '=';
    }

    return ret;
}
// _________________________________________________________________________________________
unsigned int ie::shiftForUtf8Code(uchar code)
{
    switch (code) {
        case 0xD0:
            return 0xCC80;
        case 0xD1:
            return 0xCD40;
        default:
            break;
    }
    return 0;
}
// _________________________________________________________________________________________
bool ie::isUtf8WChar(unsigned int c)
{
    uint charWith3Byts = 0x00010000;
    if (c > charWith3Byts)
        return true;
    else
        return false;
}
// _________________________________________________________________________________________
char ie::getFirstNotSpaceSymbol(const char* str)
{
    const char* sSpaceSymbols = " \n\t";
    int i = 0;
    char c = str[0];

    while (c != 0 && strchr(sSpaceSymbols, c))
        c = str[++i];

    return c;
}
// _________________________________________________________________________________________
std::string ie::encryptDecryptStr(const std::string& str, const char mask)
{
    std::string ret(str.size(), '\0');
    for (int i = 0; i < str.length(); i++)
        ret[i] = str[i] ^ mask;
    return ret;
}
// _________________________________________________________________________________________
std::string ie::encryptXorStr(const std::string& str, const std::string& password)
{
    int passLen = (int)password.size();
    int strLen = (int)str.size();
    std::string res(3 * strLen, '\0');
    int a = 0;

    for (int i = 0; i < strLen; ++i) {
        a = str[i] ^ password[i % passLen];

        res[3 * i] = '0' + a / 100;
        res[3 * i + 1] = '0' + a / 10 % 10;
        res[3 * i + 2] = '0' + a % 10;
    }

    return res;
}
// _________________________________________________________________________________________
std::string_view ie::nextWordView(const std::string_view& str, size_t& shift, const std::string& delim)
{
    size_t begin = shift;
    shift = std::string::npos;

    begin = str.find_first_not_of(delim, begin);
    if (begin == std::string::npos)
        return std::string_view();

    shift = str.find_first_of(delim, begin);
    std::string_view ret = str.substr(begin, (shift - begin));
    return ret;
}
// _________________________________________________________________________________________
std::string_view ie::prevWordView(const std::string_view& str, size_t& shift, const std::string& delim)
{
    size_t last = shift;
    shift = std::string::npos;

    last = str.find_last_not_of(delim, last);
    if (last == std::string::npos)
        return std::string_view();

    shift = str.find_last_of(delim, last);
    ++last;
    ++shift;

    std::string_view ret = str.substr(shift, (last - shift));
    --shift;
    return ret;
}
// _________________________________________________________________________________________
std::string_view ie::trimStringView(const std::string_view& str)
{
    if (str.empty())
        return std::string_view();

    auto it = str.begin();
    while (it != str.end() && isspace(*it))
        ++it;

    if (it == str.end())
        return std::string_view();

    auto eit = str.end() - 1;
    while (eit >= it && isspace(*eit))
        --eit;
    return str.substr(it - str.begin(), eit - it + 1);
}
// _________________________________________________________________________________________
void ie::log(std::string_view s)
{
    ie::log("`" + std::string(s) + "`");
}
// _________________________________________________________________________________________
void ie::log(const std::vector<std::string_view>& sv)
{
    for (auto s : sv) {
        ie::log("`" + std::string(s) + "`");
    }
}
