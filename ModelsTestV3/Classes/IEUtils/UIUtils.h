//
//  UIUtils.h
//  Exeriment
//
//  Created by d.antonov on 25.08.16.
//  Copyright © 2016 Nexters. All rights reserved.
//
#pragma once

#include "Global.h"

namespace ie {
void showUIMessage(const std::string& message, const std::string& title);
void showErrorMessage(const char* pszFormat, ...);
} // namespace ie
