//
//  IeRange.h
//  Experiment
//
//  Created by a.arsentiev on 09/06/17.
//
//

#ifndef IeRange_h
#define IeRange_h

namespace ie {
/// диапазон [от-до)
struct Range {
    uint32_t mFirst = 0; ///< первый элемент
    uint32_t mEnd = 0; ///< элемент идущий после последнего
};
} // namespace ie

#endif /* IeRange_h */
