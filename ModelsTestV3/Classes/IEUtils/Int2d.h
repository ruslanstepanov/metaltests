//
//  IntPoint.h
//  Experiment
//
//  Created by a.arsentiev on 12/04/17.
//
//

#pragma once

#include <stdint.h>

namespace ie {
struct int2d {
    union {
        struct {
            int16_t x;
            int16_t y;
        };
        int32_t i32;
    };

    int2d()
        : i32(0){};

    int2d(int16_t ax, int16_t ay)
        : x(ax)
        , y(ay)
    {
    }
    int2d(const int2d&) = default;
    int2d& operator=(const int2d&) = default;
    bool operator==(const int2d&) const;
    bool operator!=(const int2d&) const;

    const int32_t toInt32() const;
    void fromInt32(int32_t i32);
    void clear();
    bool isZero() const;
};

struct uint2d {
    union {
        struct {
            uint16_t x;
            uint16_t y;
        };
        uint32_t i32;
    };

    uint2d()
        : i32(0){};

    uint2d(uint16_t ax, uint16_t ay)
        : x(ax)
        , y(ay)
    {
    }
    uint2d(const uint2d&) = default;
    uint2d& operator=(const uint2d&) = default;
    bool operator==(const uint2d&) const;
    bool operator!=(const uint2d&) const;

    const uint32_t toUInt32() const;
    void fromUInt32(uint32_t i32);
    void clear();
    bool isZero() const;
    bool isMaxVal() const;
};
// _________________________________________________________________________________________
#pragma mark int2d
inline const int32_t int2d::toInt32() const
{
    return i32;
}
// _________________________________________________________________________________________
inline void int2d::fromInt32(int32_t val)
{
    i32 = val;
}
// _________________________________________________________________________________________
inline void int2d::clear()
{
    x = y = 0;
}
// _________________________________________________________________________________________
inline bool int2d::isZero() const
{
    return x == 0 && y == 0;
}
// _________________________________________________________________________________________
inline bool int2d::operator==(const int2d& other) const
{
    return other.x == x && other.y == y;
}
// _________________________________________________________________________________________
inline bool int2d::operator!=(const int2d& other) const
{
    return other.x != x || other.y != y;
}
// _________________________________________________________________________________________
#pragma mark uint2d
inline const uint32_t uint2d::toUInt32() const
{
    return i32;
}
// _________________________________________________________________________________________
inline void uint2d::fromUInt32(uint32_t val)
{
    i32 = val;
}
// _________________________________________________________________________________________
inline void uint2d::clear()
{
    x = y = 0;
}
// _________________________________________________________________________________________
inline bool uint2d::isZero() const
{
    return x == 0 && y == 0;
}
// _________________________________________________________________________________________
inline bool uint2d::operator==(const uint2d& other) const
{
    return other.x == x && other.y == y;
}
// _________________________________________________________________________________________
inline bool uint2d::operator!=(const uint2d& other) const
{
    return other.x != x || other.y != y;
}
// _________________________________________________________________________________________
inline bool uint2d::isMaxVal() const
{
    return i32 == std::numeric_limits<uint32_t>::max();
}
} // namespace ie

namespace std {
template <> struct hash<ie::int2d> {
    std::size_t operator()(const ie::int2d& k) const
    {
        return std::hash<std::size_t>{}(k.toInt32());
    }
};
template <> struct hash<ie::uint2d> {
    std::size_t operator()(const ie::uint2d& k) const
    {
        return std::hash<std::size_t>{}(k.toUInt32());
    }
};
} // namespace std
