//
//  UtilsLocaleReqTemplate.hpp
//  Experiment
//
//  Created by d.dolgikh on 23.06.17.
//

#pragma once

#include "Global.h"
#include "UtilsLocaleReqTemplatePre.hpp"

namespace ie {

std::string updateLocaleUseTemplateAll(const std::string& str, LocaleReqTemplate, bool active);
std::string updateLocaleUseTemplateNum(const std::string& str, LocaleReqTemplate, bool active);

} // namespace ie
