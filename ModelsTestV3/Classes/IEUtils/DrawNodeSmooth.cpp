
#include "DrawNodeSmooth.h"

Vec2 prev1, prev2, prev3, prev4, prev5, prev6, prev7, prev8, prev9;
bool _FlagPotokLine = false;

// _________________________________________________________________________________________
bool DrawNodeSmooth::init()
{
    if (!DrawNode::init()) {
        return false;
    }

    return true;
}
// _________________________________________________________________________________________
void DrawNodeSmooth::setOpacity(GLubyte opac)
{
    mOpacity = opac;
    if (_bufferCount) {
        for (int i = 0; i < _bufferCount; i++) {
            _buffer[i].colors.a = mOpacity;
        }
    }

    if (_bufferCountGLPoint) {
        for (int i = 0; i < _bufferCountGLPoint; i++) {
            _bufferGLPoint[i].colors.a = mOpacity;
        }
    }

    if (_bufferCountGLLine) {
        for (int i = 0; i < _bufferCountGLLine; i++) {
            _bufferGLLine[i].colors.a = mOpacity;
        }
        _dirtyGLLine = true;
    }
    _dirty = true;
}
// _________________________________________________________________________________________
void DrawNodeSmooth::drawTriangle(
    const Vec2& p1, const Vec2& p2, const Vec2& p3, const Color4B& color1, const Color4B& color2, const Color4B& color3)
{
    unsigned int vertex_count = 3;
    ensureCapacity(vertex_count);

    V2F_C4B_T2F a = { Vec2(p1.x, p1.y), color1, Tex2F(0.0, 0.0) };
    V2F_C4B_T2F b = { Vec2(p2.x, p2.y), color2, Tex2F(0.0, 0.0) };
    V2F_C4B_T2F c = { Vec2(p3.x, p3.y), color3, Tex2F(0.0, 0.0) };

    V2F_C4B_T2F_Triangle* triangles = (V2F_C4B_T2F_Triangle*)(_buffer + _bufferCount);
    V2F_C4B_T2F_Triangle triangle = { a, b, c };
    triangles[0] = triangle;

    _bufferCount += vertex_count;
    _dirty = true;
}
// _________________________________________________________________________________________
void DrawNodeSmooth::drawLineTriangles(
    const Vec2& origin, const Vec2& destination, const Color4F& col, float linew, float lineblurw)
{
    linew = linew / 2;

    //уравнение прямой
    float A = origin.y - destination.y;
    float B = destination.x - origin.x;
    Vec2 AB(A, B);
    /////////////
    float mu = 1 / sqrtf(A * A + B * B);
    float mu2 = linew * mu;
    mu *= lineblurw;

    Vec2 newprev1, newprev2, newprev3, newprev4, newprev5, newprev6, newprev7, newprev8, newprev9;

    if (_FlagPotokLine) {
        newprev1 = prev1;
        newprev2 = prev2;
        newprev3 = newprev2;
        //////////
        newprev4 = prev4;
        newprev5 = prev5;
        newprev6 = newprev5;
        //////////
        newprev7 = prev7;
        newprev8 = prev8;
        newprev9 = newprev8;
    }
    else {
        newprev1 = origin + AB * (mu2 + mu);
        newprev2 = origin + AB * mu2;
        newprev3 = newprev2;
        //////////
        newprev4 = origin + AB * mu2;
        newprev5 = origin - AB * mu2;
        newprev6 = newprev5;
        //////////
        newprev7 = origin - AB * mu2;
        newprev8 = origin - AB * (mu2 + mu);
        newprev9 = newprev8;
    }

    prev1 = destination + AB * (mu2 + mu);
    prev2 = destination + AB * mu2;
    prev3 = prev1;
    ///////////
    prev4 = destination + AB * mu2;
    prev5 = destination - AB * mu2;
    prev6 = prev4;
    ////////////
    prev7 = destination - AB * mu2;
    prev8 = destination - AB * (mu2 + mu);
    prev9 = prev7;

    Color4B col4b(col);
    Color4B col4bAlpha(col4b.r, col4b.g, col4b.b, 0);

    drawTriangle(newprev1, newprev2, prev1, col4bAlpha, col4b, col4bAlpha);
    drawTriangle(prev2, prev3, newprev3, col4b, col4bAlpha, col4b);
    drawTriangle(newprev4, newprev5, prev4, col4b, col4b, col4b);
    drawTriangle(prev5, prev6, newprev6, col4b, col4b, col4b);
    drawTriangle(newprev7, newprev8, prev7, col4b, col4bAlpha, col4b);
    drawTriangle(prev8, prev9, newprev9, col4bAlpha, col4b, col4bAlpha);
}
// _________________________________________________________________________________________
void DrawNodeSmooth::drawQuadBezierTriangles(const Vec2& origin, const Vec2& control, const Vec2& destination,
    unsigned int segments, const Color4F& color, float linew, float lineblurw)
{
    Vec2* vertices = new (std::nothrow) Vec2[segments + 2];
    if (!vertices)
        return;

    float t = 0.0f;
    for (unsigned int i = 0; i < segments + 1; i++) {
        vertices[i].x = powf(1 - t, 2) * origin.x + 2.0f * (1 - t) * t * control.x + t * t * destination.x;
        vertices[i].y = powf(1 - t, 2) * origin.y + 2.0f * (1 - t) * t * control.y + t * t * destination.y;
        t += 1.0f / segments;
    }

    _FlagPotokLine = false;
    for (unsigned int i = 0; i < segments; i++) {
        drawLineTriangles(vertices[i], vertices[i + 1], color, linew, lineblurw);
        _FlagPotokLine = true;
    }

    CC_SAFE_DELETE_ARRAY(vertices);
}
// _________________________________________________________________________________________
