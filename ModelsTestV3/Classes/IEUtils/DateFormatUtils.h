//
//  DateFormatUtils.h
//  Exeriment
//
//  Created by d.antonov on 26.08.16.
//  Copyright © 2016 Nexters. All rights reserved.
//
#pragma once

#include "Global.h"

namespace ie {

/// через двоеточие
/// болше дня -> дни:часы
/// меньше дня -> часы:мин
/// меньше часа -> мин:сек
std::string formatTime2Digital(time_t timeSeconds);

/// буквенная форма "2 часа 43 минут 12 секунд"
/// showFull == false не показывать то чего 0 ("2 часа" а не "2 часа 0 минут 0 секунд")
std::string formatTime(time_t timeSeconds, bool showFull = false);
/// буквенная форма "2ч. 43м. 12с."
/// showFull == false не показывать то чего 0 ("2ч." а не "2ч. 0м. 0с.")
std::string formatTimeShort(time_t timeSeconds, bool showFull = false);
/// с двоеточиями 12:02:01, максимум часы. showFull = false: не будет показывать лидирующие разряды
std::string formatTimeHMS(time_t seconds, bool showFull = false);

std::string formatDateTime(time_t timeSeconds);
std::string formatDateTime(time_t timeSeconds, const std::string& dateTempl);
} // namespace ie
