//
//  DeviceUtils.h
//  Exeriment
//
//  Created by d.antonov on 26.08.16.
//  Copyright © 2016 Nexters. All rights reserved.
//
#pragma once

enum class ScreenType : int {
    STD = 0, ///< ipad2, ipad mini, SGS2
    RETINA_PHONE = 1, ///< iphone4, iphone 5, SGS3
    RETINA_PAD = 2, ///< ipad3, SGS4

    RESET = -1 ///< сброс
};

namespace ie {

/// тип экрана: STD, RETINA_PHONE, RETINA_PAD
ScreenType getScreenType();
void setScreenType(ScreenType st);
/// "lowres" "midres" "hires
std::string getScreenResolutionDirNameFor(ScreenType type);
std::string getScreenResolutionDirName();

const char* platformPathName(); // not used
const char* platformPathNameFor(int platform); // not used
bool isStandaloneBuild();
void rateApp();
std::string version();
bool isIPad();

} // namespace ie
