//
//  UtilsLocaleReqTemplate.cpp
//  Experiment
//
//  Created by d.dolgikh on 23.06.17.
//

#include "UtilsLocaleReqTemplate.hpp"
#include "StringFormatUtils.h"

struct LocaleReq {
    std::string sActiveAll;
    std::string sActiveNum;
    std::string sDeactiveAll;
    std::string sDeactiveNum;
};

static const LocaleReq sDefault{
    "", "", // sActiveAll, sActiveNum
    "", "" // sDeactiveAll, sDeactiveNum
};

static const LocaleReq sExpeditionIslandInfoWindow{
    "^{105 154 0}^- %s ^{image: v_symbol.png dy:2 dx:2 scale:0.5}^", "", //
    "^{102 51 0}^- %s", "^{220 0 0}^%s^{102 51 0}^" //
};

// _________________________________________________________________________________________
const LocaleReq& getLocaleReq(ie::LocaleReqTemplate templ)
{
    switch (templ) {
        case ie::LocaleReqTemplate::Default:
            return sDefault;
        case ie::LocaleReqTemplate::ExpeditionIslandInfoWindow:
            return sExpeditionIslandInfoWindow;
    }
    return sDefault;
}
// _________________________________________________________________________________________
std::string ie::updateLocaleUseTemplateAll(const std::string& str, LocaleReqTemplate templ, bool active)
{
    const auto& locale = getLocaleReq(templ);
    const auto& format = active ? locale.sActiveAll : locale.sDeactiveAll;
    if (not format.empty())
        return ie::strf(format.c_str(), str.c_str());
    return str;
}
// _________________________________________________________________________________________
std::string ie::updateLocaleUseTemplateNum(const std::string& str, ie::LocaleReqTemplate templ, bool active)
{
    const auto& locale = getLocaleReq(templ);
    const auto& format = active ? locale.sActiveNum : locale.sDeactiveNum;
    if (not format.empty())
        return ie::strf(format.c_str(), str.c_str());
    return str;
}
// _________________________________________________________________________________________s
