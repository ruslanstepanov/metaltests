//
//  DebugUtils.h
//  Exeriment
//
//  Created by d.antonov on 25.08.16.
//  Copyright © 2016 Nexters. All rights reserved.
//
#include "DebugUtils.h"
//#include "AStatistic.h"
#include "UIUtils.h"
#include "DateAndTimeUtils.h"
//#include "Platform.hpp"
//#include "AWindow.h"
//#include "ButtonsController.h"
//#include "AButton.h"

#include "Utils.h" // для ENABLE_UNIT_TEST
//#include "PlatformIndependentConst.h"

#include "IELog.hpp"

static std::string sDebugTimerName = "";

static double sDebugTimerStart = 0.0;
static const int sStringBufSize = 1024;

// _________________________________________________________________________________________
void ie::debugMessage(const char* pszFormat, ...)
{
    static int sWarnCount = 4;
    if (!sWarnCount)
        return;
    sWarnCount--;

    char* szBuf;

    va_list ap;
    va_start(ap, pszFormat);

    szBuf = (char*)malloc(sStringBufSize * sizeof(char));

    vsnprintf(szBuf, sStringBufSize, pszFormat, ap);
    va_end(ap);

    if (DEV_MOD)
        showUIMessage(szBuf, "Debug Message");
    else
        ie::log("ERROR: %s", szBuf);

    free(szBuf);
}
// _________________________________________________________________________________________
void ie::debugMessageImportant(const char* pszFormat, ...)
{
    char* szBuf;

    va_list ap;
    va_start(ap, pszFormat);

    szBuf = (char*)malloc(sStringBufSize * sizeof(char));

    vsnprintf(szBuf, sStringBufSize, pszFormat, ap);
    va_end(ap);

    if (DEV_MOD)
        showUIMessage(szBuf, "Debug Message");
    else
        ie::log("ERROR: %s", szBuf);

    free(szBuf);
}
// _________________________________________________________________________________________
void ie::startDebugTimer()
{
    sDebugTimerStart = ie::time();
    sDebugTimerName = "";
}
// _________________________________________________________________________________________
void ie::startDebugTimer(std::string label)
{
    sDebugTimerStart = ie::time();
    sDebugTimerName = label;
}
// _________________________________________________________________________________________
double ie::getDebugTimer()
{
    return ie::time() - sDebugTimerStart;
}
// _________________________________________________________________________________________
double ie::showDebugTimer()
{
    double dt = ie::time() - sDebugTimerStart;
    ie::log("%s %.8lf", sDebugTimerName.c_str(), dt);
    return dt;
}
// _________________________________________________________________________________________
void ie::debugBustOnOff()
{
    ENABLE_AUTO_BOOST = !ENABLE_AUTO_BOOST;
}
// _________________________________________________________________________________________
void ie::debugWindowLink()
{
    ENABLE_DIALOG_TEST = !ENABLE_DIALOG_TEST;
}
// _________________________________________________________________________________________
void ie::debugInfoOnOff()
{
    ENABLE_UNIT_TEST = !ENABLE_UNIT_TEST;
}
// _________________________________________________________________________________________
bool ie::debugInfo()
{
    //    if (not ASK_FB_ID)
    //        return false;

    return ENABLE_UNIT_TEST;
}
// _________________________________________________________________________________________
void ie::debugTapZoneOnOff()
{
    ENABLE_DEBUG_ALWAYS_SHOW_TAP_ZONE = !ENABLE_DEBUG_ALWAYS_SHOW_TAP_ZONE;
    ENABLE_DEBUG_TAP_ZONE = !ENABLE_DEBUG_TAP_ZONE;
}
// _________________________________________________________________________________________
void ie::updateDebugButtonIndication(AWindow* win)
{
    if (!win)
        return;

    //LogicError("unimpl");

    //    cocos2d::Node* subWindows = win->getWindows();
    //    if (!subWindows || !subWindows->getChildren())
    //        return;
    //
    //    AWindow* subWin = nullptr;
    //    CCARRAY_FOREACH_TYPE(subWindows->getChildren(), subWin, AWindow*)
    //    {
    //        if (!subWin)
    //            continue;
    //
    //        ie::updateDebugButtonIndication(subWin);
    //
    //        ButtonsController* bController = subWin->getButtonsController();
    //        subWin = nullptr; // Надо вернуть нулевое значение для след итерации
    //        if (!bController)
    //            continue;
    //
    //        for (AButton* button : bController->getButtons())
    //            button->updateDebugBorder(ENABLE_DEBUG_ALWAYS_SHOW_TAP_ZONE, 0.0f);
    //    }
}
// _________________________________________________________________________________________
