//
//  DeviceUtils.cpp
//  Exeriment
//
//  Created by d.antonov on 26.08.16.
//  Copyright © 2016 Nexters. All rights reserved.
//
#include "DeviceUtils.h"
//#include "Platform.hpp"
#include "PlatformIndependentConst.h"
#include "Utils.h"
#include "GUIUtils.h"
#include "IdentifyVersion.h"
#include "ScreenNotches.hpp"
#include <sys/utsname.h>

static ScreenType* sScreenType = nullptr;

// _________________________________________________________________________________________
ScreenType ie::getScreenType()
{
    if (sScreenType == nullptr) {
        static const float sMinWidthForMid = 1280;
        static const float sMinHeightForMid = 854;

        // поменял 1920 на 2100, формально надо и sMinWidthForMid изменит на 1386
        static const float sMinWidthForHi = 2100;
        static const float sMinHeightForHi = 1280;

        sScreenType = new ScreenType;

        auto winSize = cocos2d::Director::getInstance()->getWinSizeInPixels();
        const auto& notches = getNotches();
        winSize.width -= notches.side * 2;
        winSize.height -= notches.top + notches.bottom;

        if (winSize.equals(cocos2d::Size::ZERO)) {
            LogicError(" can't detect screen type");
            return ScreenType::RESET;
        }

        if (winSize.height < sMinHeightForMid || winSize.width < sMinWidthForMid) {
            *sScreenType = ScreenType::STD;
        }
        else if (winSize.height < sMinHeightForHi || winSize.width < sMinWidthForHi) {
            *sScreenType = ScreenType::RETINA_PHONE;
        }
        else {
            *sScreenType = ScreenType::RETINA_PAD;
        }

#if P_ANDROID
        float scale = SPlatform()->getContentScale();
#pragma warn TODO calculate screen type for android tablets
#endif
    }
    return *sScreenType;
}
// _________________________________________________________________________________________
void ie::setScreenType(ScreenType st)
{
    if (st == ScreenType::RESET) {
        delete sScreenType;
        sScreenType = nullptr;
        return;
    }

    if (sScreenType == nullptr)
        sScreenType = new ScreenType();

    *sScreenType = st;
}
// _________________________________________________________________________________________
std::string ie::getScreenResolutionDirNameFor(ScreenType type)
{
    switch (type) {
        case ScreenType::RETINA_PAD:
            return "hires";

        case ScreenType::RETINA_PHONE:
            return "midres";

        default:
            break;
    }

    return "lowres";
}
// _________________________________________________________________________________________
std::string ie::getScreenResolutionDirName()
{
    return getScreenResolutionDirNameFor(getScreenType());
}
// _________________________________________________________________________________________
const char* ie::platformPathName()
{
    return platformPathNameFor(CC_TARGET_PLATFORM);
}
// _________________________________________________________________________________________
const char* ie::platformPathNameFor(int platform)
{
    switch (platform) {
        case CC_PLATFORM_ANDROID:
            return "android";

        case CC_PLATFORM_IOS:
            return "ios";

        case CC_PLATFORM_MAC:
#if MAC_EMULATION_PLATFORM == CC_PLATFORM_ANDROID
            return "android";
#elif MAC_EMULATION_PLATFORM == CC_PLATFORM_IOS
            return "ios";
#else
#error unknown MAC_EMULATION_PLATFORM
#endif
        default:
            break;
    }
    return "";
}
// _________________________________________________________________________________________
bool ie::isStandaloneBuild()
{
    return (strcmp(ASSETS_POLICY, "standalone") == 0);
}
// _________________________________________________________________________________________
void ie::rateApp()
{
    LogicError("unimpl");

    std::string url;
    if (P_IOS) {
        std::string str = SPlatform()->getAppId();
        url = "itms-apps://itunes.apple.com/app/id" + str;
    }
    else if (P_ANDROID) {
        std::string str = SPlatform()->getPkgName();
        url = "market://details?id=" + str;
    }
    else if (P_MAC) {
        ie::log("AppRateQuest::rate - mac ignor");
    }
    else {
        IEAssert(false, "bad platform");
    }

    SPlatform()->runApp(url);
}
// _________________________________________________________________________________________
std::string ie::version()
{
#ifdef LOCAL_VERSION
    return SPlatform()->getVersion();
#else
    static std::string ver;
    if (ver.empty()) {
        auto pos = sBuildVersion.find_last_of(".");
        if (pos != std::string::npos)
            ver = sBuildVersion.substr(0, pos);
        else
            LogicError("Bad version sBuildVersion");
    }
    return ver;
#endif
}
// _________________________________________________________________________________________
bool ie::isIPad()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    struct utsname systemInfo;
    uname(&systemInfo);
    std::string platform = systemInfo.machine;

    if (platform.find("iPad") == std::string::npos)
        return false;
    else
        return true;

#else
    return false;
#endif
}
// _________________________________________________________________________________________

// _________________________________________________________________________________________
