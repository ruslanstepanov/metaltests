//
//  AuxUtils.h
//  Exeriment
//
//  Created by d.antonov on 26.08.16.
//  Copyright © 2016 Nexters. All rights reserved.
//
#pragma once

//#include "Enums.pb.h"

class MPoint;
class BuildingAsMapObject;
class LandWrapper;
class AButton;

namespace ie {
/// Вспомогательная структура для сохранения и восстановления параметров некоторой ноды
struct NodeContext {
    NodeContext() = default;
    NodeContext(cocos2d::Node* node);

    cocos2d::Vec2 ap;
    float sx = 0.0f;
    float sy = 0.0f;
    float posY = 0.0f;
    float posX = 0.0f;
    float skewX = 0.0f;
    float skewY = 0.0f;
    uint8_t o;
    cocos2d::Color3B mulColor;
    cocos2d::Color3B addColor;
    cocos2d::Node* outline = nullptr;

    void save(cocos2d::Node* node);
    void load(cocos2d::Node* node);
};

cocos2d::Size ccsMult(const cocos2d::Size& s, float ratio);
cocos2d::Size ccsMult(const cocos2d::Size& s, float ratio_w, float ratio_h);
cocos2d::Size ccsMinus(const cocos2d::Size& s1, const cocos2d::Size& s2);
cocos2d::Size ccsPlus(const cocos2d::Size& s1, const cocos2d::Size& s2);
cocos2d::Size ccsMin(const cocos2d::Size& s1, const cocos2d::Size& s2);
cocos2d::Size ccsMax(const cocos2d::Size& s1, const cocos2d::Size& s2);

bool ccrContains(const cocos2d::Rect& r, const cocos2d::Vec2& p);

bool testTouchLocation(cocos2d::Vec2 p);

double calcZFromPoint(cocos2d::Vec2 p);
double calcZFromDeltaY(double);

uint randomBoolFor(uint var);
bool isNaNPoint(const cocos2d::Vec2& p);
unsigned long ccNextPOT(unsigned long x);
double getDistance(const MPoint& p1, const MPoint& p2);
///	умножение с округлением как на сервере
int roundMultiply(double num, double coeff);
/// Посчитать есть ли покадание по ноде для тача
bool calcIsSwallow(BuildingAsMapObject* bld, cocos2d::Node* node, const cocos2d::Vec2& touch);

/// Создать иконку из ленд объекта и вписать ее в указанный размер.
/// @param landObjectId Id ленд объекта.
/// @param iconBounds В эти размеры будет вписана иконка.
/// @return Иконка, вписанная в указанный размер.
cocos2d::Node* createIconFromLandObject(PrototypeId landObjectId, cocos2d::Size iconBounds);

/// Создает картинку в серых тонах из предоставленной картинки
cocos2d::Node* createGrayscaleImage(cocos2d::Node* image);
/// Делает ноду черно-белой, подменяя ее шейдер и шейдеры всех дочерних нод на grayscale шейдер. Данное действие
/// необратимо, т.к. информация о предыдущих шейдерах не сохраняется.
// todo доработать общий шейдер по аналогии с additiveColor, чтобы с его помощью можно было отрисовывать объекты
// todo в серых тонах. Данную доработку необходимо будет сделать только в случае, если фича будет востребована
// todo (в данный момент подобное поведение необходимо только для неактивных итемов магазина)
void makeNodeGrayscale(cocos2d::Node* node);

/// Приводит указанное значение к интервалу [lower, upper] (обе границы включены)
template <typename T> T clampValue(const T& n, const T& lower, const T& upper)
{
    return std::max(lower, std::min(n, upper));
}

/// Приводит указанное значение зума к допустимым границам для данного ленда.
/// @param zoomValue Значение, которое необходимо привести
/// @param landWrapper Ленд, из которого берется допустимый интервал для камеры
/// @return Значение внутри допустимого интервала зума для данного ленда
float clampZoom(float zoomValue, const LandWrapper* landWrapper);

/// Найти минимальное смещение второго прямоугольника, при котором он впишется в прямоугольник-контейнер (при
/// невозможности вписаться он будет выравниваться по центру).
/// @param rectToInscribe Прямоугольник, который надо вписать в контейнер
/// @return Значение смещения, при котором второй прямоугольник будет вписан в контейнер
cocos2d::Vec2 getOffsetToInscribeRect(const cocos2d::Rect& container, const cocos2d::Rect& rectToInscribe);

/// Найти позицию указывающей стрелки относительно кнопки в зависимости от типа стрелки
cocos2d::Vec2 getIndicationArrowPosition(Protobuf::GuiArrowType arrowType, cocos2d::Node* target);

/// Записать сообщение в лог и показать попап с этим сообщением
void logWithPopup(const std::string& msg);

/// Поместить строки в терминал
void putLinesToTerminal(const StringVec& lines);

/// Поменять анкор поинт ноды, не меняя ее позиции (обновятся координаты ноды)
void setAnchorPointPreservePosition(cocos2d::Node* node, cocos2d::Vec2 newAnchorPoint);

bool isLineIntersectRect(const cocos2d::Vec2& v1, const cocos2d::Vec2& v2, const cocos2d::Rect& rect);
bool isTriangleIntersect(const std::vector<cocos2d::Vec2>& t1, const std::vector<cocos2d::Vec2>& t2);
float getTriangleArea(const cocos2d::Vec2& v1, const cocos2d::Vec2& v2, const cocos2d::Vec2& v3);
bool pointInTriangle(const std::vector<cocos2d::Vec2>& t, const cocos2d::Vec2& point);

void blur(std::vector<uint8_t>& to, int32_t sizeX);
///  "#123456" -> ccColor3B
cocos2d::Color3B hexStrToColor(const std::string& str, const cocos2d::Color3B& def);

/*_cocos3
/// сравнивает два аргумента (left compare right)
template <typename T> bool compareType(T left, const Protobuf::CompareType& compare, T right);
_cocos3*/
} // namespace ie
/*_cocos3
// _________________________________________________________________________________________
template <typename T> bool ie::compareType(T left, const Protobuf::CompareType& compare, T right)
{
    switch (compare) {
        case Protobuf::CompareType::CMP_EQUAL:
            return left == right;
        case Protobuf::CompareType::CMP_NOT_EQUAL:
            return left != right;
        case Protobuf::CompareType::CMP_GREATER:
            return left > right;
        case Protobuf::CompareType::CMP_LESS:
            return left < right;
        case Protobuf::CompareType::CMP_GREATER_EQUAL:
            return left >= right;
        case Protobuf::CompareType::CMP_LESS_EQUAL:
            return left <= right;
    }

    return false;
}
 _cocos3*/
// _________________________________________________________________________________________
