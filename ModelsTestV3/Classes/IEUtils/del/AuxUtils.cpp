//
//  AuxUtils.cpp
//  Exeriment
//
//  Created by d.antonov on 26.08.16.
//  Copyright © 2016 Nexters. All rights reserved.
//
#include "AuxUtils.h"
#include "GUIUtils.h"
#include "MapDataType.h"
#include "MathUtils.h"
#include "Utils.h"
#include "LandsProxy.hpp"
#include "LandObjectWrapper.hpp"
#include "LandNode.hpp"
#include "SpriteMaker.h"
#include "LandWrapper.hpp"
#include "AButton.h"
#include "PopUpController.h"
#include "TerminalWindowLogic.hpp"

// #include "BuildingAsMapObject.h"

#define WHITE_PIXEL "whitePixel.pvr.ccz"

// _________________________________________________________________________________________
cc::Size ie::ccsMult(const cc::Size& s, float ratio)
{
    return cc::Size(s.width * ratio, s.height * ratio);
}
// _________________________________________________________________________________________
cc::Size ie::ccsMult(const cc::Size& s, float ratio_w, float ratio_h)
{
    return cc::Size(s.width * ratio_w, s.height * ratio_h);
}
// _________________________________________________________________________________________
cc::Size ie::ccsMin(const cc::Size& s1, const cc::Size& s2)
{
    return cc::Size(std::min(s1.width, s2.width), std::min(s1.height, s2.height));
}
// _________________________________________________________________________________________
cc::Size ie::ccsMinus(const cc::Size& s1, const cc::Size& s2)
{
    float width = s1.width - s2.width;
    float height = s1.height - s2.height;

    return cc::Size(width >= 0.0f ? width : 1.0f, height >= 0.0f ? height : 1.0f);
}
// _________________________________________________________________________________________
cc::Size ie::ccsMax(const cc::Size& s1, const cc::Size& s2)
{
    return cc::Size(std::max(s1.width, s2.width), std::max(s1.height, s2.height));
}
// _________________________________________________________________________________________
cc::Size ie::ccsPlus(const cc::Size& s1, const cc::Size& s2)
{
    float width = s1.width + s2.width;
    float height = s1.height + s2.height;

    return cc::Size(width >= 0.0f ? width : 1.0f, height >= 0.0f ? height : 1.0f);
}
// _________________________________________________________________________________________
bool ie::ccrContains(const cc::Rect& r, const cc::Vec2& p)
{
    return (r.origin.x <= p.x && r.origin.y <= p.y && r.origin.x + r.size.width >= p.x
        && r.origin.y + r.size.height >= p.y);
}
// _________________________________________________________________________________________
bool ie::testTouchLocation(cc::Vec2 p)
{
    if (p.x < 1 || p.x > ie::screenSize().width || p.y < 1 || p.y > ie::screenSize().height) {
        ie::log("Bad touch %.3f %.3f", p.x, p.y);
        return false;
    }
    return true;
}
// _________________________________________________________________________________________
double ie::calcZFromPoint(cc::Vec2 p)
{
    double x = p.x + 10000;
    double y = p.y;

    double zd = -10.0 - y * 0.01 + x * 0.0001;
    return zd * 10000;
}
// _________________________________________________________________________________________
double ie::calcZFromDeltaY(double dY)
{
    const double z0 = (-10.0 + 10000 * 0.0001) * 10000;
    double z1 = 0;
    {
        const double x = 10000;
        double y = dY;

        double zd = -10.0 - y * 0.01 + x * 0.0001;
        z1 = zd * 10000;
    }
    return z1 - z0;
}
// _________________________________________________________________________________________
uint ie::randomBoolFor(uint var)
{
    int x = (var << 13) ^ var;
    return ((x * (x * x * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0;
};
// _________________________________________________________________________________________
bool ie::isNaNPoint(const cc::Vec2& p)
{
    if (p.x != p.x || p.y != p.y)
        return true;
    else
        return false;
};
// _________________________________________________________________________________________
unsigned long ie::ccNextPOT(unsigned long x)
{
    x = x - 1;
    x = x | (x >> 1);
    x = x | (x >> 2);
    x = x | (x >> 4);
    x = x | (x >> 8);
    x = x | (x >> 16);
    return x + 1;
}
// _________________________________________________________________________________________
double ie::getDistance(const MPoint& p1, const MPoint& p2)
{
    int dx = p2.x - p1.x;
    int dy = p2.y - p1.y;

    return sqrt(dx * dx + dy * dy);
}
// _________________________________________________________________________________________
int ie::roundMultiply(double num, double coeff)
{
    return ie::math::roundMultiply(num, coeff);
}
// _________________________________________________________________________________________
bool ie::calcIsSwallow(BuildingAsMapObject* bld, cc::Node* node, const cc::Vec2& touch)
{
    LogicError("unimple");

    //    if (!bld->getIsActive() || !node)
    //        return false;
    //
    //    cc::Vec2 pos = node->getPosition();
    //    pos -= bld->getAnchorPointInPoints();
    //    cc::Vec2 origin = node->getAnchorPointInPoints();
    //    cc::Size size = node->getContentSize();
    //    float scaleX = node->getScaleX();
    //    float scaleY = node->getScaleY();
    //
    //    float x = pos.x - origin.x * scaleX;
    //    float y = pos.y - origin.y * scaleY;
    //
    //    float w = size.width * scaleX;
    //    float h = size.height * scaleY;
    //
    //    cc::Rect bounds = cc::Rect(x, y, w, h);
    //    if (!bounds.containsPoint(touch))
    //        return false;
    //
    //    // попали
    //    if (ENABLE_DEBUG_TAP_ZONE) {
    //        cc::Sprite* s = cc::Sprite::create(WHITE_PIXEL);
    //
    //        s->setScaleX(w / scaleX);
    //        s->setScaleY(h / scaleY);
    //        s->setAnchorPoint(cc::Vec2::ZERO);
    //        s->setOpacity(80);
    //        s->setColor(ccGREEN);
    //        //		s->setPosition(cc::Vec2(x, y));
    //        node->addChild(s);
    //
    //        s->runAction(CCSequence::createWithTwoActions(CCFadeTo::create(1.0f, 0),
    //            CCCallFunc::create(s, callfunc_selector(cc::Node::removeFromParentAndCleanupTrue))));
    //    }

    return true;
}
// _________________________________________________________________________________________
cc::Node* ie::createIconFromLandObject(PrototypeId landObjectId, cc::Size iconBounds)
{
    cc::Node* icon = nullptr;
    if (auto* landObj = LandsProxy::Instance()->createTempObject(landObjectId)) {
        icon = landObj->getInstance().getViewMut().createView(true);
        if (icon) {
            const auto& iconSize = icon->getContentSize();
            if (iconSize.width != 0.0f && iconSize.height != 0.0f)
                icon->setScale(std::min(iconBounds.height / iconSize.height, iconBounds.width / iconSize.width));
        }
    }

    return icon;
}
// _________________________________________________________________________________________
float ie::clampZoom(float zoomValue, const LandWrapper* landWrapper)
{
    if (landWrapper == nullptr)
        return zoomValue;

    auto landCameraZoomInterval = landWrapper->getLandCameraZoomInterval();

    return clampValue(zoomValue, landCameraZoomInterval.first, landCameraZoomInterval.second);
}
// _________________________________________________________________________________________
cc::Vec2 ie::getOffsetToInscribeRect(const cc::Rect& container, const cc::Rect& rectToInscribe)
{
    // Обработка отступов в направлении одной оси. Первые 2 параметра должны относиться к соответствующим сторонам
    // прямоугольников в отрицательном направлении по оси, если считать центральную точку нулем, вторые 2 параметра -
    // к соответствующим сторонам в положительном направлении оси.
    auto processDirection = [](float sideValue, float otherRectSideValue, float oppositeSideValue,
                                float otherRectOppositeSideValue) -> float {
        // Для простоты первые два параметра будем считать условно "левой" стороной, вторые 2 - "правой"

        float offset = 0.0f;
        bool sideOffsetUsed = false;

        // Обрабатываем "левые" границы прямоугольников
        if (otherRectSideValue < sideValue) {
            // Прямоугольник нужно сдвинуть "вправо"
            offset = sideValue - otherRectSideValue;

            // Обновим значения для последующего расчета отступа "справа"
            otherRectSideValue = sideValue;
            otherRectOppositeSideValue += offset;

            // Поднимем данный флаг, обозначающий, что второй прямоугольник был смещен "вправо" и сейчас
            // "левые" границы прямоугольников совпадают
            sideOffsetUsed = true;
        }

        if (otherRectOppositeSideValue > oppositeSideValue) {
            if (sideOffsetUsed) {
                // "Левые" границы прямоугольников совпадают, но второй прямоугольник выходит за "правую" границу.
                // В этом случае центрируем второй прямоугольник
                offset -= (otherRectOppositeSideValue - oppositeSideValue) * 0.5f;
            }
            else {
                // Прямоугольник нужно сдвинуть "влево" (в данном случае отступ будет отрицательным)
                offset = oppositeSideValue - otherRectOppositeSideValue;

                // Обновим значения для последующего расчета отступа "слева" (его не было раньше, но после сдвига
                // "влево он мог появиться")
                otherRectSideValue += offset;

                if (otherRectSideValue < sideValue) {
                    // "Правые" границы прямоугольников совпадают, но второй прямоугольник выходит за "левую" границу.
                    // В этом случае центрируем второй прямоугольник
                    offset += (sideValue - otherRectSideValue) * 0.5f;
                }
            }
        }

        return offset;
    };

    // Горизонтальный отступ считаем слева направо (в направлении оси X)
    float horizontalOffset = processDirection(container.origin.x, rectToInscribe.origin.x,
        container.origin.x + container.size.width, rectToInscribe.origin.x + rectToInscribe.size.width);
    // Вертикальный отступ считаем снизу вверх (в направлении оси Y)
    float verticalOffset = processDirection(container.origin.y, rectToInscribe.origin.y,
        container.origin.y + container.size.height, rectToInscribe.origin.y + rectToInscribe.size.height);

    return cc::Vec2(horizontalOffset, verticalOffset);
}
// _________________________________________________________________________________________
cc::Vec2 ie::getIndicationArrowPosition(Protobuf::GuiArrowType arrowType, cc::Node* target)
{
    // Считаем, что у нас 2 типа стрелок - вертикальные (указывающие вниз) и горизонтальные (указывающие влево).
    // Вертикальная стрелка указывает на центр верхней границы кнопки, горизонтальная - на центр правой границы
    // кнопки (горизонтальная стрелка будет чуть выше половины кнопки по высоте за счет полупрозрачной тени
    // у кнопки)
    cc::Vec2 arrowPosition;
    switch (arrowType) {
        case Protobuf::GuiArrowType::GUI_ARROW_H: {
            arrowPosition = cc::Vec2(target->getContentSize().width, target->getContentSize().height * 0.6f);
            break;
        }
        case Protobuf::GuiArrowType::GUI_ARROW_V: {
            arrowPosition = cc::Vec2(target->getContentSize().width * 0.5f, target->getContentSize().height);
            break;
        }
        case Protobuf::GuiArrowType::GUI_SWF_TAP:
        case Protobuf::GuiArrowType::GUI_SWF_SWIPE: {
            arrowPosition = cc::Vec2(target->getContentSize().width * 0.5f, target->getContentSize().height * 0.5f);
            break;
        }
    }

    return arrowPosition;
}
// _________________________________________________________________________________________
void ie::logWithPopup(const std::string& msg)
{
    ie::log(msg);
    if (SPopUpController())
        SPopUpController()->addPopupToQueue(msg);
}
// _________________________________________________________________________________________
void ie::putLinesToTerminal(const StringVec& lines)
{
    if (lines.empty())
        return;

    // Соберем все в одну строку, разделенную '\n'
    std::string linesText;
    for (auto& line : lines) {
        ie::logFast(line);
        if (not linesText.empty())
            linesText += "\n";

        linesText += line;
    }

    TerminalWindowLogic::addTermText(linesText);
}
// _________________________________________________________________________________________
void ie::setAnchorPointPreservePosition(cc::Node* node, cc::Vec2 newAnchorPoint)
{
    auto& prevAnchorPoint = node->getAnchorPoint();
    auto& nodeSize = node->getContentSize();

    // Посчитаем отступы от левого нижнего угла при старом и новом анкор поинтом и передвинем ноду
    auto prevOffsetFromBottomLeft = cc::Vec2(nodeSize.width * prevAnchorPoint.x, nodeSize.height * prevAnchorPoint.y);
    auto offsetFromBottomLeft = cc::Vec2(nodeSize.width * newAnchorPoint.x, nodeSize.height * newAnchorPoint.y);
    auto positionOffset = offsetFromBottomLeft - prevOffsetFromBottomLeft;

    node->setAnchorPoint(newAnchorPoint);
    node->setPosition(node->getPosition() + positionOffset);
}
// _________________________________________________________________________________________
bool ie::isLineIntersectRect(const cc::Vec2& v1, const cc::Vec2& v2, const cc::Rect& rect)
{
    const auto& leftBottom = rect.origin;
    const auto& rightTop = rect.origin + rect.size;
    // проверяем находиться ли начало или конец отрезка внутри прямоугольника
    if (rect.containsPoint(v1) || rect.containsPoint(v2))
        return true;
    // поочередно проверяем пересечения всех линий прямоугольника с заданым отрезком
    if (cc::Vec2::isSegmentIntersect(v1, v2, leftBottom, leftBottom + cc::Vec2(rect.size.width, 0)))
        return true;
    if (cc::Vec2::isSegmentIntersect(v1, v2, leftBottom, leftBottom + cc::Vec2(0, rect.size.height)))
        return true;
    if (cc::Vec2::isSegmentIntersect(v1, v2, leftBottom + cc::Vec2(rect.size.width, 0), rightTop))
        return true;
    if (cc::Vec2::isSegmentIntersect(v1, v2, leftBottom + cc::Vec2(0, rect.size.height), rightTop))
        return true;

    return false;
}
// _________________________________________________________________________________________
bool ie::isTriangleIntersect(const std::vector<cc::Vec2>& t1, const std::vector<cc::Vec2>& t2)
{
    if (t1.size() < 3 || t2.size() < 3)
        return false;
    std::pair<cc::Vec2, cc::Vec2> tl1[3] = { { t1[0], t1[1] }, { t1[1], t1[2] }, { t1[2], t1[0] } };
    std::pair<cc::Vec2, cc::Vec2> tl2[3] = { { t2[0], t2[1] }, { t2[1], t2[2] }, { t2[2], t2[0] } };

    // поочередно проверяем пересечения всех линий треугольников
    for (int i = 0; i < 3; ++i)
        for (int j = 0; j < 3; ++j)
            if (cc::Vec2::isSegmentIntersect(tl1[i].first, tl1[i].second, tl2[i].first, tl2[i].second))
                return true;
    return pointInTriangle(t1, t2[0]);
}
// _________________________________________________________________________________________
float ie::getTriangleArea(const cc::Vec2& v1, const cc::Vec2& v2, const cc::Vec2& v3)
{
    // считаем площадь треугольника
    return abs((v1.x - v3.x) * (v2.y - v3.y) - (v2.x - v3.x) * (v1.y - v3.y)) / 2.f;
}
// _________________________________________________________________________________________
bool ie::pointInTriangle(const std::vector<cc::Vec2>& t, const cc::Vec2& point)
{
    if (t.size() < 3)
        return false;
    // считаем площадь треугольника
    float s = getTriangleArea(t[0], t[1], t[2]);
    // считаем площадь треугольников образованных с помощью заданой точки
    float s1 = getTriangleArea(point, t[0], t[1]);
    float s2 = getTriangleArea(point, t[1], t[2]);
    float s3 = getTriangleArea(point, t[0], t[2]);
    // если сумма плошадей равна общей плошади треугольника то точка находиться внутри
    return abs((s1 + s2 + s3) - s) < FLT_EPSILON;
}
// _________________________________________________________________________________________
cc::Node* ie::createGrayscaleImage(cc::Node* image)
{
    auto imageSize = image->getContentSize();

    // Отрисуем картинку в рендер текстуру (полученная текстура заретейнится внутри картинки-результата,
    // саму рендер текстуру ретейнить не надо)
    cc::RenderTexture* renderTexture
        = cocos2d::RenderTexture::create(imageSize.width, imageSize.height, cc::Texture2D::PixelFormat::RGBA8888);

    ie::NodeContext context(image);
    image->setAnchorPoint(cc::Point::ZERO);
    image->setPosition(cc::Point::ZERO);
    image->setScaleX(1.0f);
    image->setScaleY(1.0f);

    cc::Director::getInstance()->getRenderer()->render();
    renderTexture->clear(0, 0, 0, 0);
    renderTexture->begin();
    image->visit();
    renderTexture->end();
    cocos2d::Director::getInstance()->getRenderer()->render();

    context.load(image);

    cc::Sprite* grayscaleImage = cc::Sprite::createWithTexture(renderTexture->getSprite()->getTexture());
    grayscaleImage->setFlippedY(true);
    grayscaleImage->setGLProgram(SpriteMaker::getGrayscaleShader());
    // Назначаем полученному изображению параметры оригинала
    context.load(grayscaleImage);

    return grayscaleImage;
}
// _________________________________________________________________________________________
void ie::makeNodeGrayscale(cc::Node* node)
{
    auto* grayscaleShader = SpriteMaker::getGrayscaleShader();

    // Рекурсивно назначим шейдер самой ноде и всем ее дочерним нодам
    node->setGLProgram(grayscaleShader);
    for (auto* childNode : node->getChildren()) {
        makeNodeGrayscale(childNode);
    }
}
// _________________________________________________________________________________________
static const uint sGaussStepsCount = 1; ///< число походов размытием
static const uint sGaussSize = 5;
static const double sGaus[] = {
    //  http://www.embege.com/gauss/
    0.0545, 0.2442, 0.4026, 0.2442, 0.0545
};
static inline uint sXYq(uint x, uint y, ie::uint2d size)
{
    return (size.y - y - 1) * size.x + x; // good Index
}
// _________________________________________________________________________________________
void ie::blur(std::vector<uint8_t>& to, int32_t sizeX)
{
    // clang-format off
    
    int32_t sizeY = int32_t(to.size()) / sizeX;
    ie::uint2d texSize(sizeX, sizeY);
    CCAssert(sGaussSize == 5, "expected sGaussSize == 5");
    
    // для сепарабельного размытия Гауссом
    ssize_t dataSize = to.size();
    uchar* tmp = new uchar[dataSize];
    
    int init_id = sXYq(0, 0, texSize);
    int x_1 = -sizeX;
    int x_2 = -(sizeX << 1);
    for (uint i = 0; i < sGaussStepsCount; ++i) {
        // по y
        for (uint x = 0; x < sizeX; ++x) {
            int id = init_id + x;
            tmp[id] = to[id] * sGaus[2] + to[id + x_1] * sGaus[3] + to[id + x_2] * sGaus[4];
            id += x_1;
            tmp[id] = to[id - x_1] * sGaus[1] + to[id] * sGaus[2] + to[id + x_1] * sGaus[3] + to[id + x_2] * sGaus[4];
            id += x_1;
            for (uint y = 2; y < sizeY - 2; ++y) {
                tmp[id] = to[id - x_2] * sGaus[0] + to[id - x_1] * sGaus[1] + to[id] * sGaus[2] + to[id + x_1] * sGaus[3] + to[id + x_2] * sGaus[4];
                id += x_1;
            }
            tmp[id] = to[id - x_2] * sGaus[0] + to[id - x_1] * sGaus[1] + to[id] * sGaus[2] + to[id + x_1] * sGaus[3];
            id += x_1;
            tmp[id] = to[id - x_2] * sGaus[0] + to[id - x_1] * sGaus[1] + to[id] * sGaus[2];
        }
        
        for (uint y = 0; y < sizeY; ++y) {
            int id = sXYq(0, y, texSize);
            to[id] = tmp[id] * sGaus[2] + tmp[id + 1] * sGaus[3] + tmp[id + 2] * sGaus[4];
            ++id;
            to[id] = tmp[id - 1] * sGaus[1] + tmp[id] * sGaus[2] + tmp[id + 1] * sGaus[3] + tmp[id + 2] * sGaus[4];
            ++id;
            for (uint x = 2; x < sizeX - 2; ++x) {
                to[id] = tmp[id - 2] * sGaus[0] + tmp[id - 1] * sGaus[1] + tmp[id] * sGaus[2] + tmp[id + 1] * sGaus[3] + tmp[id + 2] * sGaus[4];
                ++id;
            }
            to[id] = tmp[id - 2] * sGaus[0] + tmp[id - 1] * sGaus[1] + tmp[id] * sGaus[2] + tmp[id + 1] * sGaus[3];
            ++id;
            to[id] = tmp[id - 2] * sGaus[0] + tmp[id - 1] * sGaus[1] + tmp[id] * sGaus[2];
        }
    }
    
    delete[] tmp;
    // clang-format on
}
// _________________________________________________________________________________________
cc::Color3B ie::hexStrToColor(const std::string& str, const cc::Color3B& def)
{
    std::string hexCode = str;

    // ... and the target rgb integer values.
    int r, g, b;

    // Remove the hashtag ...
    if (hexCode.at(0) == '#') {
        hexCode = hexCode.erase(0, 1);
    }

    if (hexCode.size() < 6)
        return def;

    // ... and extract the rgb values.
    std::istringstream(hexCode.substr(0, 2)) >> std::hex >> r;
    std::istringstream(hexCode.substr(2, 2)) >> std::hex >> g;
    std::istringstream(hexCode.substr(4, 2)) >> std::hex >> b;

    return cc::Color3B(r, g, b);
}
// _________________________________________________________________________________________
#pragma mark NodeContext
ie::NodeContext::NodeContext(cc::Node* node)
{
    save(node);
}
// _________________________________________________________________________________________
void ie::NodeContext::save(cc::Node* node)
{
    ap = node->getAnchorPoint();
    sx = node->getScaleX();
    sy = node->getScaleY();
    posY = node->getPositionY();
    posX = node->getPositionX();
    skewX = node->getSkewX();
    skewY = node->getSkewY();
    o = node->getOpacity();
    mulColor = node->getColor();
    addColor = node->getAdditiveColor();

    if (cc::Node* o = node->getChildByTag(LandNode::sLandNodeOutlineTag)) {
        if (o->isVisible()) {
            outline = o;
            outline->setVisible(false);
        }
    }
}
// _________________________________________________________________________________________
void ie::NodeContext::load(cc::Node* node)
{
    node->setAnchorPoint(ap);
    node->setScaleX(sx);
    node->setScaleY(sy);
    node->setPositionY(posY);
    node->setPositionX(posX);
    node->setSkewX(skewX);
    node->setSkewY(skewY);
    node->setOpacity(o);
    node->setColor(mulColor);
    node->setAdditiveColor(addColor);
    if (outline)
        outline->setVisible(true);
}
