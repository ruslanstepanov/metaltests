//
//  vector2d.h
//  Experiment
//
//  Created by a.arsentiev on 12/04/17.
//
//

#pragma once

#include "CopyDisable.h"
#include "Int2d.h"

namespace ie {

template <class T_Cell> class Vector2d : CopyDisable {
public: // static:
    using Cell = T_Cell;
    using Vector1d = std::vector<Cell>;

public: // methods:
    Vector2d();
    Vector2d(uint2d size, Cell def);
    Vector2d(const Vector2d& other)
        : mYs(other.mYs)
        , mDefault(other.mDefault)
        , mSize(other.mSize)
    {
    }

    Vector2d& operator=(const Vector2d& other)
    {
        mYs = other.mYs;
        mDefault = other.mDefault;
        mSize = other.mSize;
        return *this;
    }

    Cell& operator[](uint2d);
    Cell& at(uint2d);
    Cell& at(uint16_t x, uint16_t y)
    {
        return at({ x, y });
    }

    const Cell& operator[](uint2d) const;
    const Cell& at(uint2d) const;
    const Cell& at(uint16_t x, uint16_t y) const
    {
        return at({ x, y });
    }

    // Vector1d& operator[](uint16_t);
    // const Vector1d& operator[](uint16_t) const;

    uint2d size() const;

    void resize(uint2d);
    void resize(uint16_t x, uint16_t y)
    {
        resize({ x, y });
    }

    bool check(uint2d) const;
    bool check(uint16_t x, uint16_t y) const
    {
        return check({ x, y });
    }

    void resetAll(Cell);

    void clear();

protected: // methods:
protected: // vars:
    /// данные
    std::vector<Vector1d> mYs;
    /// значение по умолчанию
    Cell mDefault = Cell();
    /// максимальный размер
    uint2d mSize;
};

#include "Vector2d_inl.h"
} // namespace ie
