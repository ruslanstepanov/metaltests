//
//  GUIUtils.h
//  Exeriment
//
//  Created by d.antonov on 26.08.16.
//  Copyright © 2016 Nexters. All rights reserved.
//
#pragma once

class AWindow;
class TableView;

namespace ie {
struct ScreenNotches;
};

enum class HorizontalAlignment { Left, Center, Right };
enum class VerticalAlignment { Top, Center, Bottom };

namespace ie {
struct Paddings {
    float left = 0.f;
    float top = 0.f;
    float right = 0.f;
    float bottom = 0.f;
};
/// масштабирование gui для типа экрана
float getGuiScale();
cocos2d::Size menuSize();
cocos2d::Vec2 menuShift(const cocos2d::Size& windowSize = cocos2d::Size::ZERO);

cocos2d::Size screenSize();
/// screenSize - getNotches
cc::Size screenSizeSafe();
/// отступы от краев экрана (чтобы вместить вырезы и прочую фигню)
const ie::ScreenNotches& getNotches();
/// включить/выключить отладочные отступы в экране как у iphone-x
void forceNotchesOnOff();
/// включить/выключить отладочную сетку, отображающую размеры минимального девайса
void minimalScreenGridOnOff();

cocos2d::Vec2 screenCenter();
const cocos2d::Size& tileSize();
/// растояние между соседними изо-тайлами [1,1] и [1,2]
float tileDistance();
cocos2d::Node* createScreenRect();
cc::Node* createTileArea();
cocos2d::Sprite* lineFrom(cocos2d::Vec2 from, cocos2d::Vec2 to);
cocos2d::Node* mapCell();

/// Поищет в окне стрелки и скролл-индикатор и присоединит их к таблице
void attacheControlsToTable(AWindow* container, TableView* tableView);

/// упорядочиваем детей ноды в линию
void layoutForNodes(cc::Node* parent, float border);

/// выставляем позицию node по центру размера parent
/// useScale изменять ли scale у node, чтобы она вписывалась в размеры parent
/// если useScale и onlyScaleReduce, тогда scale у node изменяется только в пределах (0, 1]
void fitToCenter(cc::Node* parent, cc::Node* node, bool useScale = true, bool onlyScaleReduce = true);

/// если цвет отличается то запустить экшен, иначе ничего не делать
/// stepTime время изменения цвета от 0 до 255 (127->255 за 0.5 * stepTime)
bool setAdditiveColorAnim(cc::Node* n, const cc::Color3B& c, float stepTime = 0.5f);
bool setColorAnim(cc::Node* n, const cc::Color3B& c, float stepTime = 0.5f);
/// если секйл отличается то запустить экшен, иначе ничего не делать
/// stepTime время изменения скейла х1 до х2 (х0.5->х2.7 за 2.2 * stepTime)
bool setScaleAnim(cc::Node* n, float s, float stepTime = 0.5f);
/// если прозрачность отличается то запустить экшен, иначе ничего не делать
/// stepTime время изменения от 0 до 255 (127->255 за 0.5 * stepTime)
bool setOpacityAnim(cc::Node* n, GLubyte o, float stepTime = 0.5f);

bool setProgressAnim(cc::ProgressTimer* n, float to, float stepTime = 0.5);
/// включить каскадные эффекты для себя и для детей
void setEnableCascadeEffectsToSelfAndChildren(cc::Node* n, bool val);
} // namespace ie
