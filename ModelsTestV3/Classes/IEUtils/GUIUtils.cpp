//
//  GUIUtils.cpp
//  Exeriment
//
//  Created by d.antonov on 26.08.16.
//  Copyright © 2016 Nexters. All rights reserved.
//
//#include "AuxUtils.h"
#include "DeviceUtils.h"
#include "GUIUtils.h"

//#include "Platform.hpp"
//#include "ScreenNotches.hpp"
//#include "PlatformIndependentConst.h"
#include "StatusBarProxy.hpp"

#include "AtlasCache.h"
#include "ScrollPageIndicator.h"
#include "TableArrows.h"
#include "TableView.h"
#include "TableViewController.h"
#include "ServerUrlData.h"

#define WHITE_PIXEL "whitePixel.pvr.ccz"
#define WHITE_PIXEL_NAA "whitePixel_NAA.png"

// _________________________________________________________________________________________
float ie::getGuiScale()
{
    float ret = 0.0f;

    switch (getScreenType()) {
        case ScreenType::STD:
            ret = 1.0f;
            break;
        case ScreenType::RETINA_PHONE:
            ret = 1.33333333f;
            break;
        case ScreenType::RETINA_PAD:
            ret = 2.0f;
            break;
    }

    return ret;
}
// _________________________________________________________________________________________
cc::Size ie::menuSize()
{
    cc::Size max;
    switch (ie::getScreenType()) {
        case ScreenType::STD:
            max.setSize(820, 570);
            break;
        case ScreenType::RETINA_PHONE:
            max.setSize(820, 570);
            break;
        case ScreenType::RETINA_PAD:
            max.setSize(1640, 1140);
            break;
    }

    return ccsMin(cc::Size(screenSize().width, screenSize().width), max) * 0.9f;
}
// _________________________________________________________________________________________
cc::Vec2 ie::menuShift(const cc::Size& windowSize)
{
    cc::Vec2 menuShift = cc::Vec2::ZERO;

    if (windowSize.width > 0.1f && windowSize.height > 0.1f) {
        menuShift
            = cc::Vec2((menuSize().width - windowSize.width) * 0.5f, (menuSize().height - windowSize.height) * 0.5f);
    }

    cc::Vec2 delta
        = cc::Vec2((screenSize().width - menuSize().width) / 2.0f, (screenSize().height - menuSize().height) / 2.0f);

    //    LogicError("unimpl");
    /*_cocos3_
    if (SAutoClicker() && SAutoClicker()->isTurbo()) {
        menuShift.y = 0;
        delta.y = 0;
    }
     _cocos3_*/
    return menuShift + delta;
}
// _________________________________________________________________________________________
cc::Size ie::screenSize()
{
    static bool sIsInit = false;
    static cc::Size sSize;

    if (!sIsInit) {
        sSize = cc::Director::getInstance()->getWinSize();
        sIsInit = true;
    }

    return sSize;
}
// _________________________________________________________________________________________
cc::Size ie::screenSizeSafe()
{
    cc::Size ret = screenSize();
    const auto& notches = getNotches();
    ret.width -= notches.side * 2;
    ret.height -= (notches.bottom + notches.top);
    return ret;
}
// _________________________________________________________________________________________
/// эта переменная должна переживать f5
static bool sForceUseNotches = false;
void ie::forceNotchesOnOff()
{
    sForceUseNotches = !sForceUseNotches;
}
// _________________________________________________________________________________________
const ie::ScreenNotches& ie::getNotches()
{
    if (UrlData::isRelease()) {
        return SPlatform()->getNotches();
    }

    static ie::ScreenNotches ret;
    if (sForceUseNotches) {
        float r = getGuiScale() / 2;
        ret = ie::ScreenNotches((long)(31 * r), (long)(63 * r), (long)(132 * r));
    }
    else {
        ret = SPlatform()->getNotches();
    }
    if (StatusBarProxy::Instance() && StatusBarProxy::Instance()->visible())
        ret.top += StatusBarProxy::Instance()->getUiShift() * getGuiScale();

    return ret;
}
// _________________________________________________________________________________________
cc::Vec2 ie::screenCenter()
{
    cc::Size s = ie::screenSize();
    return cc::Vec2(s.width * 0.5f, s.height * 0.5f);
}
// _________________________________________________________________________________________
const cc::Size& ie::tileSize()
{
    const static cc::Size tile = cc::Size(64, 32);
    return tile;
}
// _________________________________________________________________________________________
bool ie::checkIfInsideTile(const cc::Vec2& point, const cc::Vec2& tileCenter, const float tileScale)
{
    auto& tileSize = ie::tileSize();

    float tileHalfWidthScaled = tileSize.width * 0.5f * tileScale;
    float tileHalfHeightScaled = tileSize.height * 0.5f * tileScale;

    // Координаты точки относительно центра тайла
    cc::Vec2 pointRelative = point - tileCenter;

    // В нашем случае тайл представляет собой ромб, а значит точка находится внутри ромба, если
    // |x| / a + |y| / b <= 1,
    // где |x|, |y| - модули координат точки относительно центра, a - половина ширины ромба, b - половина высоты ромба
    return std::abs(pointRelative.x) / tileHalfWidthScaled + std::abs(pointRelative.y) / tileHalfHeightScaled <= 1.0f;
}
// _________________________________________________________________________________________
inline static double sCalcCellIso(double w, double h)
{
    w /= 2;
    h /= 2;
    return sqrt(w * w + h * h);
}
// _________________________________________________________________________________________
float ie::tileDistance()
{
    const static double sCellIsoSize = sCalcCellIso(ie::tileSize().width, ie::tileSize().height);
    return sCellIsoSize;
}
// _________________________________________________________________________________________
cc::Node* ie::createScreenRect()
{
    cc::Node* ret = cc::Node::create();

    // _
    // O
    //
    cc::Sprite* s = cc::Sprite::create("whitePixel.png");
    s->setAnchorPoint(cc::Vec2(0, 0));
    s->setPosition(cc::Vec2(0, ie::screenSize().height));
    s->setScaleX(ie::screenSize().width);
    s->setScaleY(10000);
    s->setColor(cc::Color3B(0, 0, 0));
    ret->addChild(s);

    //
    // O|
    //
    s = cc::Sprite::create("whitePixel.png");
    s->setAnchorPoint(cc::Vec2(0, 0.5f));
    s->setPosition(cc::Vec2(ie::screenSize().width, 0));
    s->setScaleX(10000);
    s->setScaleY(10000);
    s->setColor(cc::Color3B(0, 0, 0));
    ret->addChild(s);

    //
    // O
    // -
    s = cc::Sprite::create("whitePixel.png");
    s->setAnchorPoint(cc::Vec2(0, 1));
    s->setPosition(cc::Vec2(-1, 0));
    s->setScaleX(10000);
    s->setScaleY(10000);
    s->setColor(cc::Color3B(0, 0, 0));
    ret->addChild(s);

    //
    // |O
    //
    s = cc::Sprite::create("whitePixel.png");
    s->setAnchorPoint(cc::Vec2(1, 0.5f));
    s->setPosition(cc::Vec2(0, 0));
    s->setScaleX(10000);
    s->setScaleY(10000);
    s->setColor(cc::Color3B(0, 0, 0));
    ret->addChild(s);

    return ret;
}
// _________________________________________________________________________________________
cc::Node* ie::createTileArea()
{
    cc::Sprite* tile = cc::Sprite::create("whitePixel.pvr.ccz");
    if (tile == nullptr)
        return nullptr;

    tile->setAnchorPoint(cc::Vec2());
    tile->setRotation(-45.0f);
    tile->setScale(1);

    float k = 0.7071067811f; //= 1.0f / powf(2.0f, 0.5f);

    cc::Sprite* proxy = cc::Sprite::create("whitePixel.pvr.ccz");
    proxy->setTextureRect(cc::Rect(0, 0, 0, 0));

    proxy->setCascadeOpacityEnabled(true);
    proxy->setCascadeColorEnabled(true);

    proxy->setScaleX(ie::tileSize().width * k);
    proxy->setScaleY(ie::tileSize().height * k);
    proxy->setAnchorPoint(cc::Vec2(0.5f, 0.0f));
    proxy->addChild(tile);

    proxy->setOpacity(150);
    // proxy->setColor(cc::Color3B(0, 255, 0));

    return proxy;
}
// _________________________________________________________________________________________
cc::Sprite* ie::lineFrom(cc::Vec2 from, cc::Vec2 to)
{
    cc::Sprite* line = new cc::Sprite();
    line->initWithFile(WHITE_PIXEL);
    line->autorelease();

    line->setAnchorPoint(cc::Vec2(0, 0));
    line->setPosition(from);
    line->setScaleY(from.distance(to));
    line->setRotation(90.0f - RadToDeg((to - from).getAngle()));

    return line;
}
// _________________________________________________________________________________________
void ie::attacheControlsToTable(AWindow* container, TableView* tableView)
{
    if (!container || !tableView)
        return;

    cc::Node* rightArrow = dynamic_cast<cc::Node*>(container->getNode("right_arrow"));
    cc::Node* leftArrow = dynamic_cast<cc::Node*>(container->getNode("left_arrow"));
    cc::Node* upArrow = dynamic_cast<cc::Node*>(container->getNode("up_arrow"));
    cc::Node* downArrow = dynamic_cast<cc::Node*>(container->getNode("down_arrow"));

    TableArrows* ta = TableArrows::create();
    if (rightArrow && leftArrow) {
        ta->setRightArrow(rightArrow);
        ta->setLeftArrow(leftArrow);
    }
    if (upArrow && downArrow) {
        ta->setUpArrow(upArrow);
        ta->setDownArrow(downArrow);
    }
    tableView->getController()->setArrows(ta);

    ScrollPageIndicator* spi = dynamic_cast<ScrollPageIndicator*>(container->getNode("page_indicator"));
    if (spi)
        tableView->setScrollIndicator(spi);
}
// _________________________________________________________________________________________
cocos2d::Node* ie::mapCell()
{
    Atlas* mapArea = SAtlasCache()->getByGroup(WHITE_PIXEL_NAA);
    mapArea->setAnchorPoint(cc::Point::ZERO);
    mapArea->setRotation(-45.0f);
    mapArea->setOpacity(150);
    mapArea->setColor(cc::Color3B(0, 255, 0));
    mapArea->setScale(1);

    float k = 0.7071067811f; //= 1.0f / powf(2.0f, 0.5f);

    cc::Node* proxy = cc::Node::create();

    proxy->setScaleX(ie::tileSize().width * k);
    proxy->setScaleY(ie::tileSize().height * k);
    proxy->setAnchorPoint(cc::Vec2(0.5f, 0.0f));
    proxy->addChild(mapArea);
    //    proxy->setPosition()pos.toLand());

    mapArea->setScaleX(0.9);
    mapArea->setScaleY(0.9);

    return proxy;
}
// _________________________________________________________________________________________
void ie::layoutForNodes(cc::Node* parent, float border)
{
    if (not parent)
        return;

    cc::Size sizeParent(border, 0);
    for (auto* node : parent->getChildren()) {
        if (!node->isVisible())
            continue;

        node->setPosition(cc::Vec2(sizeParent.width, border) + node->getAnchorPointInPoints());
        sizeParent.width += node->getContentSize().width * node->getScale() + border;
        sizeParent.height = std::max(sizeParent.height, node->getContentSize().height * node->getScale() + border * 2);
    }
    parent->setContentSize(sizeParent);
}
// _________________________________________________________________________________________
void ie::fitToCenter(cc::Node* parent, cc::Node* node, bool useScale /* = true */, bool onlyScaleReduce /* = true */)
{
    if (parent && node) {
        cc::Size sizeParent = parent->getContentSize();
        node->setPosition(sizeParent * 0.5f);
        if (useScale) {
            sizeParent *= parent->getScale();
            const auto& sizeNode = node->getContentSize();
            if (not sizeNode.equals(cc::Size::ZERO) && not sizeParent.equals(cc::Size::ZERO)) {
                float scale = std::min(sizeParent.width / sizeNode.width, sizeParent.height / sizeNode.height);
                node->setScale(onlyScaleReduce ? std::min(scale, 1.0f) : scale);
            }
        }
    }
}
// _________________________________________________________________________________________
#pragma mark animated change
bool ie::setAdditiveColorAnim(cc::Node* n, const cc::Color3B& c, float stepTime)
{
    if (n == nullptr)
        return false;

    /// если разница (diff) меньше этого значения то переход мгновенный
    const static int sPunctuality = 16;
    const static int sActionTag = 67454178578;

    n->stopAllActionsByTag(sActionTag);

    cc::Color3B current = n->getAdditiveColor();
    int diff = abs(int(c.r + c.g + c.b) - int(current.r + current.g + current.b)) / 3;
    if (diff < sPunctuality || stepTime == 0.0f) {
        n->setAdditiveColor(c);
        return false; // без анимации
    }

    float stepCount = float(diff) / 255.0f;
    float duration = stepTime * stepCount;
    cc::Action* act = cc::EaseIn::create(cc::TintAdditiveTo::create(duration, c), 0.3f);
    act->setTag(sActionTag);
    n->runAction(act);

    return true; // c анимацией
}
// _________________________________________________________________________________________
bool ie::setColorAnim(cc::Node* n, const cc::Color3B& c, float stepTime)
{
    if (n == nullptr)
        return false;

    /// если разница (diff) меньше этого значения то переход мгновенный
    const static int sPunctuality = 16;
    const static int sActionTag = 255467323;

    n->stopAllActionsByTag(sActionTag);

    cc::Color3B current = n->getColor();
    int diff = abs(int(c.r + c.g + c.b) - int(current.r + current.g + current.b)) / 3;
    if (diff < sPunctuality || stepTime == 0.0f) {
        n->setColor(c);
        return false; // без анимации
    }

    float stepCount = float(diff) / 255.0f;
    float duration = stepTime * stepCount;

    cc::Action* act = cc::EaseIn::create(cc::TintTo::create(duration, c), 0.3f);
    act->setTag(sActionTag);
    n->runAction(act);

    return true; // c анимацией
}
// _________________________________________________________________________________________
bool ie::setScaleAnim(cc::Node* n, float s, float stepTime)
{
    if (n == nullptr || n->getScaleX() != n->getScaleY())
        return false;

    /// если разница (diff) меньше этого значения то переход мгновенный
    const static float sPunctuality = 0.05;
    const static int sActionTag = 8746587432;

    n->stopAllActionsByTag(sActionTag);

    float current = n->getScale();
    float diff = abs(current - s);
    if (diff < sPunctuality || stepTime == 0.0f) {
        n->setScale(s);
        return false; // без анимации
    }

    float stepCount = diff;
    float duration = stepTime * stepCount;

    cc::Action* act = cc::EaseIn::create(cc::ScaleTo::create(duration, s), 0.3f);
    act->setTag(sActionTag);
    n->runAction(act);

    return true; // c анимацией
}

// _________________________________________________________________________________________
bool ie::setOpacityAnim(cc::Node* n, GLubyte o, float stepTime)
{
    if (n == nullptr)
        return false;

    /// если разница (diff) меньше этого значения то переход мгновенный
    const static int sPunctuality = 16;
    const static int sActionTag = 234556685;

    n->stopAllActionsByTag(sActionTag);

    int current = n->getOpacity();
    int diff = abs(current - int(o));
    if (diff < sPunctuality || stepTime == 0.0f) {
        n->setOpacity(o);
        return false; // без анимации
    }

    float stepCount = float(diff) / 255.0f;
    float duration = stepTime * stepCount;

    cc::Action* act = cc::EaseIn::create(cc::FadeTo::create(duration, o), 0.9f);
    act->setTag(sActionTag);
    n->runAction(act);

    return true; // c анимацией
}
// _________________________________________________________________________________________
bool ie::setProgressAnim(cc::ProgressTimer* n, float to, float stepTime)
{
    if (n == nullptr)
        return false;

    to = cc::clampf(to, 0.0f, 100.0f);

    /// если разница (diff) меньше этого значения то переход мгновенный
    const static float sPunctuality = 5.0f;
    const static int sActionTag = 923634653;

    n->stopAllActionsByTag(sActionTag);

    float current = n->getPercentage();
    float diff = abs(current - to);
    if (diff < sPunctuality || stepTime == 0.0f) {
        n->setPercentage(to);
        return false; // без анимации
    }

    float stepCount = diff / 255.0f;
    float duration = stepTime * stepCount;

    cc::Action* act = cc::EaseIn::create(cc::ProgressTo::create(duration, to), 0.3);
    act->setTag(sActionTag);
    n->runAction(act);

    return true;
}
// _________________________________________________________________________________________
void ie::setEnableCascadeEffectsToSelfAndChildren(cc::Node* n, bool val)
{
    if (not n)
        return;

    n->setCascadeAddColorEnabled(val);
    n->setCascadeColorEnabled(val);
    n->setCascadeOpacityEnabled(val);

    for (cc::Node* child : n->getChildren()) {
        setEnableCascadeEffectsToSelfAndChildren(child, val);
    }
}
// _________________________________________________________________________________________
#pragma mark paddings
ie::Paddings::Paddings(float leftValue, float topValue, float rightValue, float bottomValue)
{
    left = leftValue;
    top = topValue;
    right = rightValue;
    bottom = bottomValue;
}
