/*
 * AlphaNode.h
 *
 *  Created on: September 12, 2016
 *      Author: asuvorov
 */

#pragma once

USING_NS_CC;

class DrawNodeSmooth : public DrawNode {
public:
    DrawNodeSmooth()
        : DrawNode()
    {
    }

    CREATE_FUNC(DrawNodeSmooth);
    virtual bool init() override;

    void setOpacity(GLubyte opac) override;

    void drawTriangle(const Vec2& p1, const Vec2& p2, const Vec2& p3, const Color4B& color1, const Color4B& color2,
        const Color4B& color3);

    void drawQuadBezierTriangles(const Vec2& origin, const Vec2& control, const Vec2& destination,
        unsigned int segments, const Color4F& color, float linew, float lineblurw);

    void drawLineTriangles(
        const Vec2& origin, const Vec2& destination, const Color4F& incol, float linew, float lineblurw);

private:
    GLubyte mOpacity;
};
