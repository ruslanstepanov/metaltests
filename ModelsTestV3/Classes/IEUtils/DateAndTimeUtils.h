//
//  DateAndTimeUtils.h
//  Exeriment
//
//  Created by d.antonov on 25.08.16.
//  Copyright © 2016 Nexters. All rights reserved.
//
#pragma once

#include "Global.h"

namespace ie {
/// время на сервере с учетом милисекунд
double time();
/// время на сервере округленное до секунд
time_t timeSec();
uint8_t centiSec();

/// время выставленное на часах у пользователя
/// для внутренних целей, НЕ для сервера!
double timeLocal();

int getYear();
int getMonth();
int getDay();
int getHour();
int getMinutes();
int getSeconds();

time_t localToGMT(time_t local);
time_t gmtToLocal(time_t sec);

} // namespace ie
