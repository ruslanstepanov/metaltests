//
//  AssetsUtils.cpp
//  Exeriment
//
//  Created by d.antonov on 26.08.16.
//  Copyright © 2016 Nexters. All rights reserved.
//
#include "AssetsUtils.h"
#include "StringUtils.h"
#include <sys/stat.h>
#include <iostream>
//#include "Platform.hpp"
//#include "WebAssetsConfig.hpp"
#include "DeviceUtils.h"
#include "Utils.h"

// _________________________________________________________________________________________
struct CachePaths {
    StringVec primary;
    StringVec secondary;

    static CachePaths* getInstance()
    {
        static CachePaths instance;
        return &instance;
    }

    CachePaths()
    {
        StringVec prefixes;

#if P_MAC
        bool isStandalone = ie::isStandaloneBuild();
        if (isStandalone) {
            prefixes.push_back(sMacCommonAssetsPath);

            // На маке в сборке Standalone игнорируются веб-ассеты, поэтому нужно
            // добавить путь к локальной папке web в пути поиска
            std::string webPath = sMacCommonAssetsPath + PFX_ASSETS + WebAssetsConfig::getWebAssetsRootPath();
            primary.push_back(webPath);
            secondary.push_back(webPath);
        }
#endif
        prefixes.push_back(cocos2d::FileUtils::getInstance()->getWritablePath());
        prefixes.push_back(SPlatform()->getAppRootFolder());

        for (auto& prefix : prefixes) {
            primary.push_back(prefix + PFX_ASSETS_PRIMARY);

            secondary.push_back(prefix + PFX_ASSETS_PRIMARY);
            secondary.push_back(prefix + PFX_ASSETS_SECONDARY);
        }
    }

    const StringVec& getCurrent() const
    {
        bool isSecondaryEnabled = cocos2d::FileUtils::getInstance()->isSecondaryAssetsEnabled();

        return (isSecondaryEnabled ? secondary : primary);
    }
};
// _________________________________________________________________________________________
bool ie::isBundleFileExist(const std::string& name)
{
    auto* fu = cocos2d::FileUtils::getInstance();
    std::string strTemp = fu->fullPathForFilename(name.c_str());
    bool isExist = fu->isFileExist(strTemp);
    if (!isExist && stringEndsWith(strTemp, ".png")) {
        std::string webpPath(name);
        size_t pos = webpPath.find_last_of(".");
        webpPath.replace(pos, webpPath.length(), ".webp");
        webpPath = fu->fullPathForFilename(webpPath.c_str());
        isExist = fu->isFileExist(webpPath);
    }
    return isExist;
}
// _________________________________________________________________________________________
std::string ie::getAssetsAbsoluteFilePath(const std::string& fileName)
{
    auto& cachePaths = CachePaths::getInstance()->getCurrent();

    auto fileUtils = cocos2d::FileUtils::getInstance();

    for (auto& path : cachePaths) {
        std::string fullPath = path + fileName;
        if (fileUtils->isFileExist(fullPath))
            return fullPath;
    }

    if (ie::stringEndsWith(fileName, ".png")) {
        std::string webpPath(fileName);
        size_t pos = webpPath.find_last_of(".");
        webpPath.replace(pos, webpPath.length(), ".webp");
        for (const auto& path : cachePaths) {
            std::string fullPath = path + webpPath;
            if (fileUtils->isFileExist(fullPath))
                return fullPath;
        }
    }

    return std::string();
}
// _________________________________________________________________________________________
bool ie::isAssetsFileExist(const std::string& fileName)
{
    return !getAssetsAbsoluteFilePath(fileName).empty();
}
// _________________________________________________________________________________________
bool ie::isUserPathFileExist(const std::string& name)
{
    struct stat buffer;
    return (stat(name.c_str(), &buffer) == 0);
}
// _________________________________________________________________________________________
bool ie::isUserFileExist(const std::string& name)
{
    struct stat buffer;
    return (stat((cocos2d::FileUtils::getInstance()->getWritablePath() + name).c_str(), &buffer) == 0);
}
// _________________________________________________________________________________________
bool ie::renameSettingsFiles(const std::string& inputFileName, const std::string& outputFileName)
{
    auto* fu = cocos2d::FileUtils::getInstance();
    const std::string& fullPathSrc = fu->getWritablePath() + inputFileName;
    const std::string& fullPathDst = fu->getWritablePath() + outputFileName;

    FILE* source = fopen(fullPathSrc.c_str(), "rb");
    FILE* dest = fopen(fullPathDst.c_str(), "wb");

    if (!source || !dest) {
        if (source)
            fclose(source);
        if (dest)
            fclose(dest);

        ie::log("Can't open user files! source:%s dest: %s", fullPathSrc.c_str(), fullPathDst.c_str());
        return false;
    }

    char buf[BUFSIZ] = { 0 };
    size_t size;

    do {
        size = fread(buf, 1, BUFSIZ, source);

        if (size > 0)
            fwrite(buf, 1, size, dest);
    } while (size > 0);

    fclose(source);
    fclose(dest);

    return true;
}
// _________________________________________________________________________________________
