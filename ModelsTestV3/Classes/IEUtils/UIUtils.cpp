//
//  UIUtils.cpp
//  Exeriment
//
//  Created by d.antonov on 25.08.16.
//  Copyright © 2016 Nexters. All rights reserved.
//
#include "UIUtils.h"
#include "ThreadUtils.h"

#include "Global.h"

static const int sStringBufSize = 1024;

// _________________________________________________________________________________________
void ie::showUIMessage(const std::string& message, const std::string& title)
{
    ie::log("ie::showUIMessage");
    ie::log("%s", title.c_str());
    ie::log("%s", message.c_str());

    if (isMainThread()) {
        auto* ccd = cocos2d::Director::getInstance();
        static std::string sPrevMessage;
        static auto sFrameId = ccd->getTotalFrames();
        auto frameId = ccd->getTotalFrames();

        if (message == sPrevMessage && frameId == sFrameId) {
            ie::log("ignore duplicate message");
        }
        else {
            cocos2d::MessageBox(message.c_str(), title.c_str());
            sPrevMessage = message;
            sFrameId = frameId;
        }
    }
    //    else
    //        Breakpoint();
};
// _________________________________________________________________________________________
void ie::showErrorMessage(const char* pszFormat, ...)
{
    char* szBuf;

    va_list ap;
    va_start(ap, pszFormat);

    szBuf = (char*)malloc(sStringBufSize * sizeof(char));

    vsnprintf(szBuf, sStringBufSize, pszFormat, ap);
    va_end(ap);

    showUIMessage(szBuf, "ERROR");

    free(szBuf);
}
// _________________________________________________________________________________________
