//
//  UtilsLocaleReqTemplatePre.hpp
//  Experiment
//
//  Created by d.dolgikh on 23.06.17.
//

#pragma once

namespace ie {
enum class LocaleReqTemplate { Default, ExpeditionIslandInfoWindow };
} // namespace ie
