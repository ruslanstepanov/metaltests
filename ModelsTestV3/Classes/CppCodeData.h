//
//  CppCodeData.h
//  LostChapters
//
//  Created by a.arsentiev on 20.01.14.
//  Copyright (c) 2014 Crazy Bit. All rights reserved.
//

#pragma once

#pragma clang diagnostic push // не показываем варнинг
#pragma clang diagnostic ignored "-Wpredefined-identifier-outside-function"
struct FunctionName {
    static constexpr const char* sName = __PRETTY_FUNCTION__;
};

struct CodeData {
    const char* mFunction;
    const char* mFile;
    const unsigned int mLine;

    CodeData(const char* fun, const char* file, unsigned int l)
        : mFunction(fun)
        , mFile(file)
        , mLine(l)
    {
    }
};

#define GetCodeData() CodeData(__PRETTY_FUNCTION__, __FILE__, __LINE__)

#pragma clang diagnostic pop
