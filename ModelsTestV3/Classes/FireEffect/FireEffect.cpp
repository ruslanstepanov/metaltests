#include "FireEffect.h"
//#include "GameTimers.h"
//#include "Game.h"
#include <iostream>
//#include "CustomShaderCache/CustomShaderCache.h"
//#include "AssetsUtils.h"

// Можно использовать png-шки
//static const char* sMainTxName = "f_prm_cel.png";
//static const char* sMainThinTxName = "f_prm_cel_thin.png";
//static const char* sWindTxName = "f_disp_cel.png";
//static const char* sGradTxName = "f_grad_cel.png";
//static const char* sGradBlueTxName = "f_grad_cel_blue.png";

// Но лучше pvr.ccz
static const char* sMainTxName = "f_prm_cel.pvr.ccz";
static const char* sMainThinTxName = "f_prm_cel_thin.pvr.ccz";
static const char* sWindTxName = "f_disp_cel.pvr.ccz";
static const char* sGradTxName = "f_grad_cel.pvr.ccz";
static const char* sGradBlueTxName = "f_grad_cel_blue.pvr.ccz";

static const char* sVertFileName = "FireEffectShader.vsh";
static const char* sFragFileName = "FireEffectShader.fsh";

static const float sMaxSaturation = 0.95f;

// _______________________________________________________________________
FireEffect* FireEffect::create(const cocos2d::Size& size, unsigned fireType, float deltaTime, float fireSaturation,
    float speed, float displacementScale)
{
    FireEffect* fe = new FireEffect(deltaTime);
    fe->setAnchorPoint(cocos2d::Vec2(0.0f, 0.0f));
    fe->autorelease();

    const char* mainTxName = sMainTxName;
    const char* gradTxName = sGradTxName;
    if (fireType & THIN)
        mainTxName = sMainThinTxName;
    if (fireType & BLUE)
        gradTxName = sGradBlueTxName;

    fe->init(size, mainTxName, sWindTxName, gradTxName);
    fe->setFireSaturation(fireSaturation);
    fe->setSpeed(speed);
    fe->setDisplacementScale(displacementScale);

    return fe;
}
// _______________________________________________________________________
FireEffect::~FireEffect()
{
    //    STimers()->onTick.disconnect(this, &FireEffect::onTimer);
    //    STimers()->onPSec25.disconnect(this, &FireEffect::onAnimate);

    clear();
}
// _______________________________________________________________________
void FireEffect::setFireSaturation(float val)
{
    if (val < 0.0f)
        val = 0.0f;
    if (val > sMaxSaturation)
        val = sMaxSaturation;

    mFireSaturation = val;
}
// _______________________________________________________________________
void FireEffect::animateIncrease()
{
    mSaturationSpeed = 0.01f;
    //    STimers()->onPSec25.connect(this, &FireEffect::onAnimate);
}
// _______________________________________________________________________
void FireEffect::animateDecrease()
{
    mSaturationSpeed = -0.01f;
    //    STimers()->onPSec25.connect(this, &FireEffect::onAnimate);
}
// _______________________________________________________________________
FireEffect::FireEffect(float deltaTime)
    : mDeltaTime(deltaTime)
    , mFireSaturation(sMaxSaturation)
{
}
// _______________________________________________________________________
void FireEffect::init(
    const cocos2d::Size& size, const std::string& mainTxName, const std::string& windTxName, const std::string& gradTxName)
{
    mMainTextureName = mainTxName;
    mWindTextureName = windTxName;
    mGradTextureName = gradTxName;

    if (!prepare()) {
        cocos2d::log("Can't prepate fire effect");
        return;
    }

    ///! CHECK_GL_ERROR_DEBUG();

    if (!Sprite::initWithTexture(mMainTexture)) {
        cocos2d::log("Can't init texture %s", mainTxName.c_str());
        //        Breakpoint();
        return;
    }

    cocos2d::Size texSize = mMainTexture->getContentSize();
    const float scaleX = size.width / texSize.width;
    const float scaleY = size.height / texSize.height;
    setScaleX(scaleX);
    setScaleY(scaleY);

    //    STimers()->onTick.connect(this, &FireEffect::onTimer);
}
// _______________________________________________________________________
bool FireEffect::prepare()
{
    cocos2d::Texture2D::TexParams params = { GL_LINEAR, GL_LINEAR, GL_REPEAT, GL_REPEAT };

    std::string vsfullName = cocos2d::FileUtils::getInstance()->fullPathForFilename(sVertFileName);
    std::string fsfullName = cocos2d::FileUtils::getInstance()->fullPathForFilename(sFragFileName);

    //	if (ie::isBundleFileExist(vsfullName) == false) {
    //        ie::log("can't find file %s", vsfullName.c_str());
    //        return false;
    //    }
    //	if (ie::isBundleFileExist(fsfullName) == false) {
    //        ie::log("can't find file %s", fsfullName.c_str());
    //        return false;
    //    }

    // Подготовка данных
    mShader = loadShader();
    mShader->retain();

    mMainTexture = cocos2d::Director::getInstance()->getTextureCache()->addImage(mMainTextureName.c_str());
    mMainTexture->setTexParameters(params);
    mMainTexture->retain();

    mWindTexture = cocos2d::Director::getInstance()->getTextureCache()->addImage(mWindTextureName.c_str());
    mWindTexture->setTexParameters(params);
    mWindTexture->retain();

    mGradTexture = cocos2d::Director::getInstance()->getTextureCache()->addImage(mGradTextureName.c_str());
    mGradTexture->setTexParameters(params);
    mGradTexture->retain();

    return true;
}
// _________________________________________________________________________________________
/// Возвращает custom shader из кэша или создает его
static void initFire(cocos2d::GLProgram* p)
{
    p->bindAttribLocation(cocos2d::GLProgram::ATTRIBUTE_NAME_POSITION, cocos2d::GLProgram::VERTEX_ATTRIB_POSITION);
    p->bindAttribLocation(cocos2d::GLProgram::ATTRIBUTE_NAME_COLOR, cocos2d::GLProgram::VERTEX_ATTRIB_COLOR);
    p->bindAttribLocation(cocos2d::GLProgram::ATTRIBUTE_NAME_TEX_COORD, cocos2d::GLProgram::VERTEX_ATTRIB_TEX_COORD);
}
const static std::string sRoot = "";//"shaders/";
cocos2d::GLProgram* getProgram(const std::string& name)
{
    auto* program = cocos2d::GLProgramCache::getInstance()->getGLProgram(name);
    if (not program) {
        program = new cocos2d::GLProgram();
        program->initWithFilenames(sRoot + name + ".vsh", sRoot + name + ".fsh");
        initFire(program);
        program->link();
        program->updateUniforms();
        cocos2d::GLProgramCache::getInstance()->addGLProgram(program, name.c_str());
        program->release();
    }
    return program;
}
// _________________________________________________________________________________________
cocos2d::GLProgram* FireEffect::loadShader()
{
    cocos2d::GLProgram* p = getProgram("FireEffectShader");

    GLint mainTextureUniform = glGetUniformLocation(p->getProgram(), "uMainTexture");
    GLint gradTextureUniform = glGetUniformLocation(p->getProgram(), "uGradTexture");
    GLint windTextureUniform = glGetUniformLocation(p->getProgram(), "uWindTexture");

    p->setUniformLocationWith1i(mainTextureUniform, 0);
    p->setUniformLocationWith1i(gradTextureUniform, 1);
    p->setUniformLocationWith1i(windTextureUniform, 2);

    mUniformTime = glGetUniformLocation(p->getProgram(), "uTime");
    mUniformSaturation = glGetUniformLocation(p->getProgram(), "uSaturation");
    mUnifoirmDisplacement = glGetUniformLocation(p->getProgram(), "uWindDisplacement");

    p->setUniformLocationWith1f(mUniformTime, mDeltaTime);
    p->setUniformLocationWith1f(mUniformSaturation, mFireSaturation);
    p->setUniformLocationWith1f(mUnifoirmDisplacement, mDisplacementScale);

    return p;
}
// _______________________________________________________________________
void FireEffect::clear()
{
    CC_SAFE_RELEASE_NULL(mShader);
    CC_SAFE_RELEASE_NULL(mMainTexture);
    CC_SAFE_RELEASE_NULL(mWindTexture);
    CC_SAFE_RELEASE_NULL(mGradTexture);
}
// _______________________________________________________________________
void FireEffect::onTimer()
{
    mDeltaTime += mSpeed * 0.1f;
    if (mDeltaTime > 10.0f)
        mDeltaTime -= 10.0f;
}
// _______________________________________________________________________
void FireEffect::onAnimate()
{
    setFireSaturation(mFireSaturation + mSaturationSpeed);
    
    //    if (mFireSaturation <= 0.0f || mFireSaturation >= sMaxSaturation)
    //         STimers()->onPSec25.disconnect(this, &FireEffect::onAnimate);
}
// _______________________________________________________________________
void FireEffect::draw(cocos2d::Renderer* renderer, const cocos2d::Mat4& transform, uint32_t flags)
{
    onTimer();
    
    mCustomCommand.init(_globalZOrder, transform, flags);
    mCustomCommand.func = CC_CALLBACK_0(FireEffect::doDraw, this, transform, flags);
    renderer->addCommand(&mCustomCommand);
}
// _______________________________________________________________________
void FireEffect::doDraw(const cocos2d::Mat4& transform, uint32_t flags)
{
    if (!mMainTexture || !mWindTexture || !mGradTexture) {
        return;
    }

    bool tex0Valid = glIsTexture(mMainTexture->getName());
    bool tex1Valid = glIsTexture(mWindTexture->getName());
    bool tex2Valid = glIsTexture(mGradTexture->getName());
    if (!tex0Valid || !tex1Valid || !tex2Valid) {
        return;
    }

    cocos2d::GL::bindTexture2DN(0, mMainTexture->getName());
    cocos2d::GL::bindTexture2DN(1, mGradTexture->getName());
    cocos2d::GL::bindTexture2DN(2, mWindTexture->getName());

    cocos2d::GLProgram* p = mShader;
    p->use();
    p->setUniformsForBuiltins(transform);

    p->setUniformLocationWith1f(mUniformTime, mDeltaTime);
    p->setUniformLocationWith1f(mUniformSaturation, mFireSaturation);
    p->setUniformLocationWith1f(mUnifoirmDisplacement, mDisplacementScale);

    CHECK_GL_ERROR_DEBUG();

    CC_PROFILER_START_CATEGORY(kCCProfilerCategorySprite, "cocos2d::Sprite - draw");

    cocos2d::GL::blendFunc(_blendFunc.src, _blendFunc.dst);
    CHECK_GL_ERROR_DEBUG();
    cocos2d::GL::enableVertexAttribs(cocos2d::GL::VERTEX_ATTRIB_FLAG_POS_COLOR_TEX);
    CHECK_GL_ERROR_DEBUG();

#define kQuadSize sizeof(_quad.bl)
#ifdef EMSCRIPTEN
    long offset = 0;
    setGLBufferData(&_quad, 4 * kQuadSize, 0);
#else
    long offset = (long)&_quad;
#endif // EMSCRIPTEN

    // vertex
    int diff = offsetof(cocos2d::V3F_C4B_T2F, vertices);
    glVertexAttribPointer(
        cocos2d::GLProgram::VERTEX_ATTRIB_POSITION, 3, GL_FLOAT, GL_FALSE, kQuadSize, (void*)(offset + diff));

    // texCoods
    diff = offsetof(cocos2d::V3F_C4B_T2F, texCoords);
    glVertexAttribPointer(
        cocos2d::GLProgram::VERTEX_ATTRIB_TEX_COORD, 2, GL_FLOAT, GL_FALSE, kQuadSize, (void*)(offset + diff));

    // color
    diff = offsetof(cocos2d::V3F_C4B_T2F, colors);
    glVertexAttribPointer(
        cocos2d::GLProgram::VERTEX_ATTRIB_COLOR, 4, GL_UNSIGNED_BYTE, GL_TRUE, kQuadSize, (void*)(offset + diff));

    CHECK_GL_ERROR_DEBUG();
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    CHECK_GL_ERROR_DEBUG();
}
// _________________________________________________________________________________________
