//
//  GameTimer.h
//  MysteryTown
//
//  Created by a.arsentiev on 01.07.13.
//  Copyright (c) 2013 Crazy Bit. All rights reserved.
//

#pragma once

#include "Signal/TTSignal.h"
#include "GameInstance.h"
#include "GameTimeController.hpp"
#include <memory>

/// игровые таймеры - множество сигналов которые вызываются раз в заданный период
class GameTimers : public GameTimeController, public GameInstance<GameTimers> {
    /// инстанцировав здесь
    friend class AppInstances;

public:
    GameTimers();
    
    class Timer : protected SignalV {
        friend class GameTimers;

    protected:
        double mNextCall = 0.0;
        double mDt = 0.0;
        /// указатель на указатель на строку с последним местом вызова
        const char** mLastSingletonCall = nullptr;

    public:
        Timer(double dt, const char** fromPtr);

        template <class T_Class> void connect(cocos2d::CCInvocable* t, void (T_Class::*s)());
        template <class T_Class> void disconnect(cocos2d::CCInvocable* t, void (T_Class::*s)());
        template <class T_Class> bool isConnected(cocos2d::CCInvocable* t, void (T_Class::*s)());
    };

    static GameTimers* instanceMarkFunction(const char* callFromFunction);

    virtual ~GameTimers();

    void step();

public:
    virtual double getLocalTime()
    {
        return mLocalTime;
    };
    virtual void setLocalTime(double localTime)
    {
        mLocalTime = localTime;
    };

    Timer onPSec25{ 0.04, &mLastSingletonCall }; ///< 25 times per secund
    Timer onPSec12_5{ 0.08, &mLastSingletonCall }; ///< 12.5 times per secund
    Timer onSec{ 1.0, &mLastSingletonCall }; ///< every secunds
    Timer on5Sec{ 5.0, &mLastSingletonCall }; ///< every secunds
    Timer onMin{ 60.0, &mLastSingletonCall }; ///< every minutes
    Timer on5Min{ 300.0, &mLastSingletonCall }; ///< every 5 minutes

    /// вызвается каждый раз при обнолении таймера, периодичность не гарантируется
    SignalV onTick;
    /// через сколько сек вызовется таймер
    double timeTillCall(const Timer&) const;

protected:
    /// имя ф-и вызвавшей синглтон последний раз
    const char* mLastSingletonCall = nullptr;

    /// проверяем что все отписались, если не все даем ошибку
    void onGameInstanceCleared();

protected:
    Timer* mTimers[6] = { &onPSec25, &onPSec12_5, &onSec, &on5Sec, &onMin, &on5Min };
    double mLocalTime = 0;

    float mStepCall = 0.0f;
    float mDelayCall = std::numeric_limits<float>::max();
};

#define STimers() (GameTimers::instanceMarkFunction(__PRETTY_FUNCTION__))
// ____________________________
template <class T_Class> void GameTimers::Timer::connect(cocos2d::CCInvocable* t, void (T_Class::*s)())
{
    SignalV::connect(t, s);
}
// ____________________________
template <class T_Class> void GameTimers::Timer::disconnect(cocos2d::CCInvocable* t, void (T_Class::*s)())
{
    SignalV::disconnect(t, s);
}
// ____________________________
template <class T_Class> bool GameTimers::Timer::isConnected(cocos2d::CCInvocable* t, void (T_Class::*s)())
{
    return SignalV::isConnected(t, s);
}
