//
//  GameTimeController.hpp
//  LostChapters
//
//  Created by a.arsentiev on 27.09.15.
//  Copyright © 2015 Crazy Bit. All rights reserved.
//

#pragma once

#include <stdint.h>
#include "Signal/SignalImpl.h"

/// класс GameTimeController посигнатурно иденичный канвасному GameTimer.as
class GameTimeController : public cocos2d::Ref {
public:
    static const constexpr time_t ONE_DAY = 60 * 60 * 24;
    static const constexpr time_t THREE_DAYS = 3 * ONE_DAY;
    static const constexpr time_t THREE_HOURS = 60 * 60 * 3;

    /// инициализируем при перезапуске сесси,
    /// accountLastLogin - нужен чтобы сбить полночь
    /// accountMidnightOffset - игнор, берем его из GameSession
    void initServerTime(time_t accountLastLogin); //, time_t accountMidnightOffset);

    /// время на сервере округленное до секунд
    time_t currentServerTime();

    time_t getNextServerMidnight();
    /// произойдет/произошло ли указанное время сегодня
    bool isToday(time_t time);
    /// 'or later' означает, что time может приходиться на дни после nowTime. но не наоборот.
    /// checkTime - веремя которое мы проверяем
    /// nowTime - текущее вермя
    bool isSameDayOrLater(time_t checkTime, time_t nowTime);
    /// предыдущая полночь
    time_t getPreviousRelativeMidnight(time_t nowTime);
    /// следующая полноч
    time_t getNextRelativeMidnight(time_t nowTime);
    /// посылается в момент достижения полночи по серверному времени с учетом тайм зоны игрока
    SignalV onMidnightTimer;

protected: // methods
    /// проверяем не настала ли полночь, если настала отсылаем игнал onMidnightTimer
    void checkMidnight();

protected: // vars
    /// время последней проверки на полночь или время логина
    /// так названа у флеша
    time_t mLastTime = 0;
};
