//
//  IAsyncCallBack.h
//  HelloWorld
//
//  Created by v.ishtuin on 25.04.13.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#pragma once

#include "Global.h"

// -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
class SimpleManaged : public cocos2d::CCInvocable {
public:
    void release(void);
    void retain(void);

    SimpleManaged();
    ~SimpleManaged() override;

protected:
    /// when the object is created, the reference count of it is 1
    unsigned int mReferenceCount = 1;
};
// -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
class IAsyncCallBack : public SimpleManaged {
protected:
    IAsyncCallBack(){};

public:
    virtual ~IAsyncCallBack(){};
    virtual void call() = 0;

protected:
    const char* mCallFrom = nullptr;

public:
    virtual const char* getCallFrom()
    {
        return mCallFrom;
    };
    virtual void setCallFrom(const char* value)
    {
        mCallFrom = value;
    };

    /// может ли быть выполнен сигнал (по дефолту всегда да)
    virtual bool canEmit();
    /// интерфейс для сравнения колбеков
    virtual bool isCallbackConnected(cocos2d::CCInvocable*, cocos2d::SEL_CallFunc) = 0;
    /// интерфейс для сравнения колбеков
    virtual bool isCallbackConnected(cocos2d::CCInvocable*) = 0;
    /// отписать все от колбека
    virtual void clearCallback() = 0;
    /// подписано ли что нибуть на колбек
    virtual bool emptyCallback() = 0;
};
// -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
