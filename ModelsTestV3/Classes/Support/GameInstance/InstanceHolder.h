//
//  InstaneHolder.h
//  LostChapters
//
//  Created by a.arsentiev on 08.09.14.
//  Copyright (c) 2014 Crazy Bit. All rights reserved.
//

#pragma once

#include "Global.h"

/// базовый класс для сущностей контролирующих время жизни инстансов
class InstanceHolder {
public:
    InstanceHolder();
    virtual ~InstanceHolder();

    /// зарегистрировать новый инстанс и уменьшить его счетчик ссылок
    void registerNew(cocos2d::Ref* obj);
    /// удалить все зарегистрированные инстансы
    void clearInstancess();
    /// освободить инстанс
    void releaseInstance(cocos2d::Ref* obj);

    /// создать и завладеть новым инстансом
    template <class T_InstanceType> void registerNew();

private:
    cocos2d::Vector<cocos2d::Ref*> mInstances;
};

template <class T_InstanceType> inline void InstanceHolder::registerNew()
{
    T_InstanceType* inst = new T_InstanceType();
    cocos2d::Ref* obj = static_cast<cocos2d::Ref*>(inst);

    mInstances.pushBack(obj);
    obj->release();
}
