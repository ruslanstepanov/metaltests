//
//  GameInstance.h
//  LostChapters
//
//  Created by a.arsentiev on 06.03.14.
//  Copyright (c) 2014 Crazy Bit. All rights reserved.
//

#pragma once

//#include "GameAssert.h"

/// базовый класс обеспеExчивающий безопасное инстанцирование объектов
/// при однавременном присутствии в игре двух объектов дочернего класа выдается LogicError
template <class T_Child> class GameInstance {
protected:
    static T_Child* sInstance;
    // _________________________________________________________________________________________
    GameInstance()
    {
        if (sInstance) {
//            LogicErrorF("already inicialised");
        }
        else {
            sInstance = static_cast<T_Child*>(this);
        }
    }
    // _________________________________________________________________________________________
    virtual ~GameInstance()
    {
        if (sInstance == static_cast<T_Child*>(this)) {
            sInstance = nullptr;
        }
        else {
//            LogicError("copy destuct");
        }
    }
    // _________________________________________________________________________________________
public:
    static inline T_Child* Instance()
    {
        return sInstance;
    }
};

template <class T_Child> T_Child* GameInstance<T_Child>::sInstance = nullptr;

/// базовый класс обеспеExчивающий безопасное инстанцирование объектов
/// при однавременном присутствии в игре двух объектов дочернего класа выдается LogicError
template <class T_Child> class ConstInstance {
protected:
    static T_Child* sInstance;
    // _________________________________________________________________________________________
    ConstInstance()
    {
        if (sInstance) {
//            LogicErrorF("already inicialised");
        }
        else {
            sInstance = static_cast<T_Child*>(this);
        }
    }
    // _________________________________________________________________________________________
    ~ConstInstance()
    {
        if (sInstance == static_cast<T_Child*>(this)) {
            sInstance = nullptr;
        }
        else {
//            LogicError("copy destuct");
        }
    }
    // _________________________________________________________________________________________
public:
    static inline const T_Child* Instance()
    {
        return sInstance;
    }
};

template <class T_Child> T_Child* ConstInstance<T_Child>::sInstance = nullptr;
