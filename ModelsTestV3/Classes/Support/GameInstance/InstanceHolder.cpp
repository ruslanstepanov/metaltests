//
//  InstaneHolder.cpp
//  LostChapters
//
//  Created by a.arsentiev on 08.09.14.
//  Copyright (c) 2014 Crazy Bit. All rights reserved.
//

#include "InstanceHolder.h"

// _________________________________________________________________________________________
InstanceHolder::InstanceHolder()
{
}
// _________________________________________________________________________________________
InstanceHolder::~InstanceHolder()
{
    clearInstancess();
}
// _________________________________________________________________________________________
void InstanceHolder::clearInstancess()
{
    while (mInstances.size()) {
        mInstances.popBack();
    }
}
// _________________________________________________________________________________________
void InstanceHolder::registerNew(cocos2d::Ref* obj)
{
    mInstances.pushBack(obj);
    obj->release();
}
// _________________________________________________________________________________________
void InstanceHolder::releaseInstance(cocos2d::Ref* obj)
{
    mInstances.eraseObject(obj);
}
