//
//  GameMultyInstnce.h
//  PuzzleIsland
//
//  Created by Aleksandr on 20/08/2019.
//

#pragma once

#include "GameAssert.h"
#include <unordered_set>

/// базовый класс обеспеExчивающий безопасное инстанцирование объектов
/// при однавременном присутствии в игре двух объектов дочернего класа выдается LogicError
template <class T_Child> class GameMultyInstnce {
protected:
    static std::unordered_set<T_Child*>* sAllObjects;
    // _________________________________________________________________________________________
    GameMultyInstnce()
    {
        if (sAllObjects == nullptr) {
            sAllObjects = new std::unordered_set<T_Child*>();
        }
        sAllObjects->insert(static_cast<T_Child*>(this));
    }
    // _________________________________________________________________________________________
    virtual ~GameMultyInstnce()
    {
        sAllObjects->erase(static_cast<T_Child*>(this));
    }
    // _________________________________________________________________________________________
public:
    static inline T_Child* AnyInstance()
    {
        if (sAllObjects == nullptr || sAllObjects->empty())
            return nullptr;
        return *sAllObjects->begin();
    }
};

template <class T_Child> std::unordered_set<T_Child*>* GameMultyInstnce<T_Child>::sAllObjects = nullptr;
