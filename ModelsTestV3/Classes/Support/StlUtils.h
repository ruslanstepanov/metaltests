//
//  StlUtils.h
//  MysteryTown
//
//  Created by a.arsentiev on 04.07.13.
//  Copyright (c) 2013 Crazy Bit. All rights reserved.
//

#pragma once
#include <valarray>
#include <deque>

namespace stlh {

// _________________________________________________________________________________________
template <typename T> bool contains(const std::valarray<T>& container, const T& object)
{
    size_t size = container.size();
    for (size_t i = 0; i < size; ++i) {
        if (container[i] == object)
            return true;
    }

    return false;
}
// _________________________________________________________________________________________
template <typename T> bool contains(const std::vector<T>& container, const T& object)
{
    for (const T& val : container) {
        if (val == object)
            return true;
    }
    return false;
}
// _________________________________________________________________________________________
template <typename T> bool contains(const std::deque<T>& container, const T& object)
{
    for (const T& val : container) {
        if (val == object)
            return true;
    }
    return false;
}
// _________________________________________________________________________________________
template <typename CONT, typename T> bool contains(const CONT& container, const T& object)
{
    bool exist = container.find(object) != container.end();
    return exist;
}
// _________________________________________________________________________________________
/// найти элемент массива (вектора, списка) по значению его поля
/// если элемент не найден вернет nullptr
/// @line константная версия
template <typename CONT, typename T, typename FIELD>
auto member(const CONT& container, const T& object, FIELD shift) -> decltype(&(*container.begin()))
{
    for (auto it = container.begin(); it != container.end(); ++it) {
        const auto& element = *it;
        const T& field = element.*shift;

        if (field == object)
            return &element;
    }

    return NULL;
}
// _________________________________________________________________________________________
/// найти элемент массива (вектора, списка) по значению его поля
/// если элемент не найден вернет nullptr
/// @line не константная версия
template <typename CONT, typename T, typename FIELD>
auto member(CONT& container, const T& object, FIELD shift) -> decltype(&(*container.begin()))
{
    for (auto it = container.begin(); it != container.end(); ++it) {
        auto& element = *it;
        const T& field = element.*shift;

        if (field == object)
            return &element;
    }

    return NULL;
}
// _________________________________________________________________________________________
/// найти элемент массива (вектора, списка) по значению его геттера
/// если элемент не найден вернет nullptr
/// @line не константная версия
template <typename CONT, typename T_Searsh, typename T_SearshRet, typename TRet>
const TRet* memberGet(const CONT& container, const T_Searsh& object, T_SearshRet(TRet::*getter))
{
    for (auto it = container.begin(); it != container.end(); ++it) {
        auto& element = *it;
        const T_Searsh& field = (element.*getter)();

        if (field == object)
            return &element;
    }

    return NULL;
}
// _________________________________________________________________________________________
/// найти элемент массива (вектора, списка) по значению его геттера
/// если элемент не найден вернет nullptr
/// @line не константная версия
template <typename CONT, typename T_Searsh, typename T_SearshRet, typename TRet>
TRet* memberGet(CONT& container, const T_Searsh& object, T_SearshRet(TRet::*getter))
{
    for (auto it = container.begin(); it != container.end(); ++it) {
        auto& element = *it;
        const T_Searsh& field = (element.*getter)();

        if (field == object)
            return &element;
    }

    return NULL;
}
// _________________________________________________________________________________________
/// найти элемент std::map по значению его ключа
/// если элемент не найден вернет nullptr
template <typename T, typename T_Key>
auto member(std::map<T_Key, T>& container, const T_Key& key) -> typename std::map<T_Key, T>::mapped_type*
{
    const auto& iter = container.find(key);

    if (iter == container.end())
        return nullptr;
    else
        return &iter->second;
}
// _________________________________________________________________________________________
/// найти элемент const std::map по значению его ключа
/// если элемент не найден вернет nullptr
template <typename T, typename T_Key>
auto member(const std::map<T_Key, T>& container, const T_Key& key) -> decltype(&(container.begin()->second))
{
    const auto& iter = container.find(key);

    if (iter == container.end())
        return nullptr;
    else
        return &iter->second;
}

// _________________________________________________________________________________________
/// сделать выборку элементов const std::map по значению их поля
/// НЕ ПОТОКО БЕЗОПАСТНА
/// найденные эелементы поместятся в статический буфер
template <typename T_Key, typename T_Entry, typename T, typename FIELD>
const std::map<T_Key, T_Entry>& select(const std::map<T_Key, T_Entry>& container, const T& object, FIELD shift)
{
    static std::map<T_Key, T_Entry> buff;
    buff.clear();

    for (auto& pair : container) {
        auto& element = pair.second;
        const T& field = element.*shift;

        if (field == object)
            buff.insert(pair);
    }

    return buff;
}
// _________________________________________________________________________________________
/// сделать выборку элементов (вектора, списка) по значению их поля
/// НЕ ПОТОКО БЕЗОПАСТНА
/// найденные эелементы поместятся в статический буфер
template <typename CONT, typename T, typename FIELD>
auto select(const CONT& container, const T& object, FIELD shift) -> const std::vector<decltype(&(*container.begin()))>&
{
    static std::vector<decltype(&(*container.begin()))> buff;
    buff.clear();

    for (auto it = container.begin(); it != container.end(); ++it) {
        const auto& element = *it;
        const T& field = element.*shift;

        if (field == object)
            buff.push_back(&element);
    }

    return buff;
}
// _________________________________________________________________________________________
/// сделать выборку элементов массива по значению их поля
/// НЕ ПОТОКО БЕЗОПАСТНА
/// найденные эелементы поместятся в статический буфер
template <typename T_STRUCT, typename T, typename FIELD, std::size_t T_Size>
const std::vector<const T_STRUCT*>& select(T_STRUCT (&container)[T_Size], const T& object, FIELD shift)
{
    static std::vector<const T_STRUCT*> buff;
    buff.clear();

    for (int i = 0; i < T_Size; i++) {
        const auto& element = container[i];
        const T& field = element.*shift;

        if (field == object)
            buff.push_back(&element);
    }

    return buff;
}

/// выровнить а по интервалу [min, a, max]
template <class T> const T& minmax(const T& min, const T& a, const T& max)
{
    return std::min(std::max(min, a), max);
}
} // namespace stlh
