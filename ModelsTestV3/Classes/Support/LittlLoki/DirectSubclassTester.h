//
//  DirectSubclassTester.h
//  HelloWorld
//
//  Created by Aleksandr Arsentiev on 08.03.13.
//
//

#ifndef HelloWorld_DirectSubclassTester_h
#define HelloWorld_DirectSubclassTester_h

#include "TypeManip.h"

typedef int64_t ptr64b;

template <class T_Class, class T_Parent> class DirectSubclass {
    enum { mAdrTClass = (ptr64b)(T_Class*)1 };
    enum { mAdrSP = (ptr64b)(T_Parent*)(T_Class*)1 };
    enum { mSuperSubclass = Loki::SuperSubclass<T_Parent, T_Class>::value };

public:
    enum { yes = (mAdrTClass == mAdrSP) && mSuperSubclass };
};

#define DirectSubclassAssert(__Class, __Parent)                                                                        \
    static_assert(DirectSubclass<__Class, __Parent>::yes,                                                              \
        "bad class hierarchy: " #__Parent " is not direct parent for " #__Class "")

#endif

// static_assert( DirectSubclass<ColorTextW, CCObject>::yes, "bad class hierarchy" );
