//
//  TypeTraits.h
//  LostChapters
//
//  Created by Gleb Kolobkov on 30.07.15.
//  Copyright (c) 2015 Crazy Bit. All rights reserved.
//

#pragma once

#include <type_traits>
#include <typeindex>
#include <typeinfo>
#include <array>

namespace ie {
namespace types {

    template <bool... b> struct static_all_of;

    // implementation: recurse, if the first argument is true
    template <bool... tail> struct static_all_of<true, tail...> : static_all_of<tail...> {
    };

    // end recursion if first argument is false -
    template <bool... tail> struct static_all_of<false, tail...> : std::false_type {
    };

    // - or if no more arguments
    template <> struct static_all_of<> : std::true_type {
    };

    template <bool... b> struct static_any_of;

    template <bool... tail> struct static_any_of<false, tail...> : static_any_of<tail...> {
    };

    template <bool... tail> struct static_any_of<true, tail...> : std::true_type {
    };

    // - or if no more arguments
    template <> struct static_any_of<> : std::false_type {
    };

    template <bool B, class T = void> using enable_if_t = typename std::enable_if<B, T>::type;

    struct EnumClassHash {
        template <typename T> std::size_t operator()(T t) const
        {
            return static_cast<std::size_t>(t);
        }
    };
    template <typename Key>
    using HashType = typename std::conditional<std::is_enum<Key>::value, EnumClassHash, std::hash<Key>>::type;

    template <typename T> std::type_index getTypeIndex()
    {
        return std::type_index(typeid(T));
    }

    template <typename T> std::string getTypeName()
    {
        return typeid(T).name();
    }

    template <class T, typename U>
    typename std::enable_if<!std::is_floating_point<T>::value && !std::is_floating_point<U>::value, bool>::type
    safe_equal(const T& lhs, const U& rhs)
    {
        return lhs == rhs;
    }

    template <class T, typename U>
    typename std::enable_if<std::is_floating_point<T>::value || std::is_floating_point<U>::value, bool>::type
    safe_equal(const T& lhs, const U& rhs)
    {
        using res_type = decltype(lhs - rhs);
        return std::fabs(lhs - rhs) <= std::numeric_limits<res_type>::epsilon();
    }
} // namespace types
} // namespace ie