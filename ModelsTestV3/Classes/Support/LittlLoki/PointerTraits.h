//
//  PointerTraits.h
//  LostChapters
//
//  Created by a.arsentiev on 30.06.15.
//  Copyright (c) 2015 Crazy Bit. All rights reserved.
//

#pragma once

/// извлечение типов из указателя
/// @code
/// PointerTraits<int*>::Type == int
/// PointerTraits<int*>::PtrToType == int*
/// PointerTraits<int*>::PtrToConstType == const int*
/// @endcode
template <class T_Type> struct PointerTraits;

/// извлечение типов из указателя
/// @code
/// PointerTraits<int*>::Type == int
/// PointerTraits<int*>::PtrToType == int*
/// PointerTraits<int*>::PtrToConstType == const int*
/// @endcode
template <class T_Type> struct PointerTraits<T_Type*> {
    /// PointerTraits<int*>::Type == int
    typedef T_Type Type;
    /// PointerTraits<int*>::PtrToType == int*
    typedef T_Type* PtrToType;
    /// PointerTraits<int*>::PtrToConstType == const int*
    typedef const T_Type* PtrToConstType;
    /// PointerTraits<int&>::PtrToConstType == const int&
    typedef const T_Type& RefToConstType;
};
