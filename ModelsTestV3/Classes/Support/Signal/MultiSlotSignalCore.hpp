//
//  MultiSlotMultiSlotMultiSlotSignalCore.hpp
//  LostChapters
//
//  Created by a.arsentiev on 28.11.15.
//  Copyright © 2015 Crazy Bit. All rights reserved.
//

#pragma once

#include "cocos2d.h"

class MultiSlotSignalCore {
public:
    struct Method {
        cocos2d::CCInvocable* mTarget = nullptr;
        cocos2d::SEL_CallFunc mSel = nullptr;

        typedef std::vector<Method> Vec;
        Method(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s);
        Method(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s, const char* parent);

        inline cocos2d::Ref* castRef()
        {
            return dynamic_cast<cocos2d::Ref*>(mTarget);
        }

        inline bool operator==(const Method& m) const
        {
            return mTarget == m.mTarget && mSel == m.mSel;
        }
        inline bool equals(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s) const
        {
            return mTarget == t && mSel == s;
        }
    };

protected:
    ~MultiSlotSignalCore(); // destructor is protecter so we can create it not virtual
    MultiSlotSignalCore();

    /// методы подписанные на сигнал, может быть nullptr
    Method::Vec* mMethods = nullptr;
    mutable bool mIsEmiting = false;

    void connect(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s, const char* parent);
    void disconnect(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s);
    void disconnectTarget(cocos2d::CCInvocable* t);
    bool isConnected(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s);
    bool isConnected(cocos2d::CCInvocable*);

    void clear();

    // to remove
private:
    mutable Method::Vec* mMethodsToDisconnect = nullptr;

protected:
    inline Method::Vec* getMethodsToDisconnect() const
    {
        if (!mMethodsToDisconnect) {
            mMethodsToDisconnect = new Method::Vec;
        }
        return mMethodsToDisconnect;
    }
    /// был ли метод отключен в результате прошллых вызовов текущего еммита
    bool isDisconnected(const Method& m) const;
    /// запланировать отключенее, в текущемм еммите вызван уже не будет
    void scheduleDisconnect(const Method& m) const;
    /// удалить все методы которые были запланированы к удалению
    void clearDisconnected() const;

public:
    int count();
    /// методы подписанные на сигнал, может быть nullptr
};
