//
//  AsyncDelegat.h
//  HelloWorld
//
//  Created by v.ishtuin on 25.04.13.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#pragma once

#include "IAsyncCallBack.h"

#include "SignalImpl.h"
#include "IndexTupleBuilder.h"
#include "UnReferenceType.h"
#include "AsyncDelegateBase.hpp"

#include <tuple>

template <typename... T_Args> class AsyncDelegat;

//_________ void specialistion __________
template <> //
class AsyncDelegat<> : public Signal<>, public AsyncDelegateBase {
public:
    virtual void call() override
    {
        Signal<>::emit();
    }

    /// вернуть время запланированного вызова
    virtual bool canEmit() override
    {
        return checkTime();
    }
};

//_________ full specialistion __________

template <typename... T_Args> //
struct AsyncDelegateArgs {
    virtual ~AsyncDelegateArgs(){};

    typedef std::tuple<typename UnReferenceType<T_Args>::Type...> Args;

    using Indexes = typename IndexTupleBuilder<sizeof...(T_Args)>::Indexes;

    Args mArgs;

    AsyncDelegateArgs(typename UnReferenceType<T_Args>::Type... args)
        : mArgs(args...){};

    template <typename CLASS, typename METHOD_TYPE> //
    inline void callMethod(CLASS* obj, METHOD_TYPE method)
    {
        callMethodInternal(obj, method, Indexes());
    }

    template <typename FUNCTION> //
    inline void callFunction(const FUNCTION& f)
    {
        callFunctionInternal(f, Indexes());
    }

private:
    template <typename CLASS, typename METHOD_TYPE, int... T_Index> //
    inline void callMethodInternal(CLASS* obj, METHOD_TYPE method, IndexesTuple<T_Index...> const&)
    {
        (obj->*method)(std::get<T_Index>(mArgs)...);
    }

    template <typename FUNCTION, int... T_Index> //
    inline void callFunctionInternal(const FUNCTION& f, IndexesTuple<T_Index...> const&)
    {
        f(std::get<T_Index>(mArgs)...);
    }
};

template <typename... T_Args> //
class AsyncDelegat : public Signal<T_Args...>, public AsyncDelegateArgs<T_Args...>, public AsyncDelegateBase {
    using Args = AsyncDelegateArgs<T_Args...>;

public:
    // ____________________________
    AsyncDelegat()
        : Args(typename UnReferenceType<T_Args>::Type()...){};
    // ____________________________
    AsyncDelegat(typename ConstReferenceType<T_Args>::Type... args)
        : Args(args...){};

    // ____________________________
    virtual void call() override
    {
        Args::callMethod(this, &AsyncDelegat::emit);
    }
    // ____________________________
    template <class T_Class>
    static AsyncDelegat<T_Args...>* create(
        T_Class* t, void (T_Class::*s)(T_Args...), typename ConstReferenceType<T_Args>::Type... vars)
    {
        auto delegat = new AsyncDelegat<T_Args...>(vars...);
        delegat->autorelease();
        delegat->connect(t, s);
        return delegat;
    }

    /// вернуть время запланированного вызова
    virtual bool canEmit() override
    {
        return checkTime();
    }
};

// ____________________________
