//
//  ClassMethod.h
//  HelloWorld
//
//  Created by a.arsentiev on 07.06.13.
//  Copyright (c) 2013 Crazy Bit. All rights reserved.
//

#pragma once

#include "DirectSubclassTester.h"

// template < typename ... T_Args>
class ClassMethod {
    typedef void (cocos2d::CCInvocable::*Signature)();

    cocos2d::CCInvocable* mTarget;
    Signature mSelector;

public:
    template <class T_Class, typename... T_Args> ClassMethod(cocos2d::CCInvocable* t, void (T_Class::*s)(T_Args...))
    {
        DirectSubclassAssert(T_Class, cocos2d::CCInvocable);
        mTarget = t;
        mSelector = (Signature)s;
    }

    inline const Signature& getSelector() const
    {
        return mSelector;
    };
    inline cocos2d::CCInvocable* getTarget() const
    {
        return mTarget;
    }
};
