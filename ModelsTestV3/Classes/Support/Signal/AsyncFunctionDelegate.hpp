//
//  AsyncFunctionDelegate.hpp
//  Experiment
//
//  Created by a.cherdantceva on 18/05/17.
//
//

#pragma once

#include "Global.h"

#include "IAsyncCallBack.h"
#include "AsyncDelegateBase.hpp"

#include <functional>

class AsyncFunctionDelegate : public IAsyncCallBack, public AsyncDelegateBase {
    using Function = std::function<void()>;
    Function mFunction;
    cocos2d::CCInvocable* mTarget;

public:
    AsyncFunctionDelegate(cocos2d::CCInvocable* target, const Function& f);

    virtual void call() override;
    virtual bool canEmit() override;
    virtual bool isCallbackConnected(cocos2d::CCInvocable*, cocos2d::SEL_CallFunc) override;
    virtual bool isCallbackConnected(cocos2d::CCInvocable*) override;
    virtual void clearCallback() override;
    virtual bool emptyCallback() override;
};
