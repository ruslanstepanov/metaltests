//
//  OneSlotSignalCore.cpp
//  LostChapters
//
//  Created by a.arsentiev on 30.11.15.
//  Copyright © 2015 Crazy Bit. All rights reserved.
//

#include "OneSlotSignalCore.hpp"
#include "DebugUtils.h"

;

// _________________________________________________________________________________________
OneSlotSignalCore::OneSlotSignalCore()
    : mMethod(nullptr, nullptr)
{
}
// _________________________________________________________________________________________
OneSlotSignalCore::~OneSlotSignalCore()
{
}
// _________________________________________________________________________________________
void OneSlotSignalCore::connect(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s, const char* parent)
{
    if (mMethod.mTarget) {
        const char* name = "cocos2d::CCInvocable";
        cocos2d::Ref* obj = dynamic_cast<cocos2d::Ref*>(mMethod.mTarget);
        if (obj) {
            name = obj->getObjName().c_str();
        }

        //        LogicErrorF("already connected %s", name);
        //ie::debugMessage("already connected %s", name);
        return;
    }

    if (t == nullptr || s == nullptr)
        return;

    mMethod.mTarget = t;
    mMethod.mSel = s;
}

// _________________________________________________________________________________________
void OneSlotSignalCore::disconnect(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s)
{
    if (mMethod.mTarget == t && mMethod.mSel == s) {
        mMethod.mSel = nullptr;
        mMethod.mTarget = nullptr;
    }
}
// _________________________________________________________________________________________
void OneSlotSignalCore::disconnectTarget(cocos2d::CCInvocable* t)
{
    if (mMethod.mTarget == t) {
        mMethod.mSel = nullptr;
        mMethod.mTarget = nullptr;
    }
}
// _________________________________________________________________________________________
bool OneSlotSignalCore::isConnected(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s)
{
    if (mMethod.mTarget == t && mMethod.mSel == s) {
        return true;
    }

    return false;
}
// _________________________________________________________________________________________
bool OneSlotSignalCore::isConnected(cocos2d::CCInvocable* t)
{
    if (mMethod.mTarget == t) {
        return true;
    }

    return false;
}
// _________________________________________________________________________________________
int OneSlotSignalCore::count()
{
    if (mMethod.mSel && mMethod.mTarget)
        return 1;
    return 0;
}
// _________________________________________________________________________________________
#pragma mark disconnected manage
bool OneSlotSignalCore::isDisconnected(const Method& m) const
{ // был ли метод отключен в результате прошллых вызовов текущего еммита

    return false;
}

// _________________________________________________________________________________________
void OneSlotSignalCore::clearDisconnected() const
{ // удалить все методы которые были запланированы к удалению
    return;
}
// _________________________________________________________________________________________
void OneSlotSignalCore::clear()
{
    mMethod.mSel = nullptr;
    mMethod.mTarget = nullptr;
}
// _________________________________________________________________________________________
#pragma mark Method:
OneSlotSignalCore::Method::Method(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s)
{
    mTarget = t;
    mSel = s;
}
// _________________________________________________________________________________________
OneSlotSignalCore::Method::Method(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s, const char* parent)
{
    mTarget = t;
    mSel = s;
}
#pragma mark cache
