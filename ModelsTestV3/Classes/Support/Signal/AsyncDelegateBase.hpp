//
//  AsyncDelegateBase.hpp
//  Experiment
//
//  Created by a.cherdantceva on 18/05/17.
//
//

#pragma once

#include <ctime>

class AsyncDelegateBase {
protected:
    /// время звпланированного вызова (можно использовать только один ин них)
    double mCallTimeLocal = 0;
    /// время вызова по сверверному времени (можно использовать только один ин них)
    time_t mCallTimeServer = 0;

    bool checkTime() const;

public:
    virtual ~AsyncDelegateBase(){};

    void setCallTimeLocal(double t);
    void setCallTimeServer(time_t t);
};