//
//  IndexTupleBuilder.h
//  HelloWorld
//
//  Created by Aleksandr Arsentiev on 12.03.13.
//
//

#pragma once

// Определяем класс собственно кортежа
template <int... Idxs> struct IndexesTuple {
};

// Определяем общий вид шаблона, используемого для порождения кортежа
template <int Num, typename Tp = IndexesTuple<>> struct IndexTupleBuilder;

// Определяем специализацию, которая генерирует последовательность чисел в виде пакета целочисленных параметров.
// Для этого в качестве второго параметра в объявлении шаблона используется не собственно тип кортежа, а ранее
// сформированный
// пакет. Для получения итогового пакета наследуемся от порождающегося шаблона, добавляя в пакет новое число
template <int Num, int... Idxs>
struct IndexTupleBuilder<Num, IndexesTuple<Idxs...>>
    : IndexTupleBuilder<Num - 1, IndexesTuple<Idxs..., sizeof...(Idxs)>> {
};

// Терминирующая рекурсию специализация. Содержит итоговый typedef, определяющий кортеж с нужным набором чисел
template <int... Idxs> struct IndexTupleBuilder<0, IndexesTuple<Idxs...>> {
    typedef IndexesTuple<Idxs...> Indexes;
};
