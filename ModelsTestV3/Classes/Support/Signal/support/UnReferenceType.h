//
//  UnReferenceType.h
//  HelloWorld
//
//  Created by Aleksandr Arsentiev on 12.03.13.
//
//

#pragma once

template <typename T> struct UnReferenceType;

template <typename T_Orig> struct UnReferenceType<T_Orig&> {
    typedef T_Orig Type;
};

template <typename T_Orig> struct UnReferenceType<const T_Orig&> {
    typedef T_Orig Type;
};

template <typename T_Orig> struct UnReferenceType {
    typedef T_Orig Type;
};

template <typename T> struct ConstReferenceType;

template <typename T_Orig> struct ConstReferenceType<T_Orig&> {
    typedef const T_Orig& Type;
};

template <typename T_Orig> struct ConstReferenceType<const T_Orig&> {
    typedef const T_Orig& Type;
};

template <typename T_Orig> struct ConstReferenceType {
    typedef T_Orig Type;
};
