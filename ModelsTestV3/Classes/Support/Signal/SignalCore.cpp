//
//  SignalCore.cpp
//  HelloWorld
//
//  Created by a.arsentiev on 25.11.12.
//
//

#include "SignalCore.h"
#include "StlUtils.h"

/// когда число слотов достигает 16 - создадаим кешь
static const int sCacheLimit = 16;

;

// _________________________________________________________________________________________
SignalCore::SignalCore()
{
    if (mIsEmiting && mMethods == nullptr) {
    //        LogicError("memmory corrupted");
    }
}
// _________________________________________________________________________________________
SignalCore::~SignalCore()
{
    //    IEAssert(!mIsEmiting, "trying to destruct emiting signal");

    delete mMethods;
    delete mMethodsToDisconnect;
    delete mConnectionsCache;
}
// _________________________________________________________________________________________
void SignalCore::connect(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s)
{
    if (mIsEmiting && mMethods == nullptr) {
        //        LogicError("memmory corrupted");
    }

    if (t == nullptr || s == nullptr)
        return;

    if (isConnected(t, s)) {
        removeFromMethodsToDisconnect(t, s);
        return;
    }

    if (!mMethods)
        mMethods = new Method::List;

    mMethods->push_back(Method(t, s));

    if (mConnectionsCache) {
        addToCache(t);
    }
    else if (mMethods->size() >= sCacheLimit) {
        createConnectionsCache();
    }
}
// _________________________________________________________________________________________
void SignalCore::connectRetain(cocos2d::Ref* t, cocos2d::SEL_CallFunc s)
{
    if (mIsEmiting && mMethods == nullptr) {
        //        LogicError("memmory corrupted");
    }

    if (t == nullptr || s == nullptr)
        return;

    if (isConnected(t, s)) {
        removeFromMethodsToDisconnect(t, s);
        return;
    }

    if (!mMethods)
        mMethods = new Method::List;

    mMethods->push_back(Method(t, s, true));

    if (mConnectionsCache) {
        addToCache(t);
    }
    else if (mMethods->size() >= sCacheLimit) {
        createConnectionsCache();
    }
}
// _________________________________________________________________________________________
void SignalCore::disconnect(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s)
{
    if (!mMethods) {
        return;
    }

    int* cachePtr = nullptr;
    if (mConnectionsCache) {
        cachePtr = stlh::member(*mConnectionsCache, t);
        if (cachePtr == nullptr || *cachePtr == 0) {
            return;
        }
    }

    for (auto it = mMethods->begin(); it != mMethods->end(); ++it) {
        const Method& m = *it;

        if (m.equals(t, s)) {
            if (mIsEmiting) {
                // сигнал еммитится, сделаем дисконнект позже
                scheduleDisconnect(m);
            }
            else {
                // сделаем дисконнект сечас
                mMethods->erase(it);
                if (cachePtr) {
                    *cachePtr -= 1;
                }
            }
            return;
        }
    }
}
// _________________________________________________________________________________________
void SignalCore::disconnectTarget(cocos2d::CCInvocable* t)
{
    if (!mMethods) {
        return;
    }

    int* cachePtr = nullptr;
    if (mConnectionsCache) {
        cachePtr = stlh::member(*mConnectionsCache, t);
        if (cachePtr == nullptr || *cachePtr == 0) {
            return;
        }
    }

    for (auto it = mMethods->begin(); it != mMethods->end();) {
        const Method& m = *it;
        bool errased = false;

        if (m.mTarget == t) {
            if (mIsEmiting) {
                // сигнал еммитится, сделаем дисконнект позже
                scheduleDisconnect(m);
            }
            else {
                // сделаем дисконнект сечас
                it = mMethods->erase(it);
                errased = true;
                if (cachePtr) {
                    *cachePtr -= 1;
                }
            }
        }

        if (not errased)
            ++it;
    }
}
// _________________________________________________________________________________________
bool SignalCore::isConnected(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s)
{
    if (!mMethods)
        return false;

    if (mConnectionsCache) {
        int* countPtr = stlh::member(*mConnectionsCache, t);
        if (countPtr == nullptr || *countPtr == 0) {
            return false;
        }
    }

    for (const Method& m : *mMethods) {
        if (m.equals(t, s))
            return true;
    }

    return false;
}
// _________________________________________________________________________________________
bool SignalCore::isConnected(cocos2d::CCInvocable* t)
{
    if (!mMethods)
        return false;

    if (mConnectionsCache) {
        int* countPtr = stlh::member(*mConnectionsCache, t);
        if (countPtr == nullptr || *countPtr == 0) {
            return false;
        }
        if (countPtr && *countPtr > 0) {
            return true;
        }
    }

    for (const Method& m : *mMethods) {
        if (m.mTarget == t)
            return true;
    }

    return false;
}
// _________________________________________________________________________________________
int SignalCore::count()
{
    if (mMethods == nullptr)
        return 0;

    if (mMethods && mMethodsToDisconnect == nullptr)
        return (int)mMethods->size(); // FIXME: AUTO-FIX (fixed: 9)

    if (mMethods && mMethodsToDisconnect) {
        return (int)mMethods->size() - (int)mMethodsToDisconnect->size();
    }

    return 0;
}
// _________________________________________________________________________________________
#pragma mark disconnected manage
bool SignalCore::isDisconnected(const Method& m) const
{ // был ли метод отключен в результате прошллых вызовов текущего еммита

    if (mMethodsToDisconnect == nullptr || mMethodsToDisconnect->empty())
        return false;

    for (Method& disconnected : *getMethodsToDisconnect())
        if (m == disconnected)
            return true;

    return false;
}
// _________________________________________________________________________________________
void SignalCore::scheduleDisconnect(const Method& m) const
{ // запланировать отключенее, в текущемм еммите вызван уже не будет
    if (isDisconnected(m))
        return;

    getMethodsToDisconnect()->push_back(Method(m.mTarget, m.mSel /*, false*/));
}
// _________________________________________________________________________________________
void SignalCore::clearDisconnected() const
{ // удалить все методы которые были запланированы к удалению
    if (!mMethodsToDisconnect)
        return;

    if (mMethodsToDisconnect->empty())
        return;

    if (!mMethods) {
        //        LogicError("methods must exist");
        return;
    }

    if (mIsEmiting) {
        //        LogicError("can't clear while emiting");
        return;
    }

    for (Method& disconnected : *mMethodsToDisconnect) {
        for (auto it = mMethods->begin(); it != mMethods->end(); ++it) {
            const Method& m = *it;

            if (m == disconnected) {
                mMethods->erase(it);
                break;
            }
        }
    }

    if (mConnectionsCache) {
        delete mConnectionsCache;
        createConnectionsCache();
    }
    mMethodsToDisconnect->clear();
}
// _________________________________________________________________________________________
void SignalCore::removeFromMethodsToDisconnect(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s)
{
    if (!mMethodsToDisconnect)
        return;

    if (mMethodsToDisconnect->empty())
        return;

    for (auto it = mMethodsToDisconnect->begin(); it != mMethodsToDisconnect->end();) {
        if (it->equals(t, s))
            it = mMethodsToDisconnect->erase(it);
        else
            ++it;
    }
}
// _________________________________________________________________________________________
void SignalCore::clear()
{
    if (mMethods == nullptr || mMethods->empty())
        return;

    if (!mIsEmiting) {
        delete mMethods;
        delete mMethodsToDisconnect;
        delete mConnectionsCache;
        mMethods = nullptr;
        mMethodsToDisconnect = nullptr;
        mConnectionsCache = nullptr;
    }
    else {
        for (auto it = mMethods->begin(); it != mMethods->end(); ++it) {
            const Method& m = *it;
            scheduleDisconnect(m);
        }
    }
}
// _________________________________________________________________________________________
#pragma mark Method:
SignalCore::Method::Method(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s)
{
    mTarget = t;
    mSel = s;
    mRetain = false;
}
// _________________________________________________________________________________________
SignalCore::Method::Method(cocos2d::Ref* t, cocos2d::SEL_CallFunc s, bool retain)
{
    mTarget = t;
    mSel = s;
    if (retain) {
        t->retain();
        mRetain = true;
    }
}
// _________________________________________________________________________________________
SignalCore::Method::Method(const Method& other)
{
    mSel = other.mSel;
    mTarget = other.mTarget;
    mRetain = other.mRetain;

    if (mRetain && mTarget)
        static_cast<cocos2d::Ref*>(mTarget)->retain();
}
// _________________________________________________________________________________________
const SignalCore::Method& SignalCore::Method::operator=(const Method& other)
{
    mSel = other.mSel;
    mTarget = other.mTarget;
    mRetain = other.mRetain;

    if (mRetain && mTarget)
        static_cast<cocos2d::Ref*>(mTarget)->retain();

    return *this;
}
// _________________________________________________________________________________________
SignalCore::Method::~Method()
{
    if (mRetain && mTarget) {
        static_cast<cocos2d::Ref*>(mTarget)->release();
        // mTarget = nullptr;
        // mRetain = false;
    }
}
#pragma mark cache
// _________________________________________________________________________________________
void SignalCore::createConnectionsCache() const
{
    mConnectionsCache = new ConnectionCounterCache;
    for (auto& m : *mMethods) {
        addToCache(m.mTarget);
    }
}
// _________________________________________________________________________________________
void SignalCore::addToCache(cocos2d::CCInvocable* t) const
{
    int* countPtr = stlh::member(*mConnectionsCache, t);
    if (countPtr) {
        *countPtr += 1;
    }
    mConnectionsCache->insert({ t, 1 });
}
