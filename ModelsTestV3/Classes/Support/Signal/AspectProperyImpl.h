//
//  AspectProperyImpl.h
//  HelloWorld
//
//  Created by a.arsentiev on 01.03.13.
//
//

#pragma once

#include "AspectProperyCore.h"
#include "DirectSubclassTester.h"
#include "UnReferenceType.h"
#include <string>

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
template <typename T_RType, typename T_DefType, T_DefType T_DefVal, typename... T_Args>

class AspectProperyExecT : public AspectProperyCore {
public:
    typedef T_RType (cocos2d::CCInvocable::*Signature)(T_Args...);

protected:
    inline ~AspectProperyExecT(){};
    AspectProperyExecT()
        : mDefVal(T_DefVal){};

public:
    // ____________________________

protected:
    T_RType mDefVal;

public:
    virtual const T_RType& getDefVal()
    {
        return mDefVal;
    };
    // ____________________________
    inline T_RType operator()(T_Args... val)
    {
        return emit(val...);
    };
    // ____________________________
    T_RType emit(T_Args... val)
    {
        if (mTarget && mSel) {
            cocos2d::CCInvocable* t = mTarget;
            Signature s = (Signature)mSel;
            return static_cast<T_RType>((t->*s)(val...));
        }
        return mDefVal;
    }
// ____________________________
#define slotToParent(__val) ((void (cocos2d::CCInvocable::*)())(__val))
    template <class T_Class> inline void connect(cocos2d::CCInvocable* t, T_RType (T_Class::*s)(T_Args...))
    {
        DirectSubclassAssert(T_Class, cocos2d::CCInvocable);
        AspectProperyCore::connect(t, slotToParent(s));
    }
    // ____________________________
    template <class T_Class> inline void disconnect(cocos2d::CCInvocable* t, T_RType (T_Class::*s)(T_Args...))
    {
        DirectSubclassAssert(T_Class, cocos2d::CCInvocable);
        AspectProperyCore::disconnect(t, slotToParent(s));
    }
// ____________________________
#undef slotToParent

    static_assert(&AspectProperyExecT<T_RType, T_DefType, T_DefVal, T_Args...>::mSel == &AspectProperyCore::mSel,
        "unknown magic");
};

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
template <int T_DefVal, typename... T_Args>
class IntProperty : public AspectProperyExecT<int, int, T_DefVal, T_Args...> {
};
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
template <bool T_DefVal, typename... T_Args>
class BoolProperty : public AspectProperyExecT<bool, bool, T_DefVal, T_Args...> {
};
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

template <class T_CCObject> class ObjProperty; //

// класс может быть конкретезирован только для укзателей
// gcc 4.6.3 не может выполнить приведение null, nullptr или 0 к укзателю, потому T_DefType == int
// исправлено в gcc4.7
template <class T_CCObjectPtr>
class ObjProperty<T_CCObjectPtr*> : public AspectProperyExecT<T_CCObjectPtr*, T_CCObjectPtr*, nullptr> {
};
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
template <int T_DefVal> // IMPOTENT: for T_RType<double> we use T_DefType<int> because of c++ standart
class DoubleProperty : public AspectProperyExecT<double, int, T_DefVal> {
};
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
template <typename... T_Args>
class StringRefProperty
    : public AspectProperyExecT<const std::string&, const std::string&, AspectProperyCore::StringZero, T_Args...>
// надо будет эти сигнатуры переделать, так сделал чтобы понять механику
{
};
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
template <class T_CCObjectPtr, typename... T_Args>
class PtrProperty : public AspectProperyExecT<T_CCObjectPtr, T_CCObjectPtr, nullptr, T_Args...> {
};

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
