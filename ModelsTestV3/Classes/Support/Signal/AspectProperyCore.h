//
//  AspectProperyCore.h
//  HelloWorld
//
//  Created by a.arsentiev on 15.01.13.
//
//

#pragma once
#include "SignalCore.h"
//#include "Item.h"

class AspectProperyCore {
    //_IMPLEMENT_RTTI_
    // protected:
public:
    cocos2d::SEL_CallFunc mSel;
    cocos2d::CCInvocable* mTarget;

protected:
    inline ~AspectProperyCore(){};
    AspectProperyCore();

public:
    void connect(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s);
    void disconnect();
    void disconnect(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s);
    bool isConnected();

    void clear();

public:
    static std::string StringZero;
};
