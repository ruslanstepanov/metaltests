//
//  AsyncFunctionDelegate.cpp
//  Experiment
//
//  Created by a.cherdantceva on 18/05/17.
//
//

#include "AsyncFunctionDelegate.hpp"

//______________________________________________________________________________________
AsyncFunctionDelegate::AsyncFunctionDelegate(cocos2d::CCInvocable* target, const Function& f)
    : mFunction(f)
    , mTarget(target)
{
}
//______________________________________________________________________________________
void AsyncFunctionDelegate::call()
{
    if (nullptr == mTarget) {
        return;
    }
    mFunction();
}
//______________________________________________________________________________________
bool AsyncFunctionDelegate::canEmit()
{
    return checkTime();
}
//______________________________________________________________________________________
bool AsyncFunctionDelegate::isCallbackConnected(cocos2d::CCInvocable*, cocos2d::SEL_CallFunc)
{
    return false;
}
//______________________________________________________________________________________
bool AsyncFunctionDelegate::isCallbackConnected(cocos2d::CCInvocable* target)
{
    return (mTarget == target);
};
//______________________________________________________________________________________
void AsyncFunctionDelegate::clearCallback()
{
    mTarget = nullptr;
}
//______________________________________________________________________________________
bool AsyncFunctionDelegate::emptyCallback()
{
    return (nullptr == mTarget);
}
//______________________________________________________________________________________
