//
//  AsyncDelegateBase.cpp
//  Experiment
//
//  Created by a.cherdantceva on 18/05/17.
//
//

#include "AsyncDelegateBase.hpp"

#include "Utils.h"
#include "DateAndTimeUtils.h"
//#include "GameSession.h"

//______________________________________________________________________________________
bool AsyncDelegateBase::checkTime() const
{
    if (mCallTimeLocal == 0.0 && mCallTimeServer == 0) {
        return true;
    }
    if (mCallTimeLocal != 0.0 && mCallTimeServer != 0) {
        //LogicError("bad time settings");
        return false;
    }

    if (mCallTimeServer != 0) {
        return false;//(SGameSession() && mCallTimeServer <= SGameSession()->getCurrentTime());
    }

    if (mCallTimeLocal != 0) {
        return (mCallTimeLocal <= ie::timeLocal());
    }

    //LogicError("bad time settings");
    return false;
}
//______________________________________________________________________________________
void AsyncDelegateBase::setCallTimeLocal(double t)
{
    mCallTimeLocal = t;
}
//______________________________________________________________________________________
void AsyncDelegateBase::setCallTimeServer(time_t t)
{
    mCallTimeServer = t;
}
//______________________________________________________________________________________
