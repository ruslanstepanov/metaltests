//
//  Delegat.h
//  HelloWorld
//
//  Created by Aleksandr Arsentiev on 12.03.13.
//
//

#pragma once

#include "SignalCore.h"
#include "DirectSubclassTester.h"
#include "ClassMethod.h"
#include "IndexTupleBuilder.h"
#include "UnReferenceType.h"
#include "CppCodeData.h"

#include <tuple>

// ==============================================================================

#define slotToParent(__val) ((void (cocos2d::CCInvocable::*)())(__val))

template <typename... T_Args> class Delegat : public cocos2d::Ref {
    typedef Delegat<T_Args...> MyType;

protected:
    typedef void (cocos2d::CCInvocable::*Signature)(T_Args...);
    typedef std::tuple<typename UnReferenceType<T_Args>::Type...> Args;
    Args mArgs;
    SignalCore::Method mMethod;

public:
    // ____________________________
    /// defoult constructor for conteineds
    Delegat()
        : mArgs(typename UnReferenceType<T_Args>::Type()...)
        , mMethod(nullptr, nullptr){};
    //	// ____________________________
    //	/// deprecated
    //	Delegat(  T_Args... defVals, const char* parenFunction = FunctionName::sName ):
    //	mArgs(defVals...),
    //	mMethod(nullptr,nullptr,parenFunction)
    //	{};
    // ____________________________
    /// объект, метод, аргументы, точка инициализации (для отладки)
    template <class T_Class>
    Delegat(cocos2d::CCInvocable* t, void (T_Class::*s)(T_Args...), T_Args... defVals)
        : mArgs(defVals...)
        , mMethod(t, slotToParent(s))
    {
        DirectSubclassAssert(T_Class, cocos2d::CCInvocable);
    };
    // ____________________________
    /// конструктор копирования
    Delegat(const MyType& from)
        : mArgs(from.mArgs)
        , mMethod(from.mMethod.mTarget, from.mMethod.mSel, from.mMethod.mParent){};
    // ____________________________
    Delegat<T_Args...>& operator=(const MyType& from)
    {
        mArgs = from.mArgs;
        mMethod.mTarget = from.mMethod.mTarget;
        mMethod.mSel = from.mMethod.mSel;

        return *this;
    }
    // ____________________________
    /// получить аргумент кортежа по индексу T_Index
    template <int T_Index> const typename ConstReferenceType<decltype(std::get<T_Index>(mArgs))>::Type get()
    {
        return std::get<T_Index>(mArgs);
    }
    // ____________________________
    /// выставить аргумент кортежа по индексу T_Index
    template <int T_Index> void set(typename ConstReferenceType<decltype(std::get<T_Index>(mArgs))>::Type val)
    {
        std::get<T_Index>(mArgs) = val;
    }
    // ____________________________
    void emit()
    {
        makeEmit(typename IndexTupleBuilder<sizeof...(T_Args)>::Indexes());
        //		Signal<T_Args... >::emit( std::tr1::get<0>(mArgs), std::tr1::get<1>(mArgs) ....etc );
    }
    // ____________________________
protected:
    template <int... T_Index> void makeEmit(IndexesTuple<T_Index...> const&)
    {
        if (mMethod.mTarget && mMethod.mSel) {
            cocos2d::CCInvocable* t = mMethod.mTarget;
            Signature s = (Signature)mMethod.mSel;
            (t->*s)(std::get<T_Index>(mArgs)...);
        }

        //        Signal<T_Args... >::emit( std::tr1::get<T_Index>(mArgs)...);
    }
};
#undef slotToParent

typedef Delegat<int> DelegatI;
typedef Delegat<> DelegatV;
typedef Delegat<const std::string&> DelegatS;
