//
//  AspectProperyCore.cpp
//  HelloWorld
//
//  Created by a.arsentiev on 01.03.13.
//
//

#include "AspectProperyCore.h"
#include "Utils.h"
#include "DebugUtils.h"

std::string AspectProperyCore::StringZero = "";

// _________________________________________________________________________________________
AspectProperyCore::AspectProperyCore()
{
    mSel = nullptr;
    mTarget = nullptr;
}
// _________________________________________________________________________________________
void AspectProperyCore::connect(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s)
{
    if (mTarget) {
        const char* name = "CCInvocable";
        cocos2d::Ref* obj = dynamic_cast<cocos2d::Ref*>(mTarget);
        if (obj) {
            name = "";//obj->getObjName().c_str();
        }

        //LogicErrorF("already connected %s", name);
        //ie::debugMessage("already connected %s", name);
        return;
    }

    mTarget = t;
    mSel = s;
}
// _________________________________________________________________________________________
void AspectProperyCore::clear()
{
    mSel = nullptr;
    mTarget = nullptr;
}
// _________________________________________________________________________________________
void AspectProperyCore::disconnect()
{
    mSel = nullptr;
    mTarget = nullptr;
}
// _________________________________________________________________________________________
void AspectProperyCore::disconnect(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s)
{
    if (mTarget == t && mSel == s) {
        mSel = nullptr;
        mTarget = nullptr;
    }
}
// _________________________________________________________________________________________
bool AspectProperyCore::isConnected()
{
    return mTarget;
}
// _________________________________________________________________________________________
