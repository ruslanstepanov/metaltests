//
//  SignalV.h
//  HelloWorld
//
//  Created by a.arsentiev on 25.11.12.
//
//

#pragma once
#include "SignalCore.h"
#include "OneSlotSignalCore.hpp"
#include "MultiSlotSignalCore.hpp"
//#include "DirectSubclassTester.h"
#include "ClassMethod.h"
#include "CppCodeData.h"
#include "IAsyncCallBack.h" // используется для возможности вызоыва и обработки в Коллерах СигналДелегата и АинкКолбека по одному интерфеусу
#include "Delegat.h"
#include "FunctCcRef.h"

//#define slot(__val) ( (void (cocos2d::CCInvocable::*)() ) (&__val) )

template <class T_Base, typename... T_Args> class SignalImpl : public IAsyncCallBack, protected T_Base {
#define slotToParent(__val) ((void (cocos2d::CCInvocable::*)())(__val))
public:
    // шаблонные аргументы используются для генерации сигнатуры вызываемой ф-ии
    using Signature = void (cocos2d::CCInvocable::*)(T_Args...);
    using StaticSignature = void (*)(T_Args...);

public:
    inline void operator()(T_Args... val)
    {
        emit(val...);
    };

    using SignalCore::connectRetain;

    // ____________________________
    SignalImpl(){};
    virtual ~SignalImpl(){};
    // ____________________________
    SignalImpl(const ClassMethod& method)
    {
        T_Base::connect(method.getTarget(), slotToParent(method.getSelector()), __PRETTY_FUNCTION__);
    }
    // ____________________________
    void emit(T_Args... val) const
    {
        if (!T_Base::mMethods)
            return;

        bool prevState = T_Base::mIsEmiting;

        // отмечаем что сигнал вызывается
        // все примененные к нему дисконнекты будут выполнены отложено в clearDisconnected
        T_Base::mIsEmiting = true;

        for (const typename T_Base::Method& m : *T_Base::mMethods) {
            if (!T_Base::isDisconnected(m)) {
                cocos2d::CCInvocable* t = m.mTarget;
                Signature s = (Signature)m.mSel;
                (t->*s)(val...);
            }
        }

        T_Base::mIsEmiting = prevState;
        if (T_Base::mIsEmiting == false) {
            T_Base::clearDisconnected();
        }
    }
#pragma mark connect with cocos2d::CCInvocable::methods
    // ____________________________

    template <class T_Class> inline void connect(cocos2d::CCInvocable* t, void (T_Class::*s)(T_Args...))
    {
        DirectSubclassAssert(T_Class, cocos2d::CCInvocable);
        T_Base::connect(t, slotToParent(s));
    }
    // ____________________________
    template <class T_Class> inline void disconnect(cocos2d::CCInvocable* t, void (T_Class::*s)(T_Args...))
    {
        DirectSubclassAssert(T_Class, cocos2d::CCInvocable);
        T_Base::disconnect(t, slotToParent(s));
    }
    // ____________________________
    inline void disconnectTarget(cocos2d::CCInvocable* t)
    {
        T_Base::disconnectTarget(t);
    }
    // ____________________________
    template <class T_Class> inline bool isConnected(cocos2d::CCInvocable* t, void (T_Class::*s)(T_Args...))
    {
        DirectSubclassAssert(T_Class, cocos2d::CCInvocable);
        return T_Base::isConnected(t, slotToParent(s));
    }
// ____________________________
#pragma mark connect with Signal<T_Args...>
    inline void connect(const SignalImpl<T_Base, T_Args...>& signal)
    {
        connect((cocos2d::CCInvocable*)&signal,
            (SignalImpl<T_Base, T_Args...>::Signature) & SignalImpl<T_Base, T_Args...>::emit);
    }
    // ____________________________
    inline void disconnect(const SignalImpl<T_Base, T_Args...>& signal)
    {
        disconnect((cocos2d::CCInvocable*)&signal,
            (SignalImpl<T_Base, T_Args...>::Signature) & SignalImpl<T_Base, T_Args...>::emit);
    }
    // ____________________________
    inline bool isConnected(const SignalImpl<T_Base, T_Args...>& signal)
    {
        return isConnected((cocos2d::CCInvocable*)&signal,
            (SignalImpl<T_Base, T_Args...>::Signature) & SignalImpl<T_Base, T_Args...>::emit);
    }
#pragma mark connect with Delegat<T_DelegatArgs...>
    template <typename... T_DelegatArgs> inline void connect(const Delegat<T_DelegatArgs...>& delegat)
    {
        connect((cocos2d::CCInvocable*)&delegat, &Delegat<T_DelegatArgs...>::emit);
    }
    template <typename... T_DelegatArgs> inline void connectRetain(Delegat<T_DelegatArgs...>* delegat)
    {
        SignalCore::connectRetain((cocos2d::Ref*)delegat, slotToParent(&Delegat<T_DelegatArgs...>::emit));
    }
    // ____________________________
    template <typename... T_DelegatArgs> inline void disconnect(const Delegat<T_DelegatArgs...>& delegat)
    {
        disconnect((cocos2d::CCInvocable*)&delegat, &Delegat<T_DelegatArgs...>::emit);
    }
    // ____________________________
    template <typename... T_DelegatArgs> inline bool isConnected(const Delegat<T_DelegatArgs...>& delegat)
    {
        return isConnected((cocos2d::CCInvocable*)&delegat, &Delegat<T_DelegatArgs...>::emit);
    }
#pragma mark std::function
    inline void connect(const std::function<void(T_Args...)>& f)
    {
        using CcFunctWrapper = FunctCcRef<T_Args...>;
        auto* ccf = new CcFunctWrapper(f);
        SignalCore::connectRetain(ccf, slotToParent(&CcFunctWrapper::emit));
        ccf->release();
    }

#pragma mark support
    // ____________________________
    inline int slotCount()
    {
        return T_Base::count();
    }

    // ____________________________
protected:
    void call() override
    {
        // IAsyncCallBack используется для возможности вызоыва и обработки в Коллерах СигналДелегата и АинкКолбека по
        // одному интерфеусу
        // у простых сигналов call не реализуется
        //		UnimplBreak();
    }

    /// интерфейс для сравнения колбеков
    bool isCallbackConnected(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s) override
    {
        return T_Base::isConnected(t, s);
    }
    /// интерфейс для сравнения колбеков
    virtual bool isCallbackConnected(cocos2d::CCInvocable* t) override
    {
        return T_Base::isConnected(t);
    }
    /// отписать все от колбека
    void clearCallback() override
    {
        T_Base::clear();
    }
    /// подписано ли что нибуть на колбек
    bool emptyCallback() override
    {
        return (T_Base::count() == 0);
    }
};

template <typename... T_Args> class Signal : public SignalImpl<SignalCore, T_Args...> {
public:
    Signal(){};
    Signal(const ClassMethod& method)
    {
        SignalCore::connect(method.getTarget(), slotToParent(method.getSelector()), __PRETTY_FUNCTION__);
    }
};

template <typename... T_Args> class SignalFast : public SignalImpl<MultiSlotSignalCore, T_Args...> {
public:
    SignalFast(){};
    SignalFast(const ClassMethod& method)
    {
        MultiSlotSignalCore::connect(method.getTarget(), slotToParent(method.getSelector()), __PRETTY_FUNCTION__);
    }

    void clear()
    {
        MultiSlotSignalCore::clear();
    }
};

#undef slotToParent

typedef Signal<> SignalV;
typedef Signal<const cocos2d::Color3B&> SignalColor;
typedef Signal<int> SignalI;
typedef Signal<bool> SignalB;
typedef Signal<PrototypeId> SignalPid;
typedef Signal<void*> SignalPtr;
typedef Signal<cocos2d::Ref*> SignalO;
typedef Signal<const std::string&> SignalS;

typedef SignalFast<> SignalFastV;
typedef SignalFast<const cocos2d::Color3B&> SignalFastColor;
typedef SignalFast<int> SignalFastI;
typedef SignalFast<void*> SignalFastPtr;
typedef SignalFast<cocos2d::Ref*> SignalFastO;
typedef SignalFast<const std::string&> SignalFastS;

#include "SignalOne.h"
