//
//  SignalOne.h
//  LostChapters
//
//  Created by a.arsentiev on 30.11.15.
//  Copyright © 2015 Crazy Bit. All rights reserved.
//

#pragma once

#include "OneSlotSignalCore.hpp"

template <typename... T_Args> class SignalOne : public IAsyncCallBack, protected OneSlotSignalCore {
#define slotToParent(__val) ((void (cocos2d::CCInvocable::*)())(__val))
public:
    // шаблонные аргументы используются для генерации сигнатуры вызываемой ф-ии
    typedef void (cocos2d::CCInvocable::*Signature)(T_Args...);
    typedef void (*StaticSignature)(T_Args...);

public:
    inline void operator()(T_Args... val)
    {
        emit(val...);
    };
    // ____________________________
    SignalOne(){};
    virtual ~SignalOne(){};
    // ____________________________
    SignalOne(const ClassMethod& method)
    {
        connect(method.getTarget(), slotToParent(method.getSelector()), __PRETTY_FUNCTION__);
    }
    // ____________________________
    void emit(T_Args... val) const
    {
        if (mMethod.mTarget && mMethod.mSel) {
            cocos2d::CCInvocable* t = mMethod.mTarget;
            Signature s = (Signature)mMethod.mSel;
            return (t->*s)(val...);
        }
    }
#pragma mark connect with cocos2d::CCInvocable::methods
    // ____________________________

    template <class T_Class>
    inline void connect(
        cocos2d::CCInvocable* t, void (T_Class::*s)(T_Args...), const char* mParenFunction = FunctionName::sName)
    {
        DirectSubclassAssert(T_Class, cocos2d::CCInvocable);
        OneSlotSignalCore::connect(t, slotToParent(s), mParenFunction);
    }
    // ____________________________
    template <class T_Class> inline void disconnect(cocos2d::CCInvocable* t, void (T_Class::*s)(T_Args...))
    {
        DirectSubclassAssert(T_Class, cocos2d::CCInvocable);
        OneSlotSignalCore::disconnect(t, slotToParent(s));
    }
    // ____________________________
    template <class T_Class> inline bool isConnected(cocos2d::CCInvocable* t, void (T_Class::*s)(T_Args...))
    {
        DirectSubclassAssert(T_Class, cocos2d::CCInvocable);
        return OneSlotSignalCore::isConnected(t, slotToParent(s));
    }
// ____________________________
#pragma mark connect with Signal<T_Args...>
    inline void connect(const SignalOne<T_Args...>& signal, const char* mParenFunction = FunctionName::sName)
    {
        OneSlotSignalCore::connect(
            (cocos2d::CCInvocable*)&signal, (SignalOne<T_Args...>::Signature) & SignalOne<T_Args...>::emit, mParenFunction);
    }
    // ____________________________
    inline void disconnect(const SignalOne<T_Args...>& signal)
    {
        OneSlotSignalCore::disconnect(
            (cocos2d::CCInvocable*)&signal, (SignalOne<T_Args...>::Signature) & SignalOne<T_Args...>::emit);
    }
    // ____________________________
    inline bool isConnected(const SignalOne<T_Args...>& signal)
    {
        return OneSlotSignalCore::isConnected(
            (cocos2d::CCInvocable*)&signal, (SignalOne<T_Args...>::Signature) & SignalOne<T_Args...>::emit);
    }
#pragma mark connect with Delegat<T_DelegatArgs...>
    template <typename... T_DelegatArgs>
    inline void connect(const Delegat<T_DelegatArgs...>& delegat, const char* mParenFunction = FunctionName::sName)
    {
        OneSlotSignalCore::connect((cocos2d::CCInvocable*)&delegat, &Delegat<T_DelegatArgs...>::emit, mParenFunction);
    }
    // ____________________________
    template <typename... T_DelegatArgs> inline void disconnect(const Delegat<T_DelegatArgs...>& delegat)
    {
        OneSlotSignalCore::disconnect((cocos2d::CCInvocable*)&delegat, &Delegat<T_DelegatArgs...>::emit);
    }
    // ____________________________
    template <typename... T_DelegatArgs> inline bool isConnected(const Delegat<T_DelegatArgs...>& delegat)
    {
        return OneSlotSignalCore::isConnected((cocos2d::CCInvocable*)&delegat, &Delegat<T_DelegatArgs...>::emit);
    }

#pragma mark support
    // ____________________________
    inline int slotCount()
    {
        return count();
    }

    // ____________________________
protected:
    void call() override
    {
        // IAsyncCallBack используется для возможности вызоыва и обработки в Коллерах СигналДелегата и АинкКолбека по
        // одному интерфеусу
        // у простых сигналов call не реализуется
        //		UnimplBreak();
    }

    /// интерфейс для сравнения колбеков
    bool isCallbackConnected(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s) override
    {
        return OneSlotSignalCore::isConnected(t, s);
    }
    /// интерфейс для сравнения колбеков
    virtual bool isCallbackConnected(cocos2d::CCInvocable* t) override
    {
        return OneSlotSignalCore::isConnected(t);
    }
    /// отписать все от колбека
    void clearCallback() override
    {
        OneSlotSignalCore::clear();
    }
    /// подписано ли что нибуть на колбек
    bool emptyCallback() override
    {
        return (OneSlotSignalCore::count() == 0);
    }
};

typedef SignalOne<> SignalOneV;
typedef SignalOne<const cocos2d::Color3B&> SignalOneColor;
typedef SignalOne<int> SignalOneI;
typedef SignalOne<void*> SignalOnePtr;
typedef SignalOne<cocos2d::Ref*> SignalOneO;
typedef SignalOne<const std::string&> SignalOneS;
