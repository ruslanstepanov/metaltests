//
//  SignalCore.h
//  HelloWorld
//
//  Created by a.arsentiev on 25.11.12.
//
//

#pragma once

#include "Global.h"
#include <list>
#include <map>

class SignalCore {
public:
    struct Method {
        cocos2d::CCInvocable* mTarget = nullptr;
        cocos2d::SEL_CallFunc mSel = nullptr;
        bool mRetain = false;

        typedef std::list<Method> List;
        Method(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s);
        Method(cocos2d::Ref* t, cocos2d::SEL_CallFunc s, bool retain);
        Method(const Method&);
        const Method& operator=(const Method&);
        ~Method();

        inline cocos2d::Ref* castRef()
        {
            return dynamic_cast<cocos2d::Ref*>(mTarget);
        }

        inline bool operator==(const Method& m) const
        {
            return mTarget == m.mTarget && mSel == m.mSel;
        }
        inline bool equals(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s) const
        {
            return mTarget == t && mSel == s;
        }
    };

    /// если число слотов привысило 16 то создадим кешь для того чтобы быстро определять не подключенные слоты
    /// объект, число коннектов
    typedef std::map<cocos2d::CCInvocable*, int> ConnectionCounterCache;

protected:
    ~SignalCore(); // destructor is protecter so we can create it not virtual
    SignalCore();

    /// методы подписанные на сигнал, может быть nullptr
    Method::List* mMethods = nullptr;
    mutable bool mIsEmiting = false;
    mutable ConnectionCounterCache* mConnectionsCache = nullptr;

    void connectRetain(cocos2d::Ref* t, cocos2d::SEL_CallFunc s);
    void connect(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s);
    void disconnect(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s);
    void disconnectTarget(cocos2d::CCInvocable* t);
    bool isConnected(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s);
    bool isConnected(cocos2d::CCInvocable*);

    void clear();

    /// создадим кешь для слотов
    void createConnectionsCache() const;
    /// геристируем объект слота в кеше
    void addToCache(cocos2d::CCInvocable* t) const;

    /// обходим кейс
    /// - сигнал вызывается onTick.emit()
    /// - отписываем в onTick.disconnect(<MotionController>, &MotionController::processMove);
    ///   - так как сигнал вызывается mMethodsToDisconnect <- (<MotionController>, &MotionController::processMove)
    ///     без удаления из mMethods
    /// - тут же подписываем его назад onTick.connect(this, &MotionController::processMove);
    ///   - isConnected срабатывает и "повторная" подпись не происходит
    /// - в конце вызова clearDisconnected удалит ::processMove
    void removeFromMethodsToDisconnect(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s);

    // to remove
private:
    mutable Method::List* mMethodsToDisconnect = nullptr;

protected:
    inline Method::List* getMethodsToDisconnect() const
    {
        if (!mMethodsToDisconnect) {
            mMethodsToDisconnect = new Method::List;
        }
        return mMethodsToDisconnect;
    }

    bool isDisconnected(const Method& m) const; // был ли метод отключен в результате прошллых вызовов текущего еммита
    void scheduleDisconnect(const Method& m) const; // запланировать отключенее, в текущемм еммите вызван уже не будет
    void clearDisconnected() const; // удалить все методы которые были запланированы к удалению

public:
    int count();
    /// методы подписанные на сигнал, может быть nullptr
    Method::List* getSlots()
    {
        return mMethods;
    };
};
