//
//  MultiSlotSignalCore.cpp
//  LostChapters
//
//  Created by a.arsentiev on 28.11.15.
//  Copyright © 2015 Crazy Bit. All rights reserved.
//

#include "MultiSlotSignalCore.hpp"

;

// _________________________________________________________________________________________
MultiSlotSignalCore::MultiSlotSignalCore()
{
    if (mIsEmiting && mMethods == nullptr) {
//        LogicError("memmory corrupted");
    }
}
// _________________________________________________________________________________________
MultiSlotSignalCore::~MultiSlotSignalCore()
{
//    IEAssert(!mIsEmiting, "trying to destruct emiting signal");

    delete mMethods;
    delete mMethodsToDisconnect;
}
// _________________________________________________________________________________________
void MultiSlotSignalCore::connect(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s, const char* parent)
{
    if (mIsEmiting && mMethods == nullptr) {
//        LogicError("memmory corrupted");
    }

    if (t == nullptr || s == nullptr)
        return;

    if (!mMethods)
        mMethods = new Method::Vec;

    mMethods->emplace_back(t, s, parent);
}

// _________________________________________________________________________________________
void MultiSlotSignalCore::disconnect(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s)
{
    if (!mMethods) {
        return;
    }

    for (auto it = mMethods->begin(); it != mMethods->end(); ++it) {
        const Method& m = *it;

        if (m.equals(t, s)) {
            if (mIsEmiting) {
                // сигнал еммитится, сделаем дисконнект позже
                scheduleDisconnect(m);
            }
            else {
                // сделаем дисконнект сечас
                mMethods->erase(it);
            }
            return;
        }
    }
}
// _________________________________________________________________________________________
void MultiSlotSignalCore::disconnectTarget(cocos2d::CCInvocable* t)
{
    if (!mMethods) {
        return;
    }

    for (auto it = mMethods->begin(); it != mMethods->end();) {
        const Method& m = *it;

        if (m.mTarget == t) {
            if (mIsEmiting) {
                // сигнал еммитится, сделаем дисконнект позже
                scheduleDisconnect(m);
            }
            else {
                // сделаем дисконнект сечас
                it = mMethods->erase(it);
                continue;
            }
        }

        ++it;
    }
}
// _________________________________________________________________________________________
bool MultiSlotSignalCore::isConnected(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s)
{
    if (!mMethods)
        return false;

    for (const Method& m : *mMethods) {
        if (m.equals(t, s))
            return true;
    }

    return false;
}
// _________________________________________________________________________________________
bool MultiSlotSignalCore::isConnected(cocos2d::CCInvocable* t)
{
    if (!mMethods)
        return false;

    for (const Method& m : *mMethods) {
        if (m.mTarget == t)
            return true;
    }

    return false;
}
// _________________________________________________________________________________________
int MultiSlotSignalCore::count()
{
    if (mMethods == nullptr)
        return 0;

    if (mMethods && mMethodsToDisconnect == nullptr)
        return (int)mMethods->size(); // FIXME: AUTO-FIX (fixed: 9)

    if (mMethods && mMethodsToDisconnect) {
        return (int)mMethods->size() - (int)mMethodsToDisconnect->size();
    }

    return 0;
}
// _________________________________________________________________________________________
#pragma mark disconnected manage
bool MultiSlotSignalCore::isDisconnected(const Method& m) const
{ // был ли метод отключен в результате прошллых вызовов текущего еммита

    if (mMethodsToDisconnect == nullptr || mMethodsToDisconnect->empty())
        return false;

    for (Method& disconnected : *getMethodsToDisconnect())
        if (m == disconnected)
            return true;

    return false;
}
// _________________________________________________________________________________________
void MultiSlotSignalCore::scheduleDisconnect(const Method& m) const
{ // запланировать отключенее, в текущемм еммите вызван уже не будет
    if (isDisconnected(m))
        return;

    getMethodsToDisconnect()->push_back(m);
}
// _________________________________________________________________________________________
void MultiSlotSignalCore::clearDisconnected() const
{ // удалить все методы которые были запланированы к удалению
    if (!mMethodsToDisconnect)
        return;

    if (mMethodsToDisconnect->empty())
        return;

    if (!mMethods) {
//        LogicError("methods must exist");
        return;
    }

    if (mIsEmiting) {
//        LogicError("can't clear while emiting");
        return;
    }

    for (Method& disconnected : *mMethodsToDisconnect) {
        for (auto it = mMethods->begin(); it != mMethods->end(); ++it) {
            const Method& m = *it;

            if (m == disconnected) {
                mMethods->erase(it);
                break;
            }
        }
    }

    mMethodsToDisconnect->clear();
}
// _________________________________________________________________________________________
void MultiSlotSignalCore::clear()
{
    if (mMethods == nullptr || mMethods->empty())
        return;

    if (!mIsEmiting) {
        delete mMethods;
        delete mMethodsToDisconnect;

        mMethods = nullptr;
        mMethodsToDisconnect = nullptr;
    }
    else {
        for (auto it = mMethods->begin(); it != mMethods->end(); ++it) {
            const Method& m = *it;
            scheduleDisconnect(m);
        }
    }
}
// _________________________________________________________________________________________
#pragma mark Method:
MultiSlotSignalCore::Method::Method(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s)
{
    mTarget = t;
    mSel = s;
}
// _________________________________________________________________________________________
MultiSlotSignalCore::Method::Method(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s, const char* parent)
{
    mTarget = t;
    mSel = s;
}
#pragma mark cache
