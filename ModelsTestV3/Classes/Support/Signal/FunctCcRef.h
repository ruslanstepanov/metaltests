//
//  FunctCcRef.h
//  Experiment
//
//  Created by Aleksandr on 09/01/2019.
//

#pragma once

template <typename... T_Args> class FunctCcRef : public cocos2d::Ref {
public: // static
    using MyType = FunctCcRef<T_Args...>;
    using Funct = std::function<void(T_Args...)>;

public: // methods:
    FunctCcRef(const Funct& f)
        : mF(f)
    {
    }

    void emit(T_Args... a) const
    {
        mF(a...);
    }

protected: // vars
    Funct mF;
};
