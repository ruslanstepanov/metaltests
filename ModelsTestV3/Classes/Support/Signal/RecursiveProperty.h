//
//  RecursiveProperty.h
//  MysteryTown
//
//  Created by a.arsentiev on 17.10.13.
//  Copyright (c) 2013 Crazy Bit. All rights reserved.
//
#pragma once

#include "TTSignal.h"
#include "ClassMethod.h"

// для данных свойств возможны вложенные присваивания
// при этом колбеки ( const ClassMethod& method ) будут вызыватся так будто свойства переключаются последовательно
// фвостовая рекурсия нивелируется

template <typename T_Tipe> class RecursivePropert {
    typedef std::vector<T_Tipe> VecType;

    T_Tipe mVal = T_Tipe();
    T_Tipe mPrev = T_Tipe();

    bool mIsLock = false;
    uint mRecLevel = 0;
    VecType mNextValVec;

    const SignalV onSet; // дабы никто не принял колбек сетера за сетер, уберем из него значение
public:
    Signal<T_Tipe> onChanged;

public:
    // _________________________________________________________________________________________
    RecursivePropert(const ClassMethod& method)
        : onSet(method){

        };
    // _________________________________________________________________________________________
    ~RecursivePropert()
    {
    }
// _________________________________________________________________________________________
#pragma mark access
    void set(const T_Tipe val)
    {
        // гарантируем последовательную смену состояий, даже при вложенных (рекурсивных вызовах)

        if (mIsLock) {
            mNextValVec.push_back(val);
            return;
        }

        mRecLevel++;

        // ___________ lock __________
        mIsLock = true;

        mPrev = mVal;
        mVal = val;

        onSet.emit();
        onChanged(mVal);

        mIsLock = false;
        // _________ unlock ___________

        if (mRecLevel == 1) {
            for (int i = 0; i < mNextValVec.size(); i++) {
                set(mNextValVec[i]);
            }
            mNextValVec.clear();
        }

        mRecLevel--;
    }
    // _________________________________________________________________________________________
    inline T_Tipe get()
    {
        return mVal;
    }
    // _________________________________________________________________________________________
    inline T_Tipe prev()
    {
        return mPrev;
    }
    // _________________________________________________________________________________________

    inline operator T_Tipe()
    {
        return get();
    }
};
