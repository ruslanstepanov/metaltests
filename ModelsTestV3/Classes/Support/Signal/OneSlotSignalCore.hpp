//
//  OneSlotSignalCore.hpp
//  LostChapters
//
//  Created by a.arsentiev on 30.11.15.
//  Copyright © 2015 Crazy Bit. All rights reserved.
//

#pragma once

#include "Global.h"

class OneSlotSignalCore {
public:
    struct Method {
        cocos2d::CCInvocable* mTarget = nullptr;
        cocos2d::SEL_CallFunc mSel = nullptr;

        typedef std::vector<Method> Vec;
        Method(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s);
        Method(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s, const char* parent);

        inline cocos2d::Ref* castRef()
        {
            return dynamic_cast<cocos2d::Ref*>(mTarget);
        }

        inline bool operator==(const Method& m) const
        {
            return mTarget == m.mTarget && mSel == m.mSel;
        }
        inline bool equals(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s) const
        {
            return mTarget == t && mSel == s;
        }
    };

protected:
    ~OneSlotSignalCore(); // destructor is protecter so we can create it not virtual
    OneSlotSignalCore();

    /// методы подписанные на сигнал, может быть nullptr
    Method mMethod;

    void connect(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s, const char* parent);
    void disconnect(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s);
    void disconnectTarget(cocos2d::CCInvocable* t);
    bool isConnected(cocos2d::CCInvocable* t, cocos2d::SEL_CallFunc s);
    bool isConnected(cocos2d::CCInvocable*);

    void clear();

    // to remove
private:
protected:
    /// был ли метод отключен в результате прошллых вызовов текущего еммита
    bool isDisconnected(const Method& m) const;
    /// удалить все методы которые были запланированы к удалению
    void clearDisconnected() const;

public:
    int count();
    /// методы подписанные на сигнал, может быть nullptr
};
