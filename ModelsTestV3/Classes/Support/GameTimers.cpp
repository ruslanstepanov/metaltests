//
//  GameTimer.cpp
//  MysteryTown
//
//  Created by a.arsentiev on 01.07.13.
//  Copyright (c) 2013 Crazy Bit. All rights reserved.
//

#include "GameTimers.h"
#include "AppDelegate.h"

#include "IELog.hpp"

// _________________________________________________________________________________________
#pragma mark base
GameTimers::GameTimers()
    : mLocalTime(0)
{
    //SApp()->onGameInstanceCleared.connect(this, &GameTimers::onGameInstanceCleared);
}
// _________________________________________________________________________________________
GameTimers::~GameTimers()
{
    //SApp()->onGameInstanceCleared.disconnect(this, &GameTimers::onGameInstanceCleared);
}
// _________________________________________________________________________________________
GameTimers* GameTimers::instanceMarkFunction(const char* callFromFunction)
{
    GameTimers* inst = Instance();
    if (inst)
        inst->mLastSingletonCall = callFromFunction;
    return inst;
}
// _________________________________________________________________________________________
#pragma mark step
void GameTimers::step()
{
    float dt = cocos2d::Director::getInstance()->getDeltaTime();
    if (dt > 1.0) {
        //		LogicErrorF("longStep dt = %lf", dt);
        dt = 1.0;
    }
    if (dt < 0.0f)
        dt = 0.0f;

    mLocalTime += dt;

    onTick();

    for (Timer* t : mTimers) {
        while (t->mNextCall < mLocalTime) {
            t->emit();
            t->mNextCall += t->mDt;
        }
    }

    checkMidnight();
}
// _________________________________________________________________________________________
#pragma mark tests
void GameTimers::onGameInstanceCleared()
{
    ie::log("GameTimers::onGameInstanceCleared()");

    std::string error = "++++++++++++++++++++++++++++\nobject thet run after relaunch: ";
    int slotCount = 0;

    for (Timer* t : mTimers) {
        auto* slotList = t->getSlots();

        if (slotList) {
            for (auto& slot : *slotList) {
                error += '\n';
                Ref* t = slot.castRef();
                if (t) {
                    ie::log(t->getClassName() + "::" + t->getObjName());
                    error += t->getClassName() + "::" + t->getObjName();
                }
                error += " from ";

                slotCount++;
            }
        }
    }

    int tickCount = onTick.slotCount();
    slotCount += tickCount;
    if (tickCount) {
        error += "\n and slots from onTick sgnal";
    }

    if (slotCount > 5) {
        // разрешены
        //		:: from GameLogo::GameLogo(float)
        //		:: from virtual void MainCaller::schedule()
        //		:: from void Server::onGameInstanceCreated()
        //		::GameLoading from GameLoading::GameLoading(bool));
        //		and slots from onTick sgnal ( GameSesion::onTick )
        //   если болше - утечка

        //LogicError(error);
    }
}
// _________________________________________________________________________________________
double GameTimers::timeTillCall(const Timer& t) const
{
    return t.mNextCall - mLocalTime;
}
// _________________________________________________________________________________________
GameTimers::Timer::Timer(double dt, const char** fromPtr)
{
    mDt = dt;
    mLastSingletonCall = fromPtr;
};
