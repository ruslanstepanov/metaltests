//
//  GameTimeController.cpp
//  LostChapters
//
//  Created by a.arsentiev on 27.09.15.
//  Copyright © 2015 Crazy Bit. All rights reserved.
//

#include "GameTimeController.hpp"
//#include "GameSession.h"
//#include <math.h>
//#include "AccountWrapper.h"
//#include "game/Account.pb.h"
//#include "Game.h"

// _________________________________________________________________________________________
void GameTimeController::initServerTime(time_t accountLastLogin) //, time_t accountMidnightOffset)
{
    mLastTime = accountLastLogin;
}
// _________________________________________________________________________________________
time_t GameTimeController::currentServerTime()
{
    //    if (SGameSession())
    //        return SGameSession()->getCurrentTime();

    //LogicError("SGameSession() == nullptr");
    return 0;
}
// _________________________________________________________________________________________
time_t GameTimeController::getNextServerMidnight()
{
    return getNextRelativeMidnight(currentServerTime());
}
// _________________________________________________________________________________________
bool GameTimeController::isToday(time_t time)
{
    return labs(getNextServerMidnight() - time) <= ONE_DAY;
}
// _________________________________________________________________________________________
bool GameTimeController::isSameDayOrLater(time_t checkTime, time_t nowTime)
{
    return getNextRelativeMidnight(nowTime) - checkTime <= ONE_DAY;
}
// _________________________________________________________________________________________
time_t GameTimeController::getNextRelativeMidnight(time_t nowTime)
{
    return 0;
    //    AccountWrapper* aw = SPbAccount();
    //    if (aw == nullptr || not aw->getAccount().has_midnight_offset())
    //        return 0;
    //
    //    IEAssert(aw->getAccount().has_midnight_offset(), "midnight_offset not specified");
    //    const time_t midnightOffset = aw->getAccount().has_midnight_offset() ? aw->getAccount().midnight_offset() : 0;
    //
    //    // Время начала сесии по серверному времени
    //    // Так как midnightOffset приходит тоже по серверному времени
    //    //	time_t timeT = mServerTime;
    //    time_t currentTime = nowTime;
    //    //	time_t realTime = currentTime + midnightOffset;
    //
    //    /// секунд с начала дня на сервере
    //    const unsigned currentDaySeconds = currentTime % ONE_DAY;
    //    /// во сколько начался день на сервере
    //    const time_t beginOfTheDay = currentTime - currentDaySeconds;
    //
    //    time_t midnightTime = beginOfTheDay + midnightOffset;
    //    if (currentTime < midnightTime)
    //        return midnightTime;
    //    else {
    //        time_t timeDeltaSec = currentTime - midnightTime;
    //        auto daysDelta = (timeDeltaSec + ONE_DAY) / ONE_DAY;
    //
    //        return midnightTime + daysDelta * ONE_DAY;
    //    }
}
// _________________________________________________________________________________________
time_t GameTimeController::getPreviousRelativeMidnight(time_t nowTime)
{
    return getNextRelativeMidnight(nowTime) - ONE_DAY;
}
// _________________________________________________________________________________________
void GameTimeController::checkMidnight()
{
    //    if (!Game::Instance()) {
    //        /// если нет игрового интстанса посылать событие не стоит
    //        // LogicError("SGame() == nullptr");
    //        return;
    //    }

    return;
    //    if (SPbAccount() == nullptr)
    //        return;

    if (!isToday(mLastTime)) {
        onMidnightTimer();
    }
    mLastTime = currentServerTime();
}
// _________________________________________________________________________________________
