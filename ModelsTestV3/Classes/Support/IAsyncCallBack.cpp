//
//  IAsyncCallBack.cpp
//  LostChapters
//
//  Created by a.arsentiev on 13.07.15.
//  Copyright (c) 2015 Crazy Bit. All rights reserved.
//

#include "IAsyncCallBack.h"

// _________________________________________________________________________________________
SimpleManaged::SimpleManaged()
{
}
// _________________________________________________________________________________________
SimpleManaged::~SimpleManaged()
{
}
// _________________________________________________________________________________________
void SimpleManaged::release(void)
{
    if (mReferenceCount <= 0) {
        //LogicError("reference count should greater than 0");
        return; // crash
    }

    --mReferenceCount;

    if (mReferenceCount == 0) {
        delete this;
    }
}
// _________________________________________________________________________________________
void SimpleManaged::retain(void)
{
    if (mReferenceCount <= 0) {
//        LogicError("reference count should greater than 0");
        return; // crash
    }

    ++mReferenceCount;
}
// _________________________________________________________________________________________
bool IAsyncCallBack::canEmit()
{
    return true;
}
// _________________________________________________________________________________________
