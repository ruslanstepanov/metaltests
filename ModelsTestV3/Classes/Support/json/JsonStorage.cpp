//
//  JsonStorage.cpp
//  LostChapters
//
//  Created by a.arsentiev on 08.09.14.
//  Copyright (c) 2014 Crazy Bit. All rights reserved.
//

#include "JsonStorage.h"
#include "JsonType.h"

#include <fstream>
#include <sstream>
#include <iostream>
#include <cstdio>
#include <errno.h>
//#include "easyzlib.h"

#include "StreamFileLoader.h"
//#include "Utils.h"
#include <zlib.h>
//#include "StringUtils.h"
#include "DebugUtils.h"
#include "FilePathUtils.h"
;

// _________________________________________________________________________________________
#pragma mark easy lib z
static int ezuncompress(unsigned char* pDest, long* pnDestLen, const unsigned char* pSrc, long nSrcLen)
{
    z_stream stream;
    int err;

    int nExtraChunks;
    uInt destlen;

    stream.next_in = (Bytef*)pSrc;
    stream.avail_in = (uInt)nSrcLen;
    /* Check for source > 64K on 16-bit machine: */
    if ((uLong)stream.avail_in != (uLong)nSrcLen)
        return Z_BUF_ERROR;

    destlen = (uInt)*pnDestLen;
    if ((uLong)destlen != (uLong)*pnDestLen)
        return Z_BUF_ERROR;

    stream.zalloc = (alloc_func)0;
    stream.zfree = (free_func)0;

    err = inflateInit(&stream);
    if (err != Z_OK)
        return err;

    nExtraChunks = 0;
    do {
        stream.next_out = pDest;
        stream.avail_out = destlen;
        err = inflate(&stream, Z_FINISH);
        if (err == Z_STREAM_END)
            break;
        if (err == Z_NEED_DICT || (err == Z_BUF_ERROR && stream.avail_in == 0))
            err = Z_DATA_ERROR;
        if (err != Z_BUF_ERROR) {
            inflateEnd(&stream);
            return err;
        }
        nExtraChunks += 1;
    } while (stream.avail_out == 0);

    *pnDestLen = stream.total_out;

    err = inflateEnd(&stream);
    if (err != Z_OK)
        return err;

    return nExtraChunks ? Z_BUF_ERROR : Z_OK;
}
// _________________________________________________________________________________________
/* easy zlib functions
 if destination buffer is not large enough, destination length set to required length
 */
int ezcompress(unsigned char* pDest, long* pnDestLen, const unsigned char* pSrc, long nSrcLen)
{
    z_stream stream;
    int err;

    int nExtraChunks;
    uInt destlen;

    stream.next_in = (Bytef*)pSrc;
    stream.avail_in = (uInt)nSrcLen;
#ifdef MAXSEG_64K
    /* Check for source > 64K on 16-bit machine: */
    if ((uLong)stream.avail_in != nSrcLen)
        return Z_BUF_ERROR;
#endif
    destlen = (uInt)*pnDestLen;
    if ((uLong)destlen != (uLong)*pnDestLen)
        return Z_BUF_ERROR;

    stream.zalloc = (alloc_func)0;
    stream.zfree = (free_func)0;
    stream.opaque = (voidpf)0;

    err = deflateInit(&stream, Z_DEFAULT_COMPRESSION);
    if (err != Z_OK)
        return err;

    nExtraChunks = 0;
    do {
        stream.next_out = pDest;
        stream.avail_out = destlen;
        err = deflate(&stream, Z_FINISH);
        if (err == Z_STREAM_END)
            break;
        if (err != Z_OK) {
            deflateEnd(&stream);
            return err;
        }
        nExtraChunks += 1;
    } while (stream.avail_out == 0);

    *pnDestLen = stream.total_out;

    err = deflateEnd(&stream);
    if (err != Z_OK)
        return err;

    return nExtraChunks ? Z_BUF_ERROR : Z_OK;
}
// _________________________________________________________________________________________
static const int EZ_BUF_ERROR = (-5);

// _________________________________________________________________________________________
#pragma mark declaration
static StreamFileLoader unzipSettings(std::istream& stream);
static void zipAndSaveSettings(std::stringstream& stream, const std::string& toFileName);
static bool checkSettingsCrc32(const unsigned char* buff, uint size);
unsigned int ucrc32(unsigned int* prevCrc, char symbol);

// _________________________________________________________________________________________
JsonStorage::JsonStorage(const std::string& fileName)
{
    mSettings = new JMap;
    mSavePath = cc::FileUtils::getInstance()->getWritablePath() + fileName;

    bool loaded = load(mSavePath);
    ie::log("loaded file %s: %d", mSavePath.c_str(), loaded);
    /*_cocos3_
        if (!loaded) {
            std::string path = fileUtils->getDocumentsPath() + fileName;
            loaded = load(path);
            ie::log("loaded file %s: %d", path.c_str(), loaded);
        }
    _cocos3_*/
    if (!loaded) {
        ie::log("Can't load file %s", fileName.c_str());
    }

    dump();
}
// _________________________________________________________________________________________
JsonStorage::~JsonStorage()
{
    delete mSettings;
    mSettings = nullptr;
}
// _________________________________________________________________________________________
bool JsonStorage::contains(const std::string& key) const
{
    return mSettings->Contains(key);
}
// _________________________________________________________________________________________
#pragma mark loading
bool JsonStorage::load(const std::string& path)
{
    std::string error;

    std::ifstream userSave;
    userSave.open(path.c_str());

    bool ok = false;
    ok = userSave.is_open();

    if (ok) {
        ok = loadFromStream(userSave, error, mSettings);
        userSave.close();
        if (!ok) {
            ie::log("can't load data from %s error \n%s", path.c_str(), error.c_str());
        }
    }

    return ok;
}
// _________________________________________________________________________________________
bool JsonStorage::loadFromStream(std::istream& istr, std::string& errorMsg, JMap* toMap)
{
    bool ok = false;
    bool compressed = true;
    bool isProtected = true;

    errorMsg = " ok";

    toMap->Clear();

    try {
        if (compressed) {
            StreamFileLoader unzipped = unzipSettings(istr);
            if (unzipped.getSize() == 0) {
                return false;
            }

            if (isProtected) {
                if (false
                    == checkSettingsCrc32(unzipped.getBuffer(),
                           (unsigned int)unzipped.getSize())) { // FIXME: AUTO-FIX (fixed: 59)
                    ie::debugMessage("[x] Crc32 check error");
                    errorMsg = " loadFromStream: Crc32 check error";
                    return false;
                }
            }

            json::Reader::Read(*toMap, unzipped.getStream());
        }
        else {
            istr.seekg(0, std::ios::beg);
            json::Reader::Read(*toMap, istr);
        }
        ok = true;
    }
    catch (json::Reader::ParseException& e) {
        // lines/offsets are zero-indexed, so bump them up by one for human presentation
        ie::log("Caught json::ParseException: %s\n\tLine/offset: %d/%d", e.mMessage.c_str(),
            e.m_locTokenBegin.m_nLine + 1, e.m_locTokenBegin.m_nLineOffset + 1);
        errorMsg = " loadFromStream: Caught json::ParseException";
    }
    catch (json::Reader::ScanException& e) {
        ie::log("Caught json::ScanException: %s\n\tLine/offset: %d/%d", e.mMessage.c_str(), e.m_locError.m_nLine + 1,
            e.m_locError.m_nLineOffset + 1);
        errorMsg = " loadFromStream: Caught json::ScanException";
    }
    catch (json::Exception& e) {
        ie::log("Caught json::Exception: %s", e.mMessage.c_str());
        errorMsg = " loadFromStream: Caught json::Exception";
    }

    return ok;
}
// _________________________________________________________________________________________
#pragma mark save
void JsonStorage::dump()
{
    // в json добавил мусор чтобы зипуемый файл был больше "вероятно существующего" минимального лимита если файл меньше
    // «лимита» он не грузится
    // arsentiev 23.10.2014 : 20.55
    (*mSettings)["qwertyuioplkjhgfdsazxcvbnm"]
        = JString("qwertyuiopasdfghjklzxcvbnmmnbvcxzlkjhgfdsapoiuytrewq1qaz2wsx3edc4rfv5tgb6yhn7ujm8ik,9ol.");
    std::stringstream strStream(std::stringstream::in | std::stringstream::out | std::stringstream::binary);

    // 1)
    ie::startDebugTimer("synchronize JsonStorage - write to buffer:  ");
    json::Writer::Write(*mSettings, strStream);
    ie::showDebugTimer();

    // 2)
    ie::startDebugTimer("synchronize JsonStorage - some sort of magic:  ");

    unsigned int crc32 = (unsigned int)~0;
    int counter = 0;
    int size = (int)strStream.tellp(); // FIXME: AUTO-FIX (fixed: 12)
    std::string data = strStream.str();
    while (counter < size) {
        char ch = data[counter++];
        crc32 = ucrc32(&crc32, ch);
    }
    ie::showDebugTimer();

    // 3)
    ie::startDebugTimer("synchronize JsonStorage - some other magic:  ");
    strStream.write((const char*)&crc32, sizeof(unsigned int));
    ie::showDebugTimer();

    // 4)
    ie::startDebugTimer("synchronize JsonStorage - zip and save:  ");
    zipAndSaveSettings(strStream, mSavePath);
    ie::showDebugTimer();
}

#pragma mark double op
// _________________________________________________________________________________________
double JsonStorage::getVal(const std::string& key)
{
    return mSettings->ExtractDouble(key);
}
// _________________________________________________________________________________________
void JsonStorage::setVal(const std::string& key, double val)
{
    (*mSettings)[key] = JNumber(val);
    dump();
}
// _________________________________________________________________________________________
void JsonStorage::setBool(const std::string& key, bool val)
{
    (*mSettings)[key] = JBool(val);
    dump();
}
// _________________________________________________________________________________________
bool JsonStorage::getBool(const std::string& key) const
{
    return mSettings->ExtractBool(key);
}
// _________________________________________________________________________________________
#pragma mark string op
const std::string& JsonStorage::getStr(const std::string& key)
{
    return mSettings->ExtractString(key);
}
// _________________________________________________________________________________________
void JsonStorage::setStr(const std::string& key, const std::string& val)
{
    (*mSettings)[key] = JString(val);
    dump();
}
#pragma mrk obj op
// _________________________________________________________________________________________
void JsonStorage::setObjectForKey(const std::string& key, const json::Object& object)
{
    (*mSettings)[key] = object;
    dump();
}
// _________________________________________________________________________________________
const json::Object& JsonStorage::objectForKey(const std::string& key) const
{
    auto& ell = (*mSettings)[key];
    if (ell.isObject())
        return ell;
    else {
        return JMapZero;
    }
}
// _________________________________________________________________________________________
void JsonStorage::setArrayForKey(const std::string& key, const json::Array& array)
{
    (*mSettings)[key] = array;
    dump();
}
// _________________________________________________________________________________________
const json::Array& JsonStorage::arrayForKey(const std::string& key) const
{
    auto& ell = (*mSettings)[key];
    if (ell.isArray()) {
        return ell;
    }
    else {
        return JArrayZero;
    }
}
// _________________________________________________________________________________________
void JsonStorage::clear()
{
    mSettings->Clear();
    std::remove(mSavePath.c_str());
}
// _________________________________________________________________________________________
#pragma mark support
static StreamFileLoader unzipSettings(std::istream& stream)
{
    stream.seekg(0, std::ios::end);
    int streamLength = (int)stream.tellg(); // FIXME: AUTO-FIX (fixed: 20)
    stream.seekg(0, std::ios::beg);

    IEAssert(streamLength > 10, "bad stream size");
    if (streamLength <= 10) {
        return StreamFileLoader();
    }

    unsigned char* data = new unsigned char[streamLength];
    stream.read((char*)data, streamLength);

    long unzippedLength = streamLength;
    unsigned char* dest = NULL;
    int error = 0;
    while (1) {
        // первый раз будет создан буфер не правильного размера
        dest = new unsigned char[unzippedLength];
        error = ezuncompress(dest, &unzippedLength, data, streamLength);
        if (error == EZ_BUF_ERROR) {
            // теперь мы знаем правильный размер буфера повторим еще раз
            delete[] dest;
        }
        else {
            break;
        }
    }
    delete[] data;
    IEAssert(error == 0, "decompression error")

        return StreamFileLoader::grabBuffer(dest, unzippedLength);
}
// _________________________________________________________________________________________
static void zipAndSaveSettings(std::stringstream& stream, const std::string& toFileName)
{
    FilePathUtils::createDirectoryForFile(toFileName);

    std::fstream file;
    file.open(toFileName.c_str(), std::ios::out | std::ios::in | std::ios::trunc);

    if (file.good()) {
        stream.seekg(0, std::ios::end);
        int fileLength = (int)stream.tellg(); // FIXME: AUTO-FIX (fixed: 19)
        stream.seekg(0, std::ios::beg);

        unsigned char* destBuf = new unsigned char[fileLength];

        unsigned char* dataBuf = new unsigned char[fileLength];
        stream.read((char*)dataBuf, fileLength);
        stream.seekg(0, std::ios::beg);

        long destBufLen = fileLength;
        ezcompress(destBuf, &destBufLen, dataBuf, fileLength);
        file.write((const char*)destBuf, destBufLen);

        file.close();

        delete[] destBuf;
        delete[] dataBuf;
    }
    else {
        ie::debugMessage(
            "JsonStorage::zipAndSaveSettings : file.open( %s, ...); %s", toFileName.c_str(), strerror(errno));
        LogicError("file.good() == false\n JsonStorage::zipAndSaveSettings : file.open( sSaveName.c_str(), ...); ");
    }
}
// _________________________________________________________________________________________
static bool checkSettingsCrc32(const unsigned char* buff, uint size)
{
    if (size <= sizeof(uint))
        return false;

    const unsigned char* savedCrc32Ptr = buff + size - sizeof(uint);

    bool retValue = false;
    uint savedCrc32 = *reinterpret_cast<const uint*>(savedCrc32Ptr);

    unsigned int crc32 = (unsigned int)~0;
    while (buff < savedCrc32Ptr) {
        char ch = *buff;
        crc32 = ucrc32(&crc32, ch);
        buff++;
    }

    if (crc32 == savedCrc32) {
        retValue = true;
    }

    return retValue;
}
// _________________________________________________________________________________________
unsigned int ucrc32(unsigned int* prevCrc, char symbol)
{
    static const unsigned int kCrc32Table[256] = {
        0x00000000, 0x77073096, 0xee0e612c, 0x990951ba, 0x076dc419, 0x706af48f, 0xe963a535, 0x9e6495a3, 0x0edb8832,
        0x79dcb8a4, 0xe0d5e91e, 0x97d2d988, 0x09b64c2b, 0x7eb17cbd, 0xe7b82d07, 0x90bf1d91, 0x1db71064, 0x6ab020f2,
        0xf3b97148, 0x84be41de, 0x1adad47d, 0x6ddde4eb, 0xf4d4b551, 0x83d385c7, 0x136c9856, 0x646ba8c0, 0xfd62f97a,
        0x8a65c9ec, 0x14015c4f, 0x63066cd9, 0xfa0f3d63, 0x8d080df5, 0x3b6e20c8, 0x4c69105e, 0xd56041e4, 0xa2677172,
        0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b, 0x35b5a8fa, 0x42b2986c, 0xdbbbc9d6, 0xacbcf940, 0x32d86ce3,
        0x45df5c75, 0xdcd60dcf, 0xabd13d59, 0x26d930ac, 0x51de003a, 0xc8d75180, 0xbfd06116, 0x21b4f4b5, 0x56b3c423,
        0xcfba9599, 0xb8bda50f, 0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924, 0x2f6f7c87, 0x58684c11, 0xc1611dab,
        0xb6662d3d, 0x76dc4190, 0x01db7106, 0x98d220bc, 0xefd5102a, 0x71b18589, 0x06b6b51f, 0x9fbfe4a5, 0xe8b8d433,
        0x7807c9a2, 0x0f00f934, 0x9609a88e, 0xe10e9818, 0x7f6a0dbb, 0x086d3d2d, 0x91646c97, 0xe6635c01, 0x6b6b51f4,
        0x1c6c6162, 0x856530d8, 0xf262004e, 0x6c0695ed, 0x1b01a57b, 0x8208f4c1, 0xf50fc457, 0x65b0d9c6, 0x12b7e950,
        0x8bbeb8ea, 0xfcb9887c, 0x62dd1ddf, 0x15da2d49, 0x8cd37cf3, 0xfbd44c65, 0x4db26158, 0x3ab551ce, 0xa3bc0074,
        0xd4bb30e2, 0x4adfa541, 0x3dd895d7, 0xa4d1c46d, 0xd3d6f4fb, 0x4369e96a, 0x346ed9fc, 0xad678846, 0xda60b8d0,
        0x44042d73, 0x33031de5, 0xaa0a4c5f, 0xdd0d7cc9, 0x5005713c, 0x270241aa, 0xbe0b1010, 0xc90c2086, 0x5768b525,
        0x206f85b3, 0xb966d409, 0xce61e49f, 0x5edef90e, 0x29d9c998, 0xb0d09822, 0xc7d7a8b4, 0x59b33d17, 0x2eb40d81,
        0xb7bd5c3b, 0xc0ba6cad, 0xedb88320, 0x9abfb3b6, 0x03b6e20c, 0x74b1d29a, 0xead54739, 0x9dd277af, 0x04db2615,
        0x73dc1683, 0xe3630b12, 0x94643b84, 0x0d6d6a3e, 0x7a6a5aa8, 0xe40ecf0b, 0x9309ff9d, 0x0a00ae27, 0x7d079eb1,
        0xf00f9344, 0x8708a3d2, 0x1e01f268, 0x6906c2fe, 0xf762575d, 0x806567cb, 0x196c3671, 0x6e6b06e7, 0xfed41b76,
        0x89d32be0, 0x10da7a5a, 0x67dd4acc, 0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5, 0xd6d6a3e8, 0xa1d1937e,
        0x38d8c2c4, 0x4fdff252, 0xd1bb67f1, 0xa6bc5767, 0x3fb506dd, 0x48b2364b, 0xd80d2bda, 0xaf0a1b4c, 0x36034af6,
        0x41047a60, 0xdf60efc3, 0xa867df55, 0x316e8eef, 0x4669be79, 0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236,
        0xcc0c7795, 0xbb0b4703, 0x220216b9, 0x5505262f, 0xc5ba3bbe, 0xb2bd0b28, 0x2bb45a92, 0x5cb36a04, 0xc2d7ffa7,
        0xb5d0cf31, 0x2cd99e8b, 0x5bdeae1d, 0x9b64c2b0, 0xec63f226, 0x756aa39c, 0x026d930a, 0x9c0906a9, 0xeb0e363f,
        0x72076785, 0x05005713, 0x95bf4a82, 0xe2b87a14, 0x7bb12bae, 0x0cb61b38, 0x92d28e9b, 0xe5d5be0d, 0x7cdcefb7,
        0x0bdbdf21, 0x86d3d2d4, 0xf1d4e242, 0x68ddb3f8, 0x1fda836e, 0x81be16cd, 0xf6b9265b, 0x6fb077e1, 0x18b74777,
        0x88085ae6, 0xff0f6a70, 0x66063bca, 0x11010b5c, 0x8f659eff, 0xf862ae69, 0x616bffd3, 0x166ccf45, 0xa00ae278,
        0xd70dd2ee, 0x4e048354, 0x3903b3c2, 0xa7672661, 0xd06016f7, 0x4969474d, 0x3e6e77db, 0xaed16a4a, 0xd9d65adc,
        0x40df0b66, 0x37d83bf0, 0xa9bcae53, 0xdebb9ec5, 0x47b2cf7f, 0x30b5ffe9, 0xbdbdf21c, 0xcabac28a, 0x53b39330,
        0x24b4a3a6, 0xbad03605, 0xcdd70693, 0x54de5729, 0x23d967bf, 0xb3667a2e, 0xc4614ab8, 0x5d681b02, 0x2a6f2b94,
        0xb40bbe37, 0xc30c8ea1, 0x5a05df1b, 0x2d02ef8d,
    }; // kCrc32Table

    return (*prevCrc >> 8) ^ kCrc32Table[(*prevCrc ^ symbol) & 0xFF];
}
// _________________________________________________________________________________________
