//
//  JsonPre.h
//  LostChapters
//
//  Created by a.arsentiev on 26.04.15.
//  Copyright (c) 2015 Crazy Bit. All rights reserved.
//

#pragma once

namespace json {
class Object;
class Array;
class UnknownElement;
} // namespace json

typedef json::Object JMap;
typedef json::Array JArray;
typedef json::UnknownElement JElement;

namespace rapidjson {
class CrtAllocator;
template <typename> class MemoryPoolAllocator;
template <typename> struct UTF8;
template <typename, typename> class GenericValue;
template <typename, typename, typename> class GenericDocument;
template <bool, typename> class GenericArray;

typedef GenericValue<UTF8<char>, MemoryPoolAllocator<CrtAllocator>> Value;
typedef GenericDocument<UTF8<char>, MemoryPoolAllocator<CrtAllocator>, CrtAllocator> Document;
typedef GenericArray<true, Value> ConstArray;
typedef GenericArray<false, Value> Array;
} // namespace rapidjson
