//
//  JsonType.h
//  LostChapters
//
//  Created by a.arsentiev on 06.03.14.
//  Copyright (c) 2014 Crazy Bit. All rights reserved.
//

#pragma once

#include "JsonPre.h"

#include "reader.h"
#include "writer.h"

extern JMap JMapZero;
extern JArray JArrayZero;

typedef json::Number JNumber;
typedef json::String JString;
typedef json::Boolean JBool;

#define toJMap(e) ((JMap)(e))
#define toJArr(e) ((JArray)(e))
#define toJNum(e) ((JNumber)(e))
#define toJStr(e) ((JString)(e))
#define toJBool(e) ((JBool)(e))

#define jtoNum(e) (e.getDouble())
#define jtoInt(e) (e.getDouble())
#define jtoStr(e) (e.getStdString())
#define jtoBool(e) (e.getBool())
