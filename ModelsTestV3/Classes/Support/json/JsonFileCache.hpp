//
//  GuiConfigJsonCache.hpp
//  Game
//
//  Created by a.arsentiev on 04.04.16.
//  Copyright © 2016 Crazy Bit. All rights reserved.
//

#pragma once

//#include "GameInstance.h"
#include "cocos2d.h"
#include "JsonPre.h"
#include <memory>

/// кешик загруженных и распарсенных JSON, инстанс сессии
class JsonFileCache : public cocos2d::Ref { //, public GameInstance<JsonFileCache> {
public:
    JsonFileCache();
    ~JsonFileCache() override;
    /// сброить кешь
    void clear();

    const JMap* getJMap(const std::string& fileName);
    const JArray* getJArr(const std::string& fileName);

protected: // vars
    /// кешт JMap по имени файла
    std::map<std::string, std::unique_ptr<JMap>> mMaps;
    /// кешт JArray по имени файла
    std::map<std::string, std::unique_ptr<JArray>> mArrs;
};

//extern inline JsonFileCache* SJsonFileCache()
//{
//    return JsonFileCache::Instance();
//}
