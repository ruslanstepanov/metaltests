//
//  JsonStorage.h
//  LostChapters
//
//  Created by a.arsentiev on 08.09.14.
//  Copyright (c) 2014 Crazy Bit. All rights reserved.
//

#pragma once

#include "JsonType.h"

/// настройки привязанные к девайсу
/// остаются на девайсе не передаются на сервер
class JsonStorage {
//    _IMPLEMENT_RTTI_

public: // methods
    JsonStorage(const std::string& fileName);
    virtual ~JsonStorage();

    /// узнать есть ли такой элемент
    bool contains(const std::string& key) const;
    /// получить double
    double getVal(const std::string& key);
    /// записать double
    void setVal(const std::string& key, double val);
    /// записать булев флаг
    void setBool(const std::string& key, bool val);
    /// получить булев флаг
    bool getBool(const std::string& key) const;
    /// получить double
    const std::string& getStr(const std::string& key);
    /// записать double
    void setStr(const std::string& key, const std::string& val);
    /// Задать задать объект для ключа
    void setObjectForKey(const std::string& key, const json::Object& object);
    /// Получить объект по ключу
    const json::Object& objectForKey(const std::string& key) const;
    /// Задать задать массив для ключа
    void setArrayForKey(const std::string& key, const json::Array& array);
    /// Получить массив по ключу
    const json::Array& arrayForKey(const std::string& key) const;

    /// очистить сторедж
    virtual void clear();
    /// сдампить сетинги в файл, НЕ равносильно сохранению игры так как при сохранении
    void dump();

    inline JMap* settings() const;

protected: // methods
    /// загрузить данные из файлика в mSettings;
    bool load(const std::string& path);
    /// загрузить сжатый и защищенный crc32 json из потока
    bool loadFromStream(std::istream& istr, std::string& errorMsg, JMap* toMap);

protected: // vars
    /// json объект в котором лежит все
    JMap* mSettings = nullptr;
    /// имя файлика в который мы все дампим
    std::string mSavePath;
};
JMap* JsonStorage::settings() const
{
    return mSettings;
}
