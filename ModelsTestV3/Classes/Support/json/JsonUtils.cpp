
//  JsonUtils.cpp
//  HelloWorld
//
//  Created by a.arsentiev on 21.12.12.
//
//

#include "JsonUtils.h"
#include "StreamFileLoader.h"
#include "StringUtils.h"
#include "StringFormatUtils.h"
#include "DebugUtils.h"
#include "CBSON.h"
#include "JsonType.h"
#include "json/rapidjson.h"
#include "json/document-wrapper.h"
#include "json/stringbuffer.h"
#include "json/writer.h"
#include "Items.h"

;

namespace JsonUtils {
extern bool boolFromJMap(const JMap& map, const std::string& key, bool defVal)
{
    auto it = map.Find(key);

    if (it != map.End())
        return jtoBool(it->element);
    else
        return defVal;
}
// _________________________________________________________________________________________
extern int intFromJMap(const JMap& map, const std::string& key, int defVal)
{
    auto it = map.Find(key);

    if (it != map.End())
        return jtoInt(it->element);
    else
        return defVal;
}
// _________________________________________________________________________________________
extern double doubleFromJMap(const JMap& map, const std::string& key, double defVal)
{
    return map.ExtractDouble(key);
}
// _________________________________________________________________________________________
extern std::string& stringFromJMap(const JMap& map, const std::string& key, std::string& buff)
{
    buff.clear();
    auto it = map.Find(key);

    if (it != map.End())
        buff = jtoStr(it->element);

    return buff;
}
// _________________________________________________________________________________________
extern IntVec& intVecFromJMap(const JMap& map, const std::string& key, IntVec& buff)
{
    buff.clear();
    auto it = map.Find(key);

    if (it != map.End()) {
        const JArray& arr = toJArr(it->element);
        int count = (int)arr.Size(); // FIXME: AUTO-FIX (fixed: 15)
        for (int i = 0; i < count; i++) {
            buff.push_back(jtoInt(arr[i]));
        }
    }

    return buff;
}
// _________________________________________________________________________________________
extern StringVec& stringVecFromJArray(const JArray& from, StringVec& buff)
{
    buff.clear();

    int count = (int)from.Size(); // FIXME: AUTO-FIX (fixed: 15)
    for (int i = 0; i < count; i++) {
        buff.push_back(jtoStr(from[i]));
    }

    return buff;
}
// _________________________________________________________________________________________
extern JArray& jArrayFromStringVec(const StringVec& from, JArray& to)
{
    to.Clear();

    for (auto& s : from) {
        to.Insert(JString(s));
    }
    return to;
}
// _________________________________________________________________________________________
extern JMap& jMapFromItems(const Items& items, JMap& to)
{
    to.Clear();

    Items all;
    all.combine(items);

    for (Item& it : all) {
        to[uID::decodeTag(it.getPid())] = JNumber(it.getCount());
    }

    return to;
}
// _________________________________________________________________________________________
extern cc::Vec2& ccpFromJMap(const JMap& map, const std::string& key, cc::Vec2& buff)
{
    buff.x = buff.y = 0.0f;

    auto it = map.Find(key);

    if (it != map.End()) {
        const JArray& arr = toJArr(it->element);
        int count = (int)arr.Size(); // FIXME: AUTO-FIX (fixed: 15)
        if (count == 2) {
            buff.x = jtoNum(arr[0]);
            buff.y = jtoNum(arr[1]);
        }
    }

    return buff;
}
// _________________________________________________________________________________________
extern FloatVecVec& floatVecVecFromJMap(const JMap& map, const std::string& key, FloatVecVec& buff)
{
    buff.clear();
    auto it = map.Find(key);

    if (it != map.End()) {
        const JArray& arr = toJArr(it->element);
        int count = (int)arr.Size(); // FIXME: AUTO-FIX (fixed: 15)
        for (int i = 0; i < count; ++i) {
            FloatVec vec;

            const JArray& innerArr = toJArr(arr[i]);
            int innerCount = (int)innerArr.Size(); // FIXME: AUTO-FIX (fixed: 21)
            for (int j = 0; j < innerCount; ++j) {
                vec.push_back((float)(jtoNum(innerArr[j])));
            }

            buff.push_back(vec);
        }
    }
    return buff;
}
// _________________________________________________________________________________________
extern IntVecVec& intVecVecFromJMap(const JMap& map, const std::string& key, IntVecVec& buff)
{
    buff.clear();
    auto it = map.Find(key);

    if (it != map.End()) {
        const JArray& arr = toJArr(it->element);
        int count = (int)arr.Size(); // FIXME: AUTO-FIX (fixed: 15)
        for (int i = 0; i < count; ++i) {
            IntVec vec;

            const JArray& innerArr = toJArr(arr[i]);
            int innerCount = (int)innerArr.Size(); // FIXME: AUTO-FIX (fixed: 21)
            for (int j = 0; j < innerCount; ++j) {
                vec.push_back((int)(jtoNum(innerArr[j])));
            }

            buff.push_back(vec);
        }
    }
    return buff;
}
// _________________________________________________________________________________________
extern std::string getString(const JElement& element)
{
    std::string result;

    if (element.isBoolean()) {
        char tmp[512] = { 0 };
        ie::itoa(element.toBoolean(), tmp, 10);
        result = tmp;
    }
    else if (element.isNumber()) {
        char tmp[512] = { 0 };
        snprintf(tmp, 511, "%.0lf", element.getDouble());
        result = tmp;
    }
    else if (element.isNull() || element.isObject()) {
        result = StringZero;
    }
    else if (element.isString()) {
        result = element.toString();
    }

    return result;
}
#pragma mark extract items
// _________________________________________________________________________________________
extern Items& parseItems(const JElement& elem, Items& items)
{ /*_coocs3_
     IEAssert(!items.getCount(), "trying to rewrite items container");
     // впринципе это не запрещено, но логичиски этот метод инициализирует а не дописывает

     if (!elem.isArray())
         return items;

     const JArray& array = elem;

     try {
         for (auto itemJ = array.Begin(); itemJ != array.End(); ++itemJ) {
             Item item;
             parseItem(*itemJ, item);

             if (!item.isNone())
                 items.push(item);
         }
     }
     catch (json::Exception& e) {
         ie::debugMessageImportant(e.mMessage.c_str());
     }
     return items;
 _cocos3_*/
}
// _________________________________________________________________________________________
extern Items& parseItems(const rapidjson::Value& elem, Items& items)
{ /*_coocs3_
     IEAssert(!items.getCount(), "trying to rewrite items container");
     // впринципе это не запрещено, но логичиски этот метод инициализирует а не дописывает

     if (!elem.isArray())
         return items;

     const auto& array = elem.toArray();

     try {
         for (auto itemJ = array.Begin(); itemJ != array.End(); ++itemJ) {
             Item item;
             parseItem(*itemJ, item);

             if (!item.isNone())
                 items.push(item);
         }
     }
     catch (json::Exception& e) {
         ie::debugMessageImportant(e.mMessage.c_str());
     }
     return items;
 _cocos3_*/
}
// _________________________________________________________________________________________
extern Item& parseItem(const JElement& elem, Item& item)
{ /*_coocs3_
     if (!elem.isObject())
         return item;

     const JMap& map = elem;

     try {
         int itemId = 0;
         if (map["item_id"].isNumber())
             itemId = map.ExtractInt("item_id");
         if (map["item_id"].isString()) {
             std::string str = map.ExtractString("item_id");
             itemId = uID::strToTag(str.c_str());
         }

         int count = map.ExtractInt("count");
 #warning USER_WARNING: плохой item, нет склада
         item = Item();
         item.setId(itemId);
         item.setCount(count);
     }
     catch (json::Exception& e) {
         ie::debugMessageImportant(e.mMessage.c_str());
     }

     return item;
 _cocos3_*/
}
// _________________________________________________________________________________________
extern Item& parseItem(const rapidjson::Value& elem, Item& item)
{ /*_coocs3_
     if (!elem.isObject())
         return item;

     const auto& map = elem.toObject();

     try {
         int itemId = 0;
         if (map["item_id"].isNumber())
             itemId = map.ExtractInt("item_id");
         if (map["item_id"].isString()) {
             std::string str = map.ExtractString("item_id");
             itemId = uID::strToTag(str.c_str());
         }

         int count = map.ExtractInt("count");
 #warning USER_WARNING: плохой item, нет склада
         item = Item();
         item.setId(itemId);
         item.setCount(count);
     }
     catch (json::Exception& e) {
         ie::debugMessageImportant(e.mMessage.c_str());
     }

     return item;
 _cocos3_*/
}
// _________________________________________________________________________________________
extern bool loadFromStream(JMap& obj, std::istream& istr)
{
    obj.Clear();

    bool ok = false;
    try {
        json::Reader::Read(obj, istr);
        ok = true;
    }
    catch (json::Reader::ParseException& e) {
        // lines/offsets are zero-indexed, so bump them up by one for human presentation
        ie::log("Caught json::ParseException: %s\n\tLine/offset: %d/%d", e.mMessage.c_str(),
            e.m_locTokenBegin.m_nLine + 1, e.m_locTokenBegin.m_nLineOffset + 1);
    }
    catch (json::Reader::ScanException& e) {
        ie::log("Caught json::ScanException: %s\n\tLine/offset: %d/%d", e.mMessage.c_str(), e.m_locError.m_nLine + 1,
            e.m_locError.m_nLineOffset + 1);
    }
    catch (json::Exception& e) {
        ie::log("Caught json::Exception: %s", e.mMessage.c_str());
    }

    if (!ok) {
        obj.Clear();
    }

    // ie::log("loaded : %s", ok ? "true" : "false");

    return ok;
}
// _________________________________________________________________________________________
extern bool loadFromStream(JArray& obj, std::istream& istr)
{
    obj.Clear();

    bool ok = false;
    try {
        json::Reader::Read(obj, istr);
        ok = true;
    }
    catch (json::Reader::ParseException& e) {
        // lines/offsets are zero-indexed, so bump them up by one for human presentation
        ie::log("Caught json::ParseException: %s\n\tLine/offset: %d/%d", e.mMessage.c_str(),
            e.m_locTokenBegin.m_nLine + 1, e.m_locTokenBegin.m_nLineOffset + 1);
    }
    catch (json::Reader::ScanException& e) {
        ie::log("Caught json::ScanException: %s\n\tLine/offset: %d/%d", e.mMessage.c_str(), e.m_locError.m_nLine + 1,
            e.m_locError.m_nLineOffset + 1);
    }
    catch (json::Exception& e) {
        ie::log("Caught json::Exception: %s", e.mMessage.c_str());
    }

    if (!ok) {
        obj.Clear();
    }

    ie::log("loaded : %s", ok ? "true" : "false");

    return ok;
}
// _________________________________________________________________________________________
bool loadFromFile(JArray& jarray, const std::string& path)
{
    ie::log("JsonUtils::loadFromFile(%s)", path.c_str());

    StreamFileLoader file(path);

    if (file.isOk()) {
        return loadFromStream(jarray, file.getStream());
    }

    return false;
}
// _________________________________________________________________________________________
bool loadFromFile(JMap& jmap, const std::string& path)
{
    ie::log("JsonUtils::loadFromFile(%s)", path.c_str());

    StreamFileLoader file(path);

    if (file.isOk()) {
        return loadFromStream(jmap, file.getStream());
    }

    return false;
}
// _________________________________________________________________________________________
bool loadFromFile(rapidjson::Document& doc, const std::string& path)
{
    bool check = false;
    ssize_t size = 0;
    auto* buff = cc::FileUtils::getInstance()->getFileData(path.c_str(), "r", &size);
    if (buff) {
        try {
            check = rapidjson::ParseResult(doc.Parse((const char*)buff, size));
        }
        catch (...) {
            ie::log("Error parse rapidjson in loadFromStream");
        }
        delete buff;
    }

    if (not check) {
        doc.SetObject();
    }

    return check;
}
// _________________________________________________________________________________________
extern void mergeJElement(JElement& to, const JElement& from)
{
    if (to.isNull() or (to.isNumber() && from.isNumber()) or (to.isString() && from.isString())
        or (to.isBoolean() && from.isBoolean()))
        to = from;
    else if (to.isObject() && from.isObject())
        JsonUtils::mergeJMap(static_cast<JMap&>(to), from.toObject());
    else if (to.isArray() && from.isArray())
        JsonUtils::mergeJArray(static_cast<JArray&>(to), from.toArray());
}
// _________________________________________________________________________________________
extern void mergeJMap(JMap& to, const JMap& from)
{
    for (const auto& fromIt : from) {
        auto toIt = to.Find(fromIt.name);
        if (toIt == to.End())
            to.Insert(fromIt);
        else
            mergeJElement(toIt->element, fromIt.element);
    }
}
// _________________________________________________________________________________________
extern void mergeJArray(JArray& to, const JArray& from)
{
    auto fromSize = from.Size();
    if (to.Size() < fromSize)
        to.Resize(fromSize);
    auto toIt = to.Begin();
    for (const auto& fromIt : from) {
        mergeJElement(*toIt, fromIt);
        ++toIt;
    }
}
// _________________________________________________________________________________________
extern bool deserializeJSON(const char* str, JMap& obj)
{
    if (!isJMap(str))
        return false;

    obj.Clear();

    bool ok = false;
    try {
        std::istringstream stream(str);
        json::Reader::Read(obj, stream);
        ok = true;
    }
    catch (json::Reader::ParseException& e) {
        // lines/offsets are zero-indexed, so bump them up by one for human presentation
        ie::log("Caught json::ParseException: %s\n\tLine/offset: %d/%d", e.mMessage.c_str(),
            e.m_locTokenBegin.m_nLine + 1, e.m_locTokenBegin.m_nLineOffset + 1);
    }
    catch (json::Reader::ScanException& e) {
        ie::log("Caught json::ScanException: %s\n\tLine/offset: %d/%d", e.mMessage.c_str(), e.m_locError.m_nLine + 1,
            e.m_locError.m_nLineOffset + 1);
    }
    catch (json::Exception& e) {
        ie::log("Caught json::Exception: %s", e.mMessage.c_str());
    }

    if (!ok) {
        obj.Clear();
    }

    return ok;
}
// _________________________________________________________________________________________
extern bool deserializeJSON(const char* str, JArray& obj)
{
    if (!isJArray(str))
        return false;

    obj.Clear();

    bool ok = false;
    try {
        std::istringstream stream(str);
        json::Reader::Read(obj, stream);
        ok = true;
    }
    catch (json::Reader::ParseException& e) {
        // lines/offsets are zero-indexed, so bump them up by one for human presentation
        ie::log("Caught json::ParseException: %s\n\tLine/offset: %d/%d", e.mMessage.c_str(),
            e.m_locTokenBegin.m_nLine + 1, e.m_locTokenBegin.m_nLineOffset + 1);
    }
    catch (json::Reader::ScanException& e) {
        ie::log("Caught json::ScanException: %s\n\tLine/offset: %d/%d", e.mMessage.c_str(), e.m_locError.m_nLine + 1,
            e.m_locError.m_nLineOffset + 1);
    }
    catch (json::Exception& e) {
        ie::log("Caught json::Exception: %s", e.mMessage.c_str());
    }

    if (!ok) {
        obj.Clear();
    }

    return ok;
}
// _________________________________________________________________________________________
extern bool isJMap(const char* str)
{
    if (ie::getFirstNotSpaceSymbol(str) == '{')
        return true;
    else
        return false;
}
// _________________________________________________________________________________________
extern bool isJArray(const char* str)
{
    if (ie::getFirstNotSpaceSymbol(str) == '[')
        return true;
    else
        return false;
}
// _________________________________________________________________________________________
extern bool isJString(const char* str)
{
    if (ie::getFirstNotSpaceSymbol(str) == '\"')
        return true;
    else
        return false;
}
// _________________________________________________________________________________________
extern bool isJNum(const char* str)
{
    char c = ie::getFirstNotSpaceSymbol(str);
    const static char* sNumericSymbols = "1234567890-.";

    if (strchr(sNumericSymbols, c))
        return true;
    else
        return false;
}
// _________________________________________________________________________________________
extern std::string& prepareStrForJson(std::string& str)
{
    for (char& c : str)
        if (c == '<' || c == '>')
            c = '"';

    return str;
}
// _________________________________________________________________________________________
} // namespace JsonUtils
// ==========================================================================================

namespace CbsonUtils {
#pragma mark cbson
using namespace cbson;
extern Items& parseItems(const CBArray& elem, Items& items)
{ /*_coocs3_
     IEAssert(!items.getCount(), "trying to rewrite items container");
     // впринципе это не запрещено, но логичиски этот метод инициализирует а не дописывает

     for (const CBMap& map : elem) {
         Item item;
         parseItem(map, item);
         if (!item.isNone())
             items.push(item);
     }

     return items;
 _coocs3_*/
}
// _________________________________________________________________________________________
extern Item& parseItem(const cbson::CBMap& map, Item& item)
{ /*_coocs3_
     int itemId = uID::strToTag(map["item_id"].toString());
     int count = map["count"].toDouble();
     item = Item();
     item.setId(itemId);
     item.setCount(count);
 #warning USER_WARNING: плохой item, нет склада
     return item;
 _coocs3_*/
}
// _________________________________________________________________________________________
extern cc::Vec2& parsePoint(const cbson::CBMap& map, const std::string& key, cc::Vec2& buff)
{
    const CBArray& array = map[key];
    if (array.size() == 2) {
        buff.x = array[0].toDouble().value();
        buff.y = array[1].toDouble().value();
    }
    else {
        buff.x = buff.y = 0.0f;
    }

    return buff;
}
// _________________________________________________________________________________________
extern MPoint& parsePoint(const cbson::CBMap& map, const std::string& key, MPoint& buff)
{ /*_coocs3_
     const CBArray& array = map[key];
     if (array.size() == 2) {
         buff.x = array[0].toDouble().value();
         buff.y = array[1].toDouble().value();
     }
     else {
         buff.x = buff.y = 0.0f;
     }

     return buff;
 _coocs3_*/
}
// _________________________________________________________________________________________
extern IntVec& parseIntVec(const cbson::CBMap& map, const std::string& key, IntVec& buff)
{
    buff.clear();

    for (const CBDouble& elem : map[key].toArray()) {
        buff.push_back(elem.toDouble().value());
    }

    return buff;
}
// _________________________________________________________________________________________
extern StringVec& parseStringVec(const cbson::CBMap& map, const std::string& key, StringVec& buff)
{
    buff.clear();

    for (const CBString& elem : map[key].toArray()) {
        buff.push_back(elem.toString().value());
    }

    return buff;
}
// _________________________________________________________________________________________
extern StringVecVec& parseStringVecVec(
    const cbson::CBMap& map, const std::string& key1, const std::string& key2, StringVecVec& buff)
{
    buff.clear();

    for (const CBMap& inner : map[key1].toArray()) {
        StringVec strings;
        parseStringVec(inner, key2, strings);
        buff.push_back(strings);
    }

    return buff;
}
// _________________________________________________________________________________________
extern IntVecVec& parseIntVec(
    const cbson::CBMap& map, const std::string& key1, const std::string& key2, IntVecVec& buff)
{
    buff.clear();

    for (const CBMap& inner : map[key1].toArray()) {
        IntVec numbers;
        parseIntVec(inner, key2, numbers);
        buff.push_back(numbers);
    }

    return buff;
}
// _________________________________________________________________________________________
}
