//
//  JsonType.cpp
//  LostChapters
//
//  Created by a.arsentiev on 22.09.14.
//  Copyright (c) 2014 Crazy Bit. All rights reserved.
//

#include "JsonType.h"
#include "JsonUtils.h"

JMap JMapZero = JMap();
JArray JArrayZero = JArray();
// _________________________________________________________________________________________
void json::UnknownElement::PrintDebug() const
{
    json::Writer::Write(*this, std::cout);
};
// _________________________________________________________________________________________
void json::Object::PrintDebug() const
{
    json::Writer::Write(*this, std::cout);
};
// _________________________________________________________________________________________
void json::Array::PrintDebug() const
{
    json::Writer::Write(*this, std::cout);
};
// _________________________________________________________________________________________
std::string json::UnknownElement::str() const
{
    std::stringstream ss;
    json::Writer::Write(*this, ss);
    return ss.str();
}
// _________________________________________________________________________________________
std::string json::Object::str() const
{
    std::stringstream ss;
    json::Writer::Write(*this, ss);
    return ss.str();
}
// _________________________________________________________________________________________
std::string json::Array::str() const
{
    std::stringstream ss;
    json::Writer::Write(*this, ss);

    return ss.str();
}
// _________________________________________________________________________________________
std::string json::UnknownElement::strNoPretty() const
{
    std::stringstream ss;
    json::Writer::Write(*this, ss);
    LogicError("unimpl");
    return ss.str();
}
// _________________________________________________________________________________________
std::string json::Object::strNoPretty() const
{
    std::stringstream ss;
    json::Writer::Write(*this, ss);
    LogicError("unimpl");
    return ss.str();
}
// _________________________________________________________________________________________
std::string json::Array::strNoPretty() const
{
    std::stringstream ss;
    json::Writer::Write(*this, ss);
    LogicError("unimpl");
    return ss.str();
}
// _________________________________________________________________________________________
void json::Object::fromStr(const std::string& str)
{
    JsonUtils::deserializeJSON(str.c_str(), *this);
}
// _________________________________________________________________________________________
void json::Array::fromStr(const std::string& str)
{
    JsonUtils::deserializeJSON(str.c_str(), *this);
}
// _________________________________________________________________________________________
