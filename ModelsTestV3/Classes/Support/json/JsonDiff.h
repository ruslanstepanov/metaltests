//
//  JsonDiff.cpp
//  Game
//
//  Created by Denis Dolgikh on 18.01.16.
//  Copyright © 2016 Crazy Bit. All rights reserved.
//

#pragma once

#include <string>
#include "JsonPre.h"

namespace JsonDiff {
/// Нахождение дифа при получении ошибки BAD_CHEKSUM
std::string diffRedCheckSum(const std::string& one, const std::string& two);
} // namespace JsonDiff
