//
//  JsonDiff.cpp
//  Game
//
//  Created by Denis Dolgikh on 18.01.16.
//  Copyright © 2016 Crazy Bit. All rights reserved.
//

#include "JsonDiff.h"

#include "JsonType.h"

namespace JsonDiff {
// _________________________________________________________________________________________
JArray diffRedCheckSum(const JArray& one, const JArray& two)
{
    auto AddNumber = [](const JElement& one, const JElement& two, JArray& diff, const std::string& name) {
        if (one.isNumber() && two.isNumber() && one.toNumber() != two.toNumber()) {
            JMap obj;
            obj.Insert(JMap::Member(name, JNumber(one.toNumber() - two.toNumber())));
            diff.Insert(obj);
        }
    };

    JArray diff;
    auto oneIt = one.Begin();
    auto twoIt = two.Begin();
    while (oneIt != one.End() && twoIt != two.End()) {
        const auto* oneObj = oneIt->isObject() && not oneIt->toObject().Empty() ? &*oneIt->toObject().Begin() : nullptr;
        const auto* twoObj = twoIt->isObject() && not twoIt->toObject().Empty() ? &*twoIt->toObject().Begin() : nullptr;
        if (not(oneObj && std::all_of(oneObj->name.begin(), oneObj->name.end(), ::isdigit))) {
            ++oneIt;
            continue;
        }
        if (not(twoObj && std::all_of(twoObj->name.begin(), twoObj->name.end(), ::isdigit))) {
            ++twoIt;
            continue;
        }
        int compare = oneObj->name.compare(twoObj->name);
        if (compare < 0) {
            AddNumber(oneObj->element, JNumber(0), diff, oneObj->name);
            ++oneIt;
        }
        else if (compare > 0) {
            AddNumber(JNumber(0), twoObj->element, diff, twoObj->name);
            ++twoIt;
        }
        else {
            AddNumber(oneObj->element, twoObj->element, diff, oneObj->name);
            ++oneIt;
            ++twoIt;
        }
    }
    for (; oneIt != one.End(); ++oneIt) {
        const auto* oneObj = oneIt->isObject() && not oneIt->toObject().Empty() ? &*oneIt->toObject().Begin() : nullptr;
        if (oneObj && std::all_of(oneObj->name.begin(), oneObj->name.end(), ::isdigit))
            AddNumber(oneObj->element, JNumber(0), diff, oneObj->name);
    }
    for (; twoIt != two.End(); ++twoIt) {
        const auto* twoObj = twoIt->isObject() && not twoIt->toObject().Empty() ? &*twoIt->toObject().Begin() : nullptr;
        if (twoObj && std::all_of(twoObj->name.begin(), twoObj->name.end(), ::isdigit))
            AddNumber(JNumber(0), twoObj->element, diff, twoObj->name);
    }
    return diff;
}

// _________________________________________________________________________________________
JMap diffRedCheckSum(const JMap& one, const JMap& two)
{
    auto AddArray = [](const JElement& one, const JElement& two, JMap& diff, const std::string& name) {
        if (one.isArray() && two.isArray()) {
            auto array = diffRedCheckSum(one.toArray(), two.toArray());
            if (not array.Empty()) {
                diff.Insert(JMap::Member(name, array));
            }
        }
    };

    JMap diff;
    auto oneIt = one.Begin();
    auto twoIt = two.Begin();
    while (oneIt != one.End() && twoIt != two.End()) {
        if (not std::all_of(oneIt->name.begin(), oneIt->name.end(), ::isdigit)) {
            ++oneIt;
            continue;
        }
        if (not std::all_of(twoIt->name.begin(), twoIt->name.end(), ::isdigit)) {
            ++twoIt;
            continue;
        }
        int compare = oneIt->name.compare(twoIt->name);
        if (compare < 0) {
            diff.Insert(*oneIt);
            ++oneIt;
        }
        else if (compare > 0) {
            AddArray(JArray(), twoIt->element, diff, twoIt->name);
            ++twoIt;
        }
        else {
            AddArray(oneIt->element, twoIt->element, diff, twoIt->name);
            ++oneIt;
            ++twoIt;
        }
    }
    for (; oneIt != one.End(); ++oneIt) {
        if (std::all_of(oneIt->name.begin(), oneIt->name.end(), ::isdigit))
            diff.Insert(*oneIt);
    }
    for (; twoIt != two.End(); ++twoIt) {
        if (std::all_of(twoIt->name.begin(), twoIt->name.end(), ::isdigit))
            AddArray(JArray(), twoIt->element, diff, twoIt->name);
    }
    return diff;
}

// _________________________________________________________________________________________
std::string diffRedCheckSum(const std::string& jsonOne, const std::string& jsonTwo)
{
    JMap one, two;
    one.fromStr(jsonOne);
    two.fromStr(jsonTwo);
    return JsonDiff::diffRedCheckSum(one, two).str();
}
// _________________________________________________________________________________________
} // namespace JsonDiff
