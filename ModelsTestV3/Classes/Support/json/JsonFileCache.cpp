//
//  GuiConfigJsonCache.cpp
//  Game
//
//  Created by a.arsentiev on 04.04.16.
//  Copyright © 2016 Crazy Bit. All rights reserved.
//

#include "JsonFileCache.hpp"
//#include "StreamFileLoader.h"
#include "JsonUtils.h"
//#include "PlatformIndependentConst.h"
#include "JsonType.h"

// _________________________________________________________________________________________
JsonFileCache::JsonFileCache()
{
}
// _________________________________________________________________________________________
JsonFileCache::~JsonFileCache()
{
}
// _________________________________________________________________________________________
void JsonFileCache::clear()
{
    mArrs.clear();
    mMaps.clear();
}
// _________________________________________________________________________________________
const JMap* JsonFileCache::getJMap(const std::string& fName)
{
    if (P_MAC || ASK_FB_ID) {
        clear();
    }

    auto iter = mMaps.find(fName);
    if (iter != mMaps.end()) {
        return iter->second.get();
    }

    StreamFileLoader stream(fName);
    if (stream.isOk() == false) {
        ie::log("JsonFileCache: can't load file %s", fName.c_str());
        LogicErrorF("JsonFileCache: can't load file %s", fName.c_str());
        return nullptr;
    }

    auto newMap = std::unique_ptr<JMap>(new JMap);

    bool parsed = JsonUtils::loadFromStream(*newMap, stream.getStream());
    if (!parsed) {
        ie::log("JsonFileCache: can't parce file file %s", fName.c_str());
        LogicErrorF("JsonFileCache: can't parce file file %s", fName.c_str());
        return nullptr;
    }

    const JMap* ret = newMap.get();

    mMaps.emplace(fName, std::move(newMap));

    return ret;
}
// _________________________________________________________________________________________
const JArray* JsonFileCache::getJArr(const std::string& fName)
{
    //    if (ENABLE_DIALOG_TEST || ASK_FB_ID) {
    //        clear();
    //    }

    auto iter = mArrs.find(fName);
    if (iter != mArrs.end()) {
        return iter->second.get();
    }

    StreamFileLoader stream(fName);
    if (stream.isOk() == false) {
        ie::log("JsonFileCache: can't load file %s", fName.c_str());
        LogicErrorF("JsonFileCache: can't load file %s", fName.c_str());
        return nullptr;
    }

    auto newArr = std::unique_ptr<JArray>(new JArray);

    bool parsed = JsonUtils::loadFromStream(*newArr, stream.getStream());
    if (!parsed) {
        ie::log("JsonFileCache: can't parce file file %s", fName.c_str());
        LogicErrorF("JsonFileCache: can't parce file file %s", fName.c_str());
        return nullptr;
    }

    const JArray* ret = newArr.get();

    mArrs.emplace(fName, std::move(newArr));

    return ret;
}
// _________________________________________________________________________________________
