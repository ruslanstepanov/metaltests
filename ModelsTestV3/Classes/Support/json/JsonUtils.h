//
//  JsonUtils.h
//  HelloWorld
//
//  Created by a.arsentiev on 21.12.12.
//
//

#pragma once

#include "Global.h"
#include "cocos2d.h"
#include "JsonPre.h"

class Items;
class Item;
class MPoint;

namespace cbson {
class CBArray;
class CBMap;
} // namespace cbson

namespace JsonUtils {
extern bool boolFromJMap(const JMap& map, const std::string& key, bool defVal);
extern int intFromJMap(const JMap& map, const std::string& key, int defVal);
extern double doubleFromJMap(const JMap& map, const std::string& key, double defVal);
extern std::string& stringFromJMap(const JMap& map, const std::string& key, std::string& buff);
extern IntVec& intVecFromJMap(const JMap& map, const std::string& key, IntVec& buff);
extern StringVec& stringVecFromJArray(const JArray& from, StringVec& buff);
extern JArray& jArrayFromStringVec(const StringVec& from, JArray& to);
extern JMap& jMapFromItems(const Items& items, JMap& to);
extern cocos2d::Vec2& ccpFromJMap(const JMap& map, const std::string& key, cocos2d::Vec2& buff);
extern FloatVecVec& floatVecVecFromJMap(const JMap& map, const std::string& key, FloatVecVec& buff);
extern IntVecVec& intVecVecFromJMap(const JMap& map, const std::string& key, IntVecVec& buff);

extern std::string getString(const JElement& element);

extern Items& parseItems(const JElement& elem, Items& items);
extern Items& parseItems(const rapidjson::Value& elem, Items& items);
extern Item& parseItem(const JElement& elem, Item& item);
extern Item& parseItem(const rapidjson::Value& elem, Item& item);

extern bool loadFromStream(JMap& object, std::istream& istr);
extern bool loadFromStream(JArray& object, std::istream& istr);
extern bool loadFromFile(JArray& jarray, const std::string& path);
extern bool loadFromFile(JMap& jmap, const std::string& path);
extern bool loadFromFile(rapidjson::Document& jmap, const std::string& path);

extern void mergeJMap(JMap& to, const JMap& from);
extern void mergeJArray(JArray& to, const JArray& from);
extern void mergeJElement(JElement& to, const JElement& from);

extern bool deserializeJSON(const char* str, json::Object& obj);
extern bool deserializeJSON(const char* str, json::Array& obj);
extern bool isJMap(const char* str);
extern bool isJArray(const char* str);
extern bool isJString(const char* str);
extern bool isJNum(const char* str);
extern std::string& prepareStrForJson(std::string& str);
} // namespace JsonUtils

using namespace cbson;
namespace CbsonUtils {
extern Items& parseItems(const cbson::CBArray& elem, Items& items);
extern Item& parseItem(const cbson::CBMap& elem, Item& item);
extern cocos2d::Vec2& parsePoint(const cbson::CBMap& map, const std::string& key, cocos2d::Vec2& buff);
extern MPoint& parsePoint(const cbson::CBMap& map, const std::string& key, MPoint& buff);
extern IntVec& parseIntVec(const cbson::CBMap& map, const std::string& key, IntVec& buff);
extern StringVec& parseStringVec(const cbson::CBMap& map, const std::string& key, StringVec& buff);

/// взять из map по ключу key1 массив объектов, a из каждого объекта взять строку по ключу key2
extern StringVecVec& parseStringVecVec(
    const cbson::CBMap& map, const std::string& key1, const std::string& key2, StringVecVec& buff);

/// взять из map по ключу key1 массив объектов, a из каждого объекта взять число по ключу key2
extern IntVecVec& parseIntVec(
    const cbson::CBMap& map, const std::string& key1, const std::string& key2, IntVecVec& buff);
} // namespace CbsonUtils
