///
//  Utils.h
//  HelloWorld
//
//  Created by Aleksander Alexander on 26.07.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#pragma once

#include "Global.h"
//#include <stdarg.h>
//#include <wchar.h>
//#include <stdio.h>
//#include <string.h>
//#include <unistd.h>

#include "IEUtils/Str.h"

class MPoint;

//#define MAKEWORD(a, b) ((unsigned short)(((unsigned char)(a)) | ((unsigned short)((unsigned char)(b))) << 8))

//#define HIBYTE(x) ((unsigned int)(x) >> 8)
//#define LOWBYTE(x) ((unsigned char)(x))

#define __PRINT__LINE__ ie::log("%s : %d", __PRETTY_FUNCTION__, __LINE__);

extern const std::string StringZero;
extern const StringVec StringVecZero;
extern const StringMap StringMapZero;
extern const IntSet IntSetZero;
extern const std::string FbIdDebug;
extern const std::string sBuildVersion;

extern const int sDisableOpacity;

#define DEV_MOD (ENABLE_UNIT_TEST)

#if P_MAC
extern const std::string sMacCommonAssetsPath;
#endif

extern bool ENABLE_DIALOG_TEST;
extern bool ENABLE_UNIT_TEST;
extern bool ENABLE_AUTO_BOOST;
extern bool ENABLE_DEBUG_TAP_ZONE;
extern bool ENABLE_DEBUG_ALWAYS_SHOW_TAP_ZONE;
extern bool FORCE_REWARDED_VIDEO_AND_OFFERWALL;

//	https://np.tiny-tribe.com/admin/
//	lostdev
//	8kJap29POI

;

class Atlas;
