# DO NOT EDIT
# This makefile makes sure all linkable targets are
# up-to-date with anything they link to
default:
	echo "Do not invoke directly"

# Rules to remove targets that are older than anything to which they
# link.  This forces Xcode to relink the targets from scratch.  It
# does not seem to check these dependencies itself.
PostBuild.ModelsTestV3.Debug:
PostBuild.cocos2d.Debug: /Developer/Projects/metaltests/ModelsTestV3/mac-build/bin/ModelsTestV3/Debug/ModelsTestV3.app/Contents/MacOS/ModelsTestV3
PostBuild.external.Debug: /Developer/Projects/metaltests/ModelsTestV3/mac-build/bin/ModelsTestV3/Debug/ModelsTestV3.app/Contents/MacOS/ModelsTestV3
PostBuild.ext_recast.Debug: /Developer/Projects/metaltests/ModelsTestV3/mac-build/bin/ModelsTestV3/Debug/ModelsTestV3.app/Contents/MacOS/ModelsTestV3
PostBuild.ext_tinyxml2.Debug: /Developer/Projects/metaltests/ModelsTestV3/mac-build/bin/ModelsTestV3/Debug/ModelsTestV3.app/Contents/MacOS/ModelsTestV3
PostBuild.ext_xxhash.Debug: /Developer/Projects/metaltests/ModelsTestV3/mac-build/bin/ModelsTestV3/Debug/ModelsTestV3.app/Contents/MacOS/ModelsTestV3
PostBuild.ext_xxtea.Debug: /Developer/Projects/metaltests/ModelsTestV3/mac-build/bin/ModelsTestV3/Debug/ModelsTestV3.app/Contents/MacOS/ModelsTestV3
PostBuild.ext_clipper.Debug: /Developer/Projects/metaltests/ModelsTestV3/mac-build/bin/ModelsTestV3/Debug/ModelsTestV3.app/Contents/MacOS/ModelsTestV3
PostBuild.ext_edtaa3func.Debug: /Developer/Projects/metaltests/ModelsTestV3/mac-build/bin/ModelsTestV3/Debug/ModelsTestV3.app/Contents/MacOS/ModelsTestV3
PostBuild.ext_convertUTF.Debug: /Developer/Projects/metaltests/ModelsTestV3/mac-build/bin/ModelsTestV3/Debug/ModelsTestV3.app/Contents/MacOS/ModelsTestV3
PostBuild.ext_poly2tri.Debug: /Developer/Projects/metaltests/ModelsTestV3/mac-build/bin/ModelsTestV3/Debug/ModelsTestV3.app/Contents/MacOS/ModelsTestV3
PostBuild.ext_md5.Debug: /Developer/Projects/metaltests/ModelsTestV3/mac-build/bin/ModelsTestV3/Debug/ModelsTestV3.app/Contents/MacOS/ModelsTestV3
PostBuild.ext_unzip.Debug: /Developer/Projects/metaltests/ModelsTestV3/mac-build/bin/ModelsTestV3/Debug/ModelsTestV3.app/Contents/MacOS/ModelsTestV3
/Developer/Projects/metaltests/ModelsTestV3/mac-build/bin/ModelsTestV3/Debug/ModelsTestV3.app/Contents/MacOS/ModelsTestV3:\
	/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libcocos2d.a\
	/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libexternal.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/Box2D/prebuilt/mac/libbox2d.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/chipmunk/prebuilt/mac/libchipmunk.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/freetype2/prebuilt/mac/libfreetype.a\
	/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_recast.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/jpeg/prebuilt/mac/libjpeg.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/webp/prebuilt/mac/libwebp.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/bullet/prebuilt/mac/libLinearMath.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/bullet/prebuilt/mac/libBulletDynamics.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/bullet/prebuilt/mac/libBulletCollision.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/bullet/prebuilt/mac/libLinearMath.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/bullet/prebuilt/mac/libBulletMultiThreaded.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/bullet/prebuilt/mac/libMiniCL.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/tiff/prebuilt/mac/libtiff.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/websockets/prebuilt/mac/libwebsockets.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/uv/prebuilt/mac/libuv_a.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/openssl/prebuilt/mac/libssl.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/openssl/prebuilt/mac/libcrypto.a\
	/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_tinyxml2.a\
	/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_xxhash.a\
	/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_xxtea.a\
	/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_clipper.a\
	/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_edtaa3func.a\
	/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_convertUTF.a\
	/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_poly2tri.a\
	/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_md5.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/curl/prebuilt/mac/libcurl.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/png/prebuilt/mac/libpng.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/glfw3/prebuilt/mac/libglfw3.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/zlib/prebuilt/mac/libz.a\
	/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_unzip.a\
	/usr/lib/libiconv.dylib
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV3/mac-build/bin/ModelsTestV3/Debug/ModelsTestV3.app/Contents/MacOS/ModelsTestV3


PostBuild.cocos2d.Debug:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libcocos2d.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libcocos2d.a


PostBuild.ext_clipper.Debug:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_clipper.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_clipper.a


PostBuild.ext_convertUTF.Debug:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_convertUTF.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_convertUTF.a


PostBuild.ext_edtaa3func.Debug:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_edtaa3func.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_edtaa3func.a


PostBuild.ext_md5.Debug:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_md5.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_md5.a


PostBuild.ext_poly2tri.Debug:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_poly2tri.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_poly2tri.a


PostBuild.ext_recast.Debug:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_recast.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_recast.a


PostBuild.ext_tinyxml2.Debug:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_tinyxml2.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_tinyxml2.a


PostBuild.ext_unzip.Debug:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_unzip.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_unzip.a


PostBuild.ext_xxhash.Debug:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_xxhash.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_xxhash.a


PostBuild.ext_xxtea.Debug:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_xxtea.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_xxtea.a


PostBuild.external.Debug:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libexternal.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libexternal.a


PostBuild.ModelsTestV3.Release:
PostBuild.cocos2d.Release: /Developer/Projects/metaltests/ModelsTestV3/mac-build/bin/ModelsTestV3/Release/ModelsTestV3.app/Contents/MacOS/ModelsTestV3
PostBuild.external.Release: /Developer/Projects/metaltests/ModelsTestV3/mac-build/bin/ModelsTestV3/Release/ModelsTestV3.app/Contents/MacOS/ModelsTestV3
PostBuild.ext_recast.Release: /Developer/Projects/metaltests/ModelsTestV3/mac-build/bin/ModelsTestV3/Release/ModelsTestV3.app/Contents/MacOS/ModelsTestV3
PostBuild.ext_tinyxml2.Release: /Developer/Projects/metaltests/ModelsTestV3/mac-build/bin/ModelsTestV3/Release/ModelsTestV3.app/Contents/MacOS/ModelsTestV3
PostBuild.ext_xxhash.Release: /Developer/Projects/metaltests/ModelsTestV3/mac-build/bin/ModelsTestV3/Release/ModelsTestV3.app/Contents/MacOS/ModelsTestV3
PostBuild.ext_xxtea.Release: /Developer/Projects/metaltests/ModelsTestV3/mac-build/bin/ModelsTestV3/Release/ModelsTestV3.app/Contents/MacOS/ModelsTestV3
PostBuild.ext_clipper.Release: /Developer/Projects/metaltests/ModelsTestV3/mac-build/bin/ModelsTestV3/Release/ModelsTestV3.app/Contents/MacOS/ModelsTestV3
PostBuild.ext_edtaa3func.Release: /Developer/Projects/metaltests/ModelsTestV3/mac-build/bin/ModelsTestV3/Release/ModelsTestV3.app/Contents/MacOS/ModelsTestV3
PostBuild.ext_convertUTF.Release: /Developer/Projects/metaltests/ModelsTestV3/mac-build/bin/ModelsTestV3/Release/ModelsTestV3.app/Contents/MacOS/ModelsTestV3
PostBuild.ext_poly2tri.Release: /Developer/Projects/metaltests/ModelsTestV3/mac-build/bin/ModelsTestV3/Release/ModelsTestV3.app/Contents/MacOS/ModelsTestV3
PostBuild.ext_md5.Release: /Developer/Projects/metaltests/ModelsTestV3/mac-build/bin/ModelsTestV3/Release/ModelsTestV3.app/Contents/MacOS/ModelsTestV3
PostBuild.ext_unzip.Release: /Developer/Projects/metaltests/ModelsTestV3/mac-build/bin/ModelsTestV3/Release/ModelsTestV3.app/Contents/MacOS/ModelsTestV3
/Developer/Projects/metaltests/ModelsTestV3/mac-build/bin/ModelsTestV3/Release/ModelsTestV3.app/Contents/MacOS/ModelsTestV3:\
	/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libcocos2d.a\
	/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libexternal.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/Box2D/prebuilt/mac/libbox2d.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/chipmunk/prebuilt/mac/libchipmunk.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/freetype2/prebuilt/mac/libfreetype.a\
	/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_recast.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/jpeg/prebuilt/mac/libjpeg.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/webp/prebuilt/mac/libwebp.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/bullet/prebuilt/mac/libLinearMath.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/bullet/prebuilt/mac/libBulletDynamics.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/bullet/prebuilt/mac/libBulletCollision.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/bullet/prebuilt/mac/libLinearMath.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/bullet/prebuilt/mac/libBulletMultiThreaded.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/bullet/prebuilt/mac/libMiniCL.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/tiff/prebuilt/mac/libtiff.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/websockets/prebuilt/mac/libwebsockets.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/uv/prebuilt/mac/libuv_a.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/openssl/prebuilt/mac/libssl.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/openssl/prebuilt/mac/libcrypto.a\
	/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_tinyxml2.a\
	/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_xxhash.a\
	/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_xxtea.a\
	/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_clipper.a\
	/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_edtaa3func.a\
	/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_convertUTF.a\
	/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_poly2tri.a\
	/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_md5.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/curl/prebuilt/mac/libcurl.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/png/prebuilt/mac/libpng.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/glfw3/prebuilt/mac/libglfw3.a\
	/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/zlib/prebuilt/mac/libz.a\
	/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_unzip.a\
	/usr/lib/libiconv.dylib
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV3/mac-build/bin/ModelsTestV3/Release/ModelsTestV3.app/Contents/MacOS/ModelsTestV3


PostBuild.cocos2d.Release:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libcocos2d.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libcocos2d.a


PostBuild.ext_clipper.Release:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_clipper.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_clipper.a


PostBuild.ext_convertUTF.Release:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_convertUTF.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_convertUTF.a


PostBuild.ext_edtaa3func.Release:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_edtaa3func.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_edtaa3func.a


PostBuild.ext_md5.Release:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_md5.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_md5.a


PostBuild.ext_poly2tri.Release:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_poly2tri.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_poly2tri.a


PostBuild.ext_recast.Release:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_recast.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_recast.a


PostBuild.ext_tinyxml2.Release:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_tinyxml2.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_tinyxml2.a


PostBuild.ext_unzip.Release:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_unzip.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_unzip.a


PostBuild.ext_xxhash.Release:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_xxhash.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_xxhash.a


PostBuild.ext_xxtea.Release:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_xxtea.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_xxtea.a


PostBuild.external.Release:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libexternal.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libexternal.a




# For each target create a dummy ruleso the target does not have to exist
/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/Box2D/prebuilt/mac/libbox2d.a:
/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/bullet/prebuilt/mac/libBulletCollision.a:
/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/bullet/prebuilt/mac/libBulletDynamics.a:
/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/bullet/prebuilt/mac/libBulletMultiThreaded.a:
/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/bullet/prebuilt/mac/libLinearMath.a:
/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/bullet/prebuilt/mac/libMiniCL.a:
/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/chipmunk/prebuilt/mac/libchipmunk.a:
/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/curl/prebuilt/mac/libcurl.a:
/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/freetype2/prebuilt/mac/libfreetype.a:
/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/glfw3/prebuilt/mac/libglfw3.a:
/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/jpeg/prebuilt/mac/libjpeg.a:
/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/openssl/prebuilt/mac/libcrypto.a:
/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/openssl/prebuilt/mac/libssl.a:
/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/png/prebuilt/mac/libpng.a:
/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/tiff/prebuilt/mac/libtiff.a:
/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/uv/prebuilt/mac/libuv_a.a:
/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/webp/prebuilt/mac/libwebp.a:
/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/websockets/prebuilt/mac/libwebsockets.a:
/Developer/Projects/metaltests/ModelsTestV3/cocos2d/external/zlib/prebuilt/mac/libz.a:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libcocos2d.a:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_clipper.a:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_convertUTF.a:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_edtaa3func.a:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_md5.a:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_poly2tri.a:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_recast.a:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_tinyxml2.a:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_unzip.a:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_xxhash.a:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libext_xxtea.a:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Debug/libexternal.a:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libcocos2d.a:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_clipper.a:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_convertUTF.a:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_edtaa3func.a:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_md5.a:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_poly2tri.a:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_recast.a:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_tinyxml2.a:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_unzip.a:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_xxhash.a:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libext_xxtea.a:
/Developer/Projects/metaltests/ModelsTestV3/mac-build/lib/Release/libexternal.a:
/usr/lib/libiconv.dylib:
