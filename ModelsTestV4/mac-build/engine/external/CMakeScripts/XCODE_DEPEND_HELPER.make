# DO NOT EDIT
# This makefile makes sure all linkable targets are
# up-to-date with anything they link to
default:
	echo "Do not invoke directly"

# Rules to remove targets that are older than anything to which they
# link.  This forces Xcode to relink the targets from scratch.  It
# does not seem to check these dependencies itself.
PostBuild.ext_clipper.Debug:
/Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Debug/libext_clipper.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Debug/libext_clipper.a


PostBuild.ext_convertUTF.Debug:
/Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Debug/libext_convertUTF.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Debug/libext_convertUTF.a


PostBuild.ext_edtaa3func.Debug:
/Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Debug/libext_edtaa3func.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Debug/libext_edtaa3func.a


PostBuild.ext_md5.Debug:
/Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Debug/libext_md5.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Debug/libext_md5.a


PostBuild.ext_poly2tri.Debug:
/Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Debug/libext_poly2tri.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Debug/libext_poly2tri.a


PostBuild.ext_recast.Debug:
/Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Debug/libext_recast.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Debug/libext_recast.a


PostBuild.ext_tinyxml2.Debug:
/Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Debug/libext_tinyxml2.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Debug/libext_tinyxml2.a


PostBuild.ext_unzip.Debug:
/Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Debug/libext_unzip.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Debug/libext_unzip.a


PostBuild.ext_xxhash.Debug:
/Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Debug/libext_xxhash.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Debug/libext_xxhash.a


PostBuild.ext_xxtea.Debug:
/Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Debug/libext_xxtea.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Debug/libext_xxtea.a


PostBuild.external.Debug:
/Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Debug/libexternal.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Debug/libexternal.a


PostBuild.ext_clipper.Release:
/Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Release/libext_clipper.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Release/libext_clipper.a


PostBuild.ext_convertUTF.Release:
/Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Release/libext_convertUTF.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Release/libext_convertUTF.a


PostBuild.ext_edtaa3func.Release:
/Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Release/libext_edtaa3func.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Release/libext_edtaa3func.a


PostBuild.ext_md5.Release:
/Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Release/libext_md5.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Release/libext_md5.a


PostBuild.ext_poly2tri.Release:
/Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Release/libext_poly2tri.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Release/libext_poly2tri.a


PostBuild.ext_recast.Release:
/Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Release/libext_recast.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Release/libext_recast.a


PostBuild.ext_tinyxml2.Release:
/Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Release/libext_tinyxml2.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Release/libext_tinyxml2.a


PostBuild.ext_unzip.Release:
/Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Release/libext_unzip.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Release/libext_unzip.a


PostBuild.ext_xxhash.Release:
/Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Release/libext_xxhash.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Release/libext_xxhash.a


PostBuild.ext_xxtea.Release:
/Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Release/libext_xxtea.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Release/libext_xxtea.a


PostBuild.external.Release:
/Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Release/libexternal.a:
	/bin/rm -f /Developer/Projects/metaltests/ModelsTestV4/mac-build/lib/Release/libexternal.a




# For each target create a dummy ruleso the target does not have to exist
