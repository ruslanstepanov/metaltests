#include "FireEffect.h"
//#include "GameTimers.h"
//#include "Game.h"
#include <iostream>
//#include "CustomShaderCache/CustomShaderCache.h"
//#include "AssetsUtils.h"

// Можно использовать png-шки
//static const char* sMainTxName = "f_prm_cel.png";
//static const char* sMainThinTxName = "f_prm_cel_thin.png";
//static const char* sWindTxName = "f_disp_cel.png";
//static const char* sGradTxName = "f_grad_cel.png";
//static const char* sGradBlueTxName = "f_grad_cel_blue.png";

// Но лучше pvr.ccz
static const char* sMainTxName = "f_prm_cel.pvr.ccz";
static const char* sMainThinTxName = "f_prm_cel_thin.pvr.ccz";
static const char* sWindTxName = "f_disp_cel.pvr.ccz";
static const char* sGradTxName = "f_grad_cel.pvr.ccz";
static const char* sGradBlueTxName = "f_grad_cel_blue.pvr.ccz";

static const char* sVertFileName = "FireEffectShader.vsh";
static const char* sFragFileName = "FireEffectShader.fsh";

static const float sMaxSaturation = 0.95f;

// _______________________________________________________________________
FireEffect* FireEffect::create(const cocos2d::Size& size, unsigned fireType, float deltaTime, float fireSaturation,
    float speed, float displacementScale)
{
    FireEffect* fe = new FireEffect(deltaTime);
    fe->setAnchorPoint(cocos2d::Vec2(0.0f, 0.0f));
    fe->autorelease();

    const char* mainTxName = sMainTxName;
    const char* gradTxName = sGradTxName;
    if (fireType & THIN)
        mainTxName = sMainThinTxName;
    if (fireType & BLUE)
        gradTxName = sGradBlueTxName;

    fe->init(size, mainTxName, sWindTxName, gradTxName);
    fe->setFireSaturation(fireSaturation);
    fe->setSpeed(speed);
    fe->setDisplacementScale(displacementScale);

    return fe;
}
// _______________________________________________________________________
FireEffect::~FireEffect()
{
    //    STimers()->onTick.disconnect(this, &FireEffect::onTimer);
    //    STimers()->onPSec25.disconnect(this, &FireEffect::onAnimate);

    clear();
}
// _______________________________________________________________________
void FireEffect::setFireSaturation(float val)
{
    if (val < 0.0f)
        val = 0.0f;
    if (val > sMaxSaturation)
        val = sMaxSaturation;

    mFireSaturation = val;
}
// _______________________________________________________________________
void FireEffect::animateIncrease()
{
    mSaturationSpeed = 0.01f;
    //    STimers()->onPSec25.connect(this, &FireEffect::onAnimate);
}
// _______________________________________________________________________
void FireEffect::animateDecrease()
{
    mSaturationSpeed = -0.01f;
    //    STimers()->onPSec25.connect(this, &FireEffect::onAnimate);
}
// _______________________________________________________________________
FireEffect::FireEffect(float deltaTime)
    : mDeltaTime(deltaTime)
    , mFireSaturation(sMaxSaturation)
{
}
// _______________________________________________________________________
void FireEffect::init(
    const cocos2d::Size& size, const std::string& mainTxName, const std::string& windTxName, const std::string& gradTxName)
{
    mMainTextureName = mainTxName;
    mWindTextureName = windTxName;
    mGradTextureName = gradTxName;

    if (!prepare()) {
        cocos2d::log("Can't prepate fire effect");
        return;
    }

    ///! CHECK_GL_ERROR_DEBUG();

    if (!Sprite::initWithTexture(mMainTexture)) {
        cocos2d::log("Can't init texture %s", mainTxName.c_str());
        //        Breakpoint();
        return;
    }

    cocos2d::Size texSize = mMainTexture->getContentSize();
    const float scaleX = size.width / texSize.width;
    const float scaleY = size.height / texSize.height;
    setScaleX(scaleX);
    setScaleY(scaleY);

    //    STimers()->onTick.connect(this, &FireEffect::onTimer);
}
// _______________________________________________________________________
bool FireEffect::prepare()
{
    cocos2d::Texture2D::TexParams params = { backend::SamplerFilter::LINEAR, backend::SamplerFilter::LINEAR, backend::SamplerAddressMode::REPEAT, backend::SamplerAddressMode::REPEAT };

    std::string vsfullName = cocos2d::FileUtils::getInstance()->fullPathForFilename(sVertFileName);
    std::string fsfullName = cocos2d::FileUtils::getInstance()->fullPathForFilename(sFragFileName);

    //	if (ie::isBundleFileExist(vsfullName) == false) {
    //        ie::log("can't find file %s", vsfullName.c_str());
    //        return false;
    //    }
    //	if (ie::isBundleFileExist(fsfullName) == false) {
    //        ie::log("can't find file %s", fsfullName.c_str());
    //        return false;
    //    }

    // Подготовка данных
    loadShader();

    mMainTexture = cocos2d::Director::getInstance()->getTextureCache()->addImage(mMainTextureName.c_str());
    mMainTexture->setTexParameters(params);
    mMainTexture->retain();

    mWindTexture = cocos2d::Director::getInstance()->getTextureCache()->addImage(mWindTextureName.c_str());
    mWindTexture->setTexParameters(params);
    mWindTexture->retain();

    mGradTexture = cocos2d::Director::getInstance()->getTextureCache()->addImage(mGradTextureName.c_str());
    mGradTexture->setTexParameters(params);
    mGradTexture->retain();
    
    _programState->setTexture(mMainTextureUniform, 0, mMainTexture->getBackendTexture());
    _programState->setTexture(mWindTextureUniform, 1, mWindTexture->getBackendTexture());
    _programState->setTexture(mGradTextureUniform, 2, mGradTexture->getBackendTexture());

    return true;
}
// _________________________________________________________________________________________
const static std::string sRoot = "";//"shaders/";
// _________________________________________________________________________________________
void FireEffect::loadShader()
{
    CC_SAFE_RELEASE_NULL(_programState);
    
    // "FireEffectShader"
    // sRoot + name + ".vsh", sRoot + name + ".fsh"
    const char * shaderVert = R"(
    #ifdef GL_ES
        #define QUALITY highp
    #else
        #define QUALITY
    #endif

    attribute QUALITY vec4 a_position;
    attribute QUALITY vec2 a_texCoord;
    attribute QUALITY vec4 a_color;
    uniform mat4 u_MVPMatrix;
    varying QUALITY vec2 v_texCoord;

    void main()
    {
        gl_Position = u_MVPMatrix * a_position;
        v_texCoord = a_texCoord;
    }
    )";
    
    const char * shaderFrag = R"(
    #ifdef GL_ES
        #define QUALITY highp
    #else
        #define QUALITY
    #endif

    uniform QUALITY sampler2D uMainTexture;
    uniform QUALITY sampler2D uWindTexture;
    uniform QUALITY sampler2D uGradTexture;

    uniform QUALITY float uTime;
    uniform QUALITY float uSaturation;
    uniform QUALITY float uWindDisplacement;

    varying QUALITY vec2 v_texCoord;

    void main()
    {
        //        gl_FragColor = vec4(1.0,0.0,1.0,1.0);
        float blue = texture2D(uMainTexture, vec2(v_texCoord.x, v_texCoord.y)).b;

        vec2 windTex = texture2D(uWindTexture, vec2(v_texCoord.x, v_texCoord.y + uTime * 0.7)).rb;
        windTex = windTex * 2.0 - 1.0;
        windTex *= blue * uWindDisplacement * 0.3;

        float red = texture2D(uMainTexture, vec2(v_texCoord.x, v_texCoord.y)).r;
        float green = texture2D(uMainTexture, vec2(v_texCoord.x + windTex.x, v_texCoord.y + uTime + windTex.y)).g;

        float displacement = (red * green) * uSaturation;

        vec2 fireTexCoord = vec2(0.01 + displacement, 0.5);
        vec4 fireColor = texture2D(uGradTexture, fireTexCoord);

        gl_FragColor = fireColor;
    }
    
    
    )";
    _programState = new cocos2d::backend::ProgramState(shaderVert, shaderFrag);

    mMainTextureUniform = _programState->getUniformLocation("uMainTexture");
    mWindTextureUniform = _programState->getUniformLocation("uWindTexture");
    mGradTextureUniform = _programState->getUniformLocation("uGradTexture");
    
    mUniformTime = _programState->getUniformLocation("uTime");
    mUniformSaturation = _programState->getUniformLocation("uSaturation");
    mUnifoirmDisplacement = _programState->getUniformLocation("uWindDisplacement");
    
    _programState->setUniform(mUniformTime, &mDeltaTime, sizeof(mDeltaTime));
    _programState->setUniform(mUniformSaturation, &mFireSaturation, sizeof(mFireSaturation));
    _programState->setUniform(mUnifoirmDisplacement, &mDisplacementScale, sizeof(mDisplacementScale));
    
    

    
    
    auto &pipelineDescriptor = _customCommand.getPipelineDescriptor();
    pipelineDescriptor.programState = _programState;
    
    auto layout = _programState->getVertexLayout();
    const auto& attributeInfo = _programState->getProgram()->getActiveAttributes();
    auto iter = attributeInfo.find("a_position");
    if(iter != attributeInfo.end()) {
       layout->setAttribute("a_position", iter->second.location, backend::VertexFormat::FLOAT3, offsetof(V3F_C4B_T2F, vertices), false);
    }
    iter = attributeInfo.find("a_color");
    if(iter != attributeInfo.end()) {
       layout->setAttribute("a_color", iter->second.location, backend::VertexFormat::UBYTE4, offsetof(V3F_C4B_T2F, colors), true);
    }
    iter = attributeInfo.find("a_texCoord");
    if(iter != attributeInfo.end()) {
       layout->setAttribute("a_texCoord", iter->second.location, backend::VertexFormat::FLOAT2, offsetof(V3F_C4B_T2F, texCoords), true);
    }
    layout->setLayout(sizeof(_quad.bl));
    
    initBuffer();
    
    // Чтобы прозрачность не становилась черной
    backend::BlendDescriptor& blendDescriptor = pipelineDescriptor.blendDescriptor;
    blendDescriptor.blendEnabled = true;
    blendDescriptor.rgbBlendOperation = backend::BlendOperation::ADD;
    blendDescriptor.alphaBlendOperation = backend::BlendOperation::ADD;
    // pipelineDescriptor.colorAttachments[0].sourceRGBBlendFactor = .destinationAlpha
    blendDescriptor.sourceRGBBlendFactor = backend::BlendFactor::SRC_ALPHA;
    blendDescriptor.destinationRGBBlendFactor = backend::BlendFactor::ONE_MINUS_SRC_ALPHA;
    // pipelineDescriptor.colorAttachments[0].sourceAlphaBlendFactor = .destinationAlpha
    blendDescriptor.sourceAlphaBlendFactor = backend::BlendFactor::SRC_ALPHA;
    // pipelineDescriptor.colorAttachments[0].destinationAlphaBlendFactor = .oneMinusBlendAlpha
    blendDescriptor.destinationAlphaBlendFactor = backend::BlendFactor::ONE_MINUS_SRC_ALPHA;
    setOpacityModifyRGB(false);
}
// _______________________________________________________________________
void FireEffect::initBuffer()
{
    uint16_t indices[6] = { 0, 1, 2, 2, 1, 3 };
    _customCommand.createIndexBuffer(CustomCommand::IndexFormat::U_SHORT, sizeof(indices) / sizeof(indices[0]), CustomCommand::BufferUsage::STATIC);
    _customCommand.updateIndexBuffer(indices, sizeof(indices));
    
    _customCommand.createVertexBuffer(sizeof(_quad.bl), 4, CustomCommand::BufferUsage::DYNAMIC);
    _customCommand.updateVertexBuffer(&_quad, sizeof(_quad.bl) * 4);
}
// _______________________________________________________________________
void FireEffect::clear()
{
    CC_SAFE_RELEASE_NULL(_programState);
    CC_SAFE_RELEASE_NULL(mMainTexture);
    CC_SAFE_RELEASE_NULL(mWindTexture);
    CC_SAFE_RELEASE_NULL(mGradTexture);
}
// _______________________________________________________________________
void FireEffect::onTimer()
{
    mDeltaTime += mSpeed * 0.1f;
    if (mDeltaTime > 10.0f)
        mDeltaTime -= 10.0f;
}
// _______________________________________________________________________
void FireEffect::onAnimate()
{
    setFireSaturation(mFireSaturation + mSaturationSpeed);
    
    //    if (mFireSaturation <= 0.0f || mFireSaturation >= sMaxSaturation)
    //         STimers()->onPSec25.disconnect(this, &FireEffect::onAnimate);
}
// _______________________________________________________________________
void FireEffect::draw(cocos2d::Renderer* renderer, const cocos2d::Mat4& transform, uint32_t flags)
{
    // tick
    onTimer();
    
    _customCommand.init(_globalZOrder, transform, flags);
    _customCommand.setBeforeCallback(CC_CALLBACK_0(FireEffect::doDraw, this, transform, flags));
    renderer->addCommand(&_customCommand);
}
// _______________________________________________________________________
void FireEffect::doDraw(const cocos2d::Mat4& transform, uint32_t flags)
{
    _customCommand.updateVertexBuffer(&_quad, sizeof(_quad.bl) * 4);
    
    const auto& projectionMat = Director::getInstance()->getMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_PROJECTION) * transform;;
    _programState->setUniform(_mvpMatrixLocation, projectionMat.m, sizeof(projectionMat.m));
    
    _programState->setUniform(mUniformTime, &mDeltaTime, sizeof(mDeltaTime));
    _programState->setUniform(mUniformSaturation, &mFireSaturation, sizeof(mFireSaturation));
    _programState->setUniform(mUnifoirmDisplacement, &mDisplacementScale, sizeof(mDisplacementScale));
}
// _________________________________________________________________________________________
