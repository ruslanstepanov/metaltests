#pragma once

#define GL_SILENCE_DEPRECATION true
#include "cocos2d.h"

USING_NS_CC;

class FireEffect : public Sprite {
    //_IMPLEMENT_RTTI_
public:
    enum FireType { BOLD = 1, THIN = 2, ORANGE = 4, BLUE = 8 };

    /// Создание эффекта огня
    /// @param size Размер спрайта огня
    /// @param fireType Тип огня: оранжевый, голубой, узкий и пр
    /// @param deltaTime сдвиг времени анимации огня
    /// @param fireSaturation Величина пламени, значение от 0.0f до 1.0f
    static FireEffect* create(const cocos2d::Size& size, unsigned fireType, float deltaTime = 0.0f,
        float fireSaturation = 1.0f, float speed = 1.0f, float displacement = 1.0f);

    ~FireEffect() override;

    /** Задать размер пламени
     * @param value 0.0f - минимальноый размер пламени
     * 1.0f - максимальный размер пламени
     */
    void setFireSaturation(float value);
    /// Анимировать воспламенение
    void animateIncrease();
    /// Анимировать потухание
    void animateDecrease();
    /// Скорость вылетания огня
    inline void setSpeed(float value)
    {
        mSpeed = value;
    }
    /// Масштаб смещения
    inline void setDisplacementScale(float value)
    {
        mDisplacementScale = value;
    }

private:
    /// имена текстур
    std::string mMainTextureName;
    std::string mWindTextureName;
    std::string mGradTextureName;
    /// идентификаторы тектур
    cocos2d::Texture2D* mMainTexture = nullptr;
    cocos2d::Texture2D* mWindTexture = nullptr;
    cocos2d::Texture2D* mGradTexture = nullptr;
    /// сдвиг по времени
    float mDeltaTime = 0.0f;
    /// размер огня 0.0f - 1.0f
    float mFireSaturation = 1.0f;
    /// скорость изменения огня (для анимации)
    float mSaturationSpeed = 0.0f;
    /// Скорость пламени
    float mSpeed = 1.0f;
    /// Масштаб смещения
    float mDisplacementScale = 1.0f;

    FireEffect(float deltaTime);
    
    /// подготовить рендерер к рисованию
    bool prepare();
    /// почистить контекст после рисования
    void clear();
    /// вызов таймера 1/25 секунды
    void onTimer();
    /// вызов по таймеру для анимашек воспламенения и потухания
    void onAnimate();
    /// формирование команды для рендерера
    void draw(cocos2d::Renderer* renderer, const cocos2d::Mat4& transform, uint32_t flags) override;
    /// функция отрисовки
    void doDraw(const cocos2d::Mat4& transform, uint32_t flags);
    
protected: // methods
    /// первичная инициализация
    void init(const cocos2d::Size& size, const std::string& mainTxName, const std::string& windTxName,
        const std::string& gradTxName);
    /// грузин и линкует шейдеры
    void loadShader();
    /// инициализируем буфер вершин
    void initBuffer();
    
    void myDraw();
    
protected: // vars
    /// Команда для отрисовки
    cocos2d::CustomCommand _customCommand;
    /// шейдерная программа
    cocos2d::backend::ProgramState* _programState = nullptr;
    /// Юниформ для времени
    backend::UniformLocation mUniformTime;
    /// Юниформ для наполнения огня
    backend::UniformLocation mUniformSaturation;
    /// Юниформ для дисплейсмента огня
    backend::UniformLocation mUnifoirmDisplacement;
    /// шейдерная программа
    backend::UniformLocation mMainTextureUniform;
    backend::UniformLocation mGradTextureUniform;
    backend::UniformLocation mWindTextureUniform;
};
