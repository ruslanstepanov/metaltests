/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#include "HelloWorldScene.h"

USING_NS_CC;

#define Z_LABELS 100
#define Z_BUTTONS 50
#define Z_FIRE  25
#define Z_MODELS 10
#define Z_ANIM_SPRITES 0

Scene* HelloWorld::createScene()
{
    return HelloWorld::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
    auto closeItem = MenuItemImage::create(
                                           "CloseNormal.png",
                                           "CloseSelected.png",
                                           CC_CALLBACK_1(HelloWorld::onAddModels, this));

    if (closeItem == nullptr ||
        closeItem->getContentSize().width <= 0 ||
        closeItem->getContentSize().height <= 0)
    {
        problemLoading("'CloseNormal.png' and 'CloseSelected.png'");
    }
    else
    {
        closeItem->setScale(3);
        
        float x = origin.x + visibleSize.width - closeItem->getContentSize().width/2;
        float y = origin.y + closeItem->getContentSize().height/2;
        //closeItem->setPosition(Vec2(x,y));
        
        closeItem->setPosition(Vec2(visibleSize.width/2, visibleSize.height/2));
    }

    // btnAddAnimSprites
    auto btnAddAnimSprites = MenuItemImage::create(
                                           "CloseNormal.png",
                                           "CloseSelected.png",
                                           CC_CALLBACK_1(HelloWorld::onAddAnimSprites, this));
    if (btnAddAnimSprites)
    {
        btnAddAnimSprites->setScale(3);
        btnAddAnimSprites->setPosition(Vec2(visibleSize.width/2 + 100, visibleSize.height/2));
    }
    
    // btnAddFires
    auto btnAddFires = MenuItemImage::create(
                                           "CloseNormal.png",
                                           "CloseSelected.png",
                                           CC_CALLBACK_1(HelloWorld::onAddFires, this));
    if (btnAddFires)
    {
        btnAddFires->setScale(3);
        btnAddFires->setPosition(Vec2(visibleSize.width/2 + 200, visibleSize.height/2));
    }

    // create menu, it's an autorelease object
    auto menu = Menu::create(closeItem, btnAddAnimSprites, btnAddFires, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, Z_BUTTONS);

    /////////////////////////////
    // 3. add your codes below...

    // add a label shows "Hello World"
    // create and initialize a label

    mLabelCounts = Label::createWithTTF("Hello World", "fonts/Marker Felt.ttf", 24);
    if (mLabelCounts == nullptr)
    {
        problemLoading("'fonts/Marker Felt.ttf'");
    }
    else
    {
        mLabelCounts->setAnchorPoint(Vec2(0.5f, 1.0f));
        // position the label on the center of the screen
        mLabelCounts->setPosition(Vec2(origin.x + visibleSize.width/2,
                                origin.y + visibleSize.height));

        // add the label as a child to this layer
        this->addChild(mLabelCounts, Z_LABELS);
    }

    // add "HelloWorld" splash screen"
    auto sprite = Sprite::create("HelloWorld.png");
    if (sprite == nullptr)
    {
        problemLoading("'HelloWorld.png'");
    }
    else
    {
        // position the sprite on the center of the screen
        sprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));

        // add the sprite as a child to this layer
        this->addChild(sprite, 0);
        
        sprite->setVisible(false);
    }
    
    FileUtils::getInstance()->addSearchPath("Resources");
    FileUtils::getInstance()->addSearchPath("3d");
    FileUtils::getInstance()->addSearchPath("3d-textures");
    FileUtils::getInstance()->addSearchPath("shaders");
    FileUtils::getInstance()->addSearchPath("fire");
    
    // Сделать фон белым
    //    Director::getInstance()->setClearColor(Color4F::WHITE);
    
    //    // Добавить огня
    //    addFireEffect();
    
    return true;
}


void HelloWorld::onAddModels(Ref* pSender)
{
    int addCount = 50;
    for (int i=0; i<addCount; i++)
        createSprite3d();
    
    m3dModelsCount += addCount;
    
    updateLabel();
    
    //Close the cocos2d-x game scene and quit the application
    //Director::getInstance()->end();

    /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() as given above,instead trigger a custom event created in RootViewController.mm as below*/

    //EventCustom customEndEvent("game_scene_close_event");
    //_eventDispatcher->dispatchEvent(&customEndEvent);
}
// _________________________________________________________________________________________
void HelloWorld::onAddAnimSprites(Ref* pSender)
{
    int addCount = 100;
    for (int i=0; i<addCount; i++)
        createAnimatedSprite();
    
    mAnimSpritesCount += addCount;
    
    updateLabel();
}
// _________________________________________________________________________________________
void HelloWorld::onAddFires(Ref* pSender)
{
    int addCount = 50;
    for (int i=0; i<addCount; i++)
        addFireEffect();
    
    mFiresCount += addCount;
    
    updateLabel();
}
// _________________________________________________________________________________________
void HelloWorld::updateLabel()
{
    if (not mLabelCounts)
        return;
    
    std::string str = "Cocos2d v4\nModels = " + std::to_string(m3dModelsCount) + "\n" +
                        "Sprites = " + std::to_string(mAnimSpritesCount) + "\n" +
                           "Fires = " + std::to_string(mFiresCount);
    mLabelCounts->setString(str);
}
// _________________________________________________________________________________________
// создаем первую тестовую модельку
void HelloWorld::createSprite3d()
{
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    auto modelFilePath = FileUtils::getInstance()->fullPathForFilename("chicken_model.c3b");
    auto sprite = Sprite3D::create(modelFilePath, modelFilePath); //c3b file, created with the FBX-converter
    // "/Developer/Projects/ModelsTest/mac-build/bin/ModelsTest/Debug/ModelsTest.app/Contents/Resources/HelloWorld.png"
    //    if (not sprite)
    //        return;
    
    sprite->setScale(0.5f); //sets the object scale in float
    //sprite->setPosition(Vec2(200,200)); //sets sprite position
    //sprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    float posX = rand() % (int)visibleSize.width + origin.x;
    float posY = rand() % (int)visibleSize.height + origin.y;
    sprite->setPosition(posX, posY);
    addChild(sprite,Z_MODELS); //adds sprite to scene, z-index: 1
    
    
    auto animation = Animation3D::create("chicken_anim_run.c3b");
    auto animate = Animate3D::create(animation);
    sprite->runAction(RepeatForever::create(animate));
    
    //    "chicken_color.png"
    //    cocos2d::Texture* txt = cocos2d::Texture::create("chicken_color.png");
    auto txtFile = FileUtils::getInstance()->fullPathForFilename("chicken_color.png");
    sprite->setTexture(txtFile);
}
// _________________________________________________________________________________________
// Создаем спрайт в рандомном месте, который двигается вверх-вниз по экрану
void HelloWorld::createAnimatedSprite()
{
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    auto sprite = Sprite::create("HelloWorld.png");
    sprite->setColor(cocos2d::Color3B::GREEN);
    
    sprite->setScale(0.05f);
    
    float posX = rand() % (int)visibleSize.width + origin.x;
    float posY = visibleSize.height + origin.y + 50;
    sprite->setPosition(posX, posY);
    addChild(sprite,Z_ANIM_SPRITES);
    
    float maxDelaySec = 2.0f;
    float delaySec = maxDelaySec * float(rand() % 1000) / 1000.0f;
    DelayTime* delay = DelayTime::create(delaySec);
    
    float moveTime = 3.0f;
    MoveTo* moveBottom = MoveTo::create(moveTime, Vec2(posX, origin.y));
    MoveTo* moveTop = MoveTo::create(0, Vec2(posX, visibleSize.height + origin.y));
    
    Sequence* moveBottomTop = Sequence::createWithTwoActions(moveBottom, moveTop);
    
    int repeatCount = 999999;
    Repeat* moveRepeat = Repeat::create(moveBottomTop, repeatCount);
    
    Sequence* action = Sequence::createWithTwoActions(delay, moveRepeat);
    sprite->runAction(action);
}
// _________________________________________________________________________________________
// Добавить огонька
#include "FireEffect/FireEffect.h"
void HelloWorld::addFireEffect()
{
    cocos2d::Size fireSize = cocos2d::Size(400,400);
    FireEffect::FireType fireEffectType = FireEffect::FireType::BOLD;
    float deltaTime = 0.0f;
    float fireSaturation = 1.0f;
    float animSpeed = 1.0f/30;
    float displacementScale = 1.0f; // aspect->displacement_scale()
    
    FireEffect* fireNode = FireEffect::create(fireSize, fireEffectType, deltaTime, fireSaturation, animSpeed, displacementScale);
    addChild(fireNode, Z_FIRE);
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    //    fireNode->setPosition(Vec2(visibleSize.width/4 + origin.x, visibleSize.height * 2/3 + origin.y));
    float posX = rand() % (int)visibleSize.width + origin.x;
    float posY = rand() % (int)visibleSize.height + origin.y;
    fireNode->setPosition(posX, posY);
}
// _________________________________________________________________________________________
