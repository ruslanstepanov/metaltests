
#ifdef GL_ES
#define QUALITY highp
#else
#define QUALITY
#endif

attribute QUALITY vec4 a_position;
attribute QUALITY vec2 a_texCoord;
attribute QUALITY vec4 a_color;

varying QUALITY vec2 v_texCoord;



void main()
{
	gl_Position = CC_MVPMatrix * a_position;

	v_texCoord = a_texCoord;
}
