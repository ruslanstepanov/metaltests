
/*
#ifdef GL_ES
#define QUALITY highp
#else
#define QUALITY
#endif

uniform QUALITY sampler2D uMainTexture;
uniform QUALITY sampler2D uWindTexture;
uniform QUALITY sampler2D uGradTexture;

uniform QUALITY float uTime;
uniform QUALITY float uSaturation;
uniform QUALITY float uWindDisplacement;

varying QUALITY vec2 v_texCoord;


// _________________________________________________________________________________
void main()
{
	float blue = texture2D(uMainTexture, vec2(v_texCoord.x, v_texCoord.y)).b;

	vec2 windTex = texture2D(uWindTexture, vec2(v_texCoord.x, v_texCoord.y + uTime * 0.7)).rb;
	windTex = windTex * 2.0 - 1.0;
	windTex *= blue * uWindDisplacement * 0.3;

	float red = texture2D(uMainTexture, vec2(v_texCoord.x, v_texCoord.y)).r;
	float green = texture2D(uMainTexture, vec2(v_texCoord.x + windTex.x, v_texCoord.y + uTime + windTex.y)).g;

	float displacement = (red * green) * uSaturation;

	vec2 fireTexCoord = vec2(0.01 + displacement, 0.5);
	vec4 fireColor = texture2D(uGradTexture, fireTexCoord);

	gl_FragColor = fireColor;
}
// _________________________________________________________________________________
*/

// test
void main()
{
}
